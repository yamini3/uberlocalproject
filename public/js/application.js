$(document).ready(function () {
  $("body").click(function (e) {
    if (!$(e.target).is('header')) {
      let toggleIcon = $('#navbar-ws').hasClass('show')
      if (toggleIcon) {
        $('#toggle').removeClass('fa-caret-up')
        $('#toggle').addClass('fa-caret-down')
      } else if(!toggleIcon && $(e.target).is('#toggle')) {
        $('#toggle').removeClass('fa-caret-down')
        $('#toggle').addClass('fa-caret-up')
      }

      $('.navbar-collapse').collapse('hide');
    }
  })
  $('.navbar-nav > span').on('click', function () {
    $('.navbar-collapse').collapse('hide');
  });

  $('.carousel').on('slid.bs.carousel', function (e) {
    var carouselData = $(this).data('bs.carousel');
    interval: 0
    var slideFrom = $(this).find('.active').index();
    var slideTo = $(e.relatedTarget).index();
    $('.result>a')
      .removeClass('active-p')
      .eq(slideFrom)
      .addClass('active-p');
  });
  $('.carousel').carousel({
    interval: 0
  })
  var itemSelect = '.card.item';
  // Show card click.
  $(document).on('click', itemSelect, function () {
    if ($(this).hasClass('unbinded')) return;
    //create select list based on Interest areas per row

    // If clicking on the element with an open card.
    if ($(this).hasClass('show-card')) {
      $(this).removeClass('show-card');
      $(this).find('.normal-height').removeClass('show-bubble');
      $('.support-card-container').remove();
      $(itemSelect + ':last-child').addClass('row_last');
    } else {
      // Hide any card that may be open somewhere else.
      $('.show-card').removeClass('show-card');
      $(this).find('.normal-height ').removeClass('show-bubble');
      $('.support-card-container').remove();
      $(itemSelect + ':last-child').addClass('row_last');

      // Grab the closest (next) row-end object to the clicked element.
      // This will make sure we grab the last element of the row.
      if ($(this).is(':visible')) {
        var $endObj = $(this).hasClass('row_last') ? $(this) : $($(this).nextAll(
          '.row_last')[0]);
      }
      // Create our card container.
      var $container = $(document.createElement('div'));
      // Add the necessary classes to the clicked element and card container.
      $(this).addClass('show-card');
      $(this).find('.normal-height ').addClass('show-bubble');
      $container.addClass('support-card-container');
      // Grab the html from the view field.
      $container.html($(this).find('.support-card').html());
      // Append the card container to the end of the row.
      $container.insertAfter($endObj);

      // Move the window to the card container
      $('html,body').animate({
          scrollTop: $(".support-card-container").offset().top - 250},
      300);
      let self = this;

      $(".close-icon:visible").click(function () {
        $(".support-card-container").remove()
        $(self).find('.normal-height ').removeClass('show-bubble');

      })
    }
  });  
});
