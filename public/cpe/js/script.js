/**
 * Owl Carousel v2.3.4
 * Copyright 2013-2018 David Deutsch
 * Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE
 */
// ! function (a, b, c, d) {
//   function e(b, c) {
//     this.settings = null, this.options = a.extend({}, e.Defaults, c), this.$element = a(b), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = {
//       time: null,
//       target: null,
//       pointer: null,
//       stage: {
//         start: null,
//         current: null
//       },
//       direction: null
//     }, this._states = {
//       current: {},
//       tags: {
//         initializing: ["busy"],
//         animating: ["busy"],
//         dragging: ["interacting"]
//       }
//     }, a.each(["onResize", "onThrottledResize"], a.proxy(function (b, c) {
//       this._handlers[c] = a.proxy(this[c], this)
//     }, this)), a.each(e.Plugins, a.proxy(function (a, b) {
//       this._plugins[a.charAt(0).toLowerCase() + a.slice(1)] = new b(this)
//     }, this)), a.each(e.Workers, a.proxy(function (b, c) {
//       this._pipe.push({
//         filter: c.filter,
//         run: a.proxy(c.run, this)
//       })
//     }, this)), this.setup(), this.initialize()
//   }
//   e.Defaults = {
//     items: 3,
//     loop: !1,
//     center: !1,
//     rewind: !1,
//     checkVisibility: !0,
//     mouseDrag: !0,
//     touchDrag: !0,
//     pullDrag: !0,
//     freeDrag: !1,
//     margin: 0,
//     stagePadding: 0,
//     merge: !1,
//     mergeFit: !0,
//     autoWidth: !1,
//     startPosition: 0,
//     rtl: !1,
//     smartSpeed: 250,
//     fluidSpeed: !1,
//     dragEndSpeed: !1,
//     responsive: {},
//     responsiveRefreshRate: 200,
//     responsiveBaseElement: b,
//     fallbackEasing: "swing",
//     slideTransition: "",
//     info: !1,
//     nestedItemSelector: !1,
//     itemElement: "div",
//     stageElement: "div",
//     refreshClass: "owl-refresh",
//     loadedClass: "owl-loaded",
//     loadingClass: "owl-loading",
//     rtlClass: "owl-rtl",
//     responsiveClass: "owl-responsive",
//     dragClass: "owl-drag",
//     itemClass: "owl-item",
//     stageClass: "owl-stage",
//     stageOuterClass: "owl-stage-outer",
//     grabClass: "owl-grab"
//   }, e.Width = {
//     Default: "default",
//     Inner: "inner",
//     Outer: "outer"
//   }, e.Type = {
//     Event: "event",
//     State: "state"
//   }, e.Plugins = {}, e.Workers = [{
//     filter: ["width", "settings"],
//     run: function () {
//       this._width = this.$element.width()
//     }
//   }, {
//     filter: ["width", "items", "settings"],
//     run: function (a) {
//       a.current = this._items && this._items[this.relative(this._current)]
//     }
//   }, {
//     filter: ["items", "settings"],
//     run: function () {
//       this.$stage.children(".cloned").remove()
//     }
//   }, {
//     filter: ["width", "items", "settings"],
//     run: function (a) {
//       var b = this.settings.margin || "",
//         c = !this.settings.autoWidth,
//         d = this.settings.rtl,
//         e = {
//           width: "auto",
//           "margin-left": d ? b : "",
//           "margin-right": d ? "" : b
//         };
//       !c && this.$stage.children().css(e), a.css = e
//     }
//   }, {
//     filter: ["width", "items", "settings"],
//     run: function (a) {
//       var b = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
//         c = null,
//         d = this._items.length,
//         e = !this.settings.autoWidth,
//         f = [];
//       for (a.items = {
//           merge: !1,
//           width: b
//         }; d--;) c = this._mergers[d], c = this.settings.mergeFit && Math.min(c, this.settings.items) || c, a.items.merge = c > 1 || a.items.merge, f[d] = e ? b * c : this._items[d].width();
//       this._widths = f
//     }
//   }, {
//     filter: ["items", "settings"],
//     run: function () {
//       var b = [],
//         c = this._items,
//         d = this.settings,
//         e = Math.max(2 * d.items, 4),
//         f = 2 * Math.ceil(c.length / 2),
//         g = d.loop && c.length ? d.rewind ? e : Math.max(e, f) : 0,
//         h = "",
//         i = "";
//       for (g /= 2; g > 0;) b.push(this.normalize(b.length / 2, !0)), h += c[b[b.length - 1]][0].outerHTML, b.push(this.normalize(c.length - 1 - (b.length - 1) / 2, !0)), i = c[b[b.length - 1]][0].outerHTML + i, g -= 1;
//       this._clones = b, a(h).addClass("cloned").appendTo(this.$stage), a(i).addClass("cloned").prependTo(this.$stage)
//     }
//   }, {
//     filter: ["width", "items", "settings"],
//     run: function () {
//       for (var a = this.settings.rtl ? 1 : -1, b = this._clones.length + this._items.length, c = -1, d = 0, e = 0, f = []; ++c < b;) d = f[c - 1] || 0, e = this._widths[this.relative(c)] + this.settings.margin, f.push(d + e * a);
//       this._coordinates = f
//     }
//   }, {
//     filter: ["width", "items", "settings"],
//     run: function () {
//       var a = this.settings.stagePadding,
//         b = this._coordinates,
//         c = {
//           width: Math.ceil(Math.abs(b[b.length - 1])) + 2 * a,
//           "padding-left": a || "",
//           "padding-right": a || ""
//         };
//       this.$stage.css(c)
//     }
//   }, {
//     filter: ["width", "items", "settings"],
//     run: function (a) {
//       var b = this._coordinates.length,
//         c = !this.settings.autoWidth,
//         d = this.$stage.children();
//       if (c && a.items.merge)
//         for (; b--;) a.css.width = this._widths[this.relative(b)], d.eq(b).css(a.css);
//       else c && (a.css.width = a.items.width, d.css(a.css))
//     }
//   }, {
//     filter: ["items"],
//     run: function () {
//       this._coordinates.length < 1 && this.$stage.removeAttr("style")
//     }
//   }, {
//     filter: ["width", "items", "settings"],
//     run: function (a) {
//       a.current = a.current ? this.$stage.children().index(a.current) : 0, a.current = Math.max(this.minimum(), Math.min(this.maximum(), a.current)), this.reset(a.current)
//     }
//   }, {
//     filter: ["position"],
//     run: function () {
//       this.animate(this.coordinates(this._current))
//     }
//   }, {
//     filter: ["width", "position", "items", "settings"],
//     run: function () {
//       var a, b, c, d, e = this.settings.rtl ? 1 : -1,
//         f = 2 * this.settings.stagePadding,
//         g = this.coordinates(this.current()) + f,
//         h = g + this.width() * e,
//         i = [];
//       for (c = 0, d = this._coordinates.length; c < d; c++) a = this._coordinates[c - 1] || 0, b = Math.abs(this._coordinates[c]) + f * e, (this.op(a, "<=", g) && this.op(a, ">", h) || this.op(b, "<", g) && this.op(b, ">", h)) && i.push(c);
//       this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + i.join("), :eq(") + ")").addClass("active"), this.$stage.children(".center").removeClass("center"), this.settings.center && this.$stage.children().eq(this.current()).addClass("center")
//     }
//   }], e.prototype.initializeStage = function () {
//     this.$stage = this.$element.find("." + this.settings.stageClass), this.$stage.length || (this.$element.addClass(this.options.loadingClass), this.$stage = a("<" + this.settings.stageElement + ">", {
//       class: this.settings.stageClass
//     }).wrap(a("<div/>", {
//       class: this.settings.stageOuterClass
//     })), this.$element.append(this.$stage.parent()))
//   }, e.prototype.initializeItems = function () {
//     var b = this.$element.find(".owl-item");
//     if (b.length) return this._items = b.get().map(function (b) {
//       return a(b)
//     }), this._mergers = this._items.map(function () {
//       return 1
//     }), void this.refresh();
//     this.replace(this.$element.children().not(this.$stage.parent())), this.isVisible() ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass)
//   }, e.prototype.initialize = function () {
//     if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
//       var a, b, c;
//       a = this.$element.find("img"), b = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : d, c = this.$element.children(b).width(), a.length && c <= 0 && this.preloadAutoWidthImages(a)
//     }
//     this.initializeStage(), this.initializeItems(), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized")
//   }, e.prototype.isVisible = function () {
//     return !this.settings.checkVisibility || this.$element.is(":visible")
//   }, e.prototype.setup = function () {
//     var b = this.viewport(),
//       c = this.options.responsive,
//       d = -1,
//       e = null;
//     c ? (a.each(c, function (a) {
//       a <= b && a > d && (d = Number(a))
//     }), e = a.extend({}, this.options, c[d]), "function" == typeof e.stagePadding && (e.stagePadding = e.stagePadding()), delete e.responsive, e.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + d))) : e = a.extend({}, this.options), this.trigger("change", {
//       property: {
//         name: "settings",
//         value: e
//       }
//     }), this._breakpoint = d, this.settings = e, this.invalidate("settings"), this.trigger("changed", {
//       property: {
//         name: "settings",
//         value: this.settings
//       }
//     })
//   }, e.prototype.optionsLogic = function () {
//     this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1)
//   }, e.prototype.prepare = function (b) {
//     var c = this.trigger("prepare", {
//       content: b
//     });
//     return c.data || (c.data = a("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(b)), this.trigger("prepared", {
//       content: c.data
//     }), c.data
//   }, e.prototype.update = function () {
//     for (var b = 0, c = this._pipe.length, d = a.proxy(function (a) {
//         return this[a]
//       }, this._invalidated), e = {}; b < c;)(this._invalidated.all || a.grep(this._pipe[b].filter, d).length > 0) && this._pipe[b].run(e), b++;
//     this._invalidated = {}, !this.is("valid") && this.enter("valid")
//   }, e.prototype.width = function (a) {
//     switch (a = a || e.Width.Default) {
//       case e.Width.Inner:
//       case e.Width.Outer:
//         return this._width;
//       default:
//         return this._width - 2 * this.settings.stagePadding + this.settings.margin
//     }
//   }, e.prototype.refresh = function () {
//     this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed")
//   }, e.prototype.onThrottledResize = function () {
//     b.clearTimeout(this.resizeTimer), this.resizeTimer = b.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate)
//   }, e.prototype.onResize = function () {
//     return !!this._items.length && (this._width !== this.$element.width() && (!!this.isVisible() && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")))))
//   }, e.prototype.registerEventHandlers = function () {
//     a.support.transition && this.$stage.on(a.support.transition.end + ".owl.core", a.proxy(this.onTransitionEnd, this)), !1 !== this.settings.responsive && this.on(b, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", a.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
//       return !1
//     })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", a.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", a.proxy(this.onDragEnd, this)))
//   }, e.prototype.onDragStart = function (b) {
//     var d = null;
//     3 !== b.which && (a.support.transform ? (d = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), d = {
//       x: d[16 === d.length ? 12 : 4],
//       y: d[16 === d.length ? 13 : 5]
//     }) : (d = this.$stage.position(), d = {
//       x: this.settings.rtl ? d.left + this.$stage.width() - this.width() + this.settings.margin : d.left,
//       y: d.top
//     }), this.is("animating") && (a.support.transform ? this.animate(d.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === b.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = a(b.target), this._drag.stage.start = d, this._drag.stage.current = d, this._drag.pointer = this.pointer(b), a(c).on("mouseup.owl.core touchend.owl.core", a.proxy(this.onDragEnd, this)), a(c).one("mousemove.owl.core touchmove.owl.core", a.proxy(function (b) {
//       var d = this.difference(this._drag.pointer, this.pointer(b));
//       a(c).on("mousemove.owl.core touchmove.owl.core", a.proxy(this.onDragMove, this)), Math.abs(d.x) < Math.abs(d.y) && this.is("valid") || (b.preventDefault(), this.enter("dragging"), this.trigger("drag"))
//     }, this)))
//   }, e.prototype.onDragMove = function (a) {
//     var b = null,
//       c = null,
//       d = null,
//       e = this.difference(this._drag.pointer, this.pointer(a)),
//       f = this.difference(this._drag.stage.start, e);
//     this.is("dragging") && (a.preventDefault(), this.settings.loop ? (b = this.coordinates(this.minimum()), c = this.coordinates(this.maximum() + 1) - b, f.x = ((f.x - b) % c + c) % c + b) : (b = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), c = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), d = this.settings.pullDrag ? -1 * e.x / 5 : 0, f.x = Math.max(Math.min(f.x, b + d), c + d)), this._drag.stage.current = f, this.animate(f.x))
//   }, e.prototype.onDragEnd = function (b) {
//     var d = this.difference(this._drag.pointer, this.pointer(b)),
//       e = this._drag.stage.current,
//       f = d.x > 0 ^ this.settings.rtl ? "left" : "right";
//     a(c).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== d.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(e.x, 0 !== d.x ? f : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = f, (Math.abs(d.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () {
//       return !1
//     })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"))
//   }, e.prototype.closest = function (b, c) {
//     var e = -1,
//       f = 30,
//       g = this.width(),
//       h = this.coordinates();
//     return this.settings.freeDrag || a.each(h, a.proxy(function (a, i) {
//       return "left" === c && b > i - f && b < i + f ? e = a : "right" === c && b > i - g - f && b < i - g + f ? e = a + 1 : this.op(b, "<", i) && this.op(b, ">", h[a + 1] !== d ? h[a + 1] : i - g) && (e = "left" === c ? a + 1 : a), -1 === e
//     }, this)), this.settings.loop || (this.op(b, ">", h[this.minimum()]) ? e = b = this.minimum() : this.op(b, "<", h[this.maximum()]) && (e = b = this.maximum())), e
//   }, e.prototype.animate = function (b) {
//     var c = this.speed() > 0;
//     this.is("animating") && this.onTransitionEnd(), c && (this.enter("animating"), this.trigger("translate")), a.support.transform3d && a.support.transition ? this.$stage.css({
//       transform: "translate3d(" + b + "px,0px,0px)",
//       transition: this.speed() / 1e3 + "s" + (this.settings.slideTransition ? " " + this.settings.slideTransition : "")
//     }) : c ? this.$stage.animate({
//       left: b + "px"
//     }, this.speed(), this.settings.fallbackEasing, a.proxy(this.onTransitionEnd, this)) : this.$stage.css({
//       left: b + "px"
//     })
//   }, e.prototype.is = function (a) {
//     return this._states.current[a] && this._states.current[a] > 0
//   }, e.prototype.current = function (a) {
//     if (a === d) return this._current;
//     if (0 === this._items.length) return d;
//     if (a = this.normalize(a), this._current !== a) {
//       var b = this.trigger("change", {
//         property: {
//           name: "position",
//           value: a
//         }
//       });
//       b.data !== d && (a = this.normalize(b.data)), this._current = a, this.invalidate("position"), this.trigger("changed", {
//         property: {
//           name: "position",
//           value: this._current
//         }
//       })
//     }
//     return this._current
//   }, e.prototype.invalidate = function (b) {
//     return "string" === a.type(b) && (this._invalidated[b] = !0, this.is("valid") && this.leave("valid")), a.map(this._invalidated, function (a, b) {
//       return b
//     })
//   }, e.prototype.reset = function (a) {
//     (a = this.normalize(a)) !== d && (this._speed = 0, this._current = a, this.suppress(["translate", "translated"]), this.animate(this.coordinates(a)), this.release(["translate", "translated"]))
//   }, e.prototype.normalize = function (a, b) {
//     var c = this._items.length,
//       e = b ? 0 : this._clones.length;
//     return !this.isNumeric(a) || c < 1 ? a = d : (a < 0 || a >= c + e) && (a = ((a - e / 2) % c + c) % c + e / 2), a
//   }, e.prototype.relative = function (a) {
//     return a -= this._clones.length / 2, this.normalize(a, !0)
//   }, e.prototype.maximum = function (a) {
//     var b, c, d, e = this.settings,
//       f = this._coordinates.length;
//     if (e.loop) f = this._clones.length / 2 + this._items.length - 1;
//     else if (e.autoWidth || e.merge) {
//       if (b = this._items.length)
//         for (c = this._items[--b].width(), d = this.$element.width(); b-- && !((c += this._items[b].width() + this.settings.margin) > d););
//       f = b + 1
//     } else f = e.center ? this._items.length - 1 : this._items.length - e.items;
//     return a && (f -= this._clones.length / 2), Math.max(f, 0)
//   }, e.prototype.minimum = function (a) {
//     return a ? 0 : this._clones.length / 2
//   }, e.prototype.items = function (a) {
//     return a === d ? this._items.slice() : (a = this.normalize(a, !0), this._items[a])
//   }, e.prototype.mergers = function (a) {
//     return a === d ? this._mergers.slice() : (a = this.normalize(a, !0), this._mergers[a])
//   }, e.prototype.clones = function (b) {
//     var c = this._clones.length / 2,
//       e = c + this._items.length,
//       f = function (a) {
//         return a % 2 == 0 ? e + a / 2 : c - (a + 1) / 2
//       };
//     return b === d ? a.map(this._clones, function (a, b) {
//       return f(b)
//     }) : a.map(this._clones, function (a, c) {
//       return a === b ? f(c) : null
//     })
//   }, e.prototype.speed = function (a) {
//     return a !== d && (this._speed = a), this._speed
//   }, e.prototype.coordinates = function (b) {
//     var c, e = 1,
//       f = b - 1;
//     return b === d ? a.map(this._coordinates, a.proxy(function (a, b) {
//       return this.coordinates(b)
//     }, this)) : (this.settings.center ? (this.settings.rtl && (e = -1, f = b + 1), c = this._coordinates[b], c += (this.width() - c + (this._coordinates[f] || 0)) / 2 * e) : c = this._coordinates[f] || 0, c = Math.ceil(c))
//   }, e.prototype.duration = function (a, b, c) {
//     return 0 === c ? 0 : Math.min(Math.max(Math.abs(b - a), 1), 6) * Math.abs(c || this.settings.smartSpeed)
//   }, e.prototype.to = function (a, b) {
//     var c = this.current(),
//       d = null,
//       e = a - this.relative(c),
//       f = (e > 0) - (e < 0),
//       g = this._items.length,
//       h = this.minimum(),
//       i = this.maximum();
//     this.settings.loop ? (!this.settings.rewind && Math.abs(e) > g / 2 && (e += -1 * f * g), a = c + e, (d = ((a - h) % g + g) % g + h) !== a && d - e <= i && d - e > 0 && (c = d - e, a = d, this.reset(c))) : this.settings.rewind ? (i += 1, a = (a % i + i) % i) : a = Math.max(h, Math.min(i, a)), this.speed(this.duration(c, a, b)), this.current(a), this.isVisible() && this.update()
//   }, e.prototype.next = function (a) {
//     a = a || !1, this.to(this.relative(this.current()) + 1, a)
//   }, e.prototype.prev = function (a) {
//     a = a || !1, this.to(this.relative(this.current()) - 1, a)
//   }, e.prototype.onTransitionEnd = function (a) {
//     if (a !== d && (a.stopPropagation(), (a.target || a.srcElement || a.originalTarget) !== this.$stage.get(0))) return !1;
//     this.leave("animating"), this.trigger("translated")
//   }, e.prototype.viewport = function () {
//     var d;
//     return this.options.responsiveBaseElement !== b ? d = a(this.options.responsiveBaseElement).width() : b.innerWidth ? d = b.innerWidth : c.documentElement && c.documentElement.clientWidth ? d = c.documentElement.clientWidth : console.warn("Can not detect viewport width."), d
//   }, e.prototype.replace = function (b) {
//     this.$stage.empty(), this._items = [], b && (b = b instanceof jQuery ? b : a(b)), this.settings.nestedItemSelector && (b = b.find("." + this.settings.nestedItemSelector)), b.filter(function () {
//       return 1 === this.nodeType
//     }).each(a.proxy(function (a, b) {
//       b = this.prepare(b), this.$stage.append(b), this._items.push(b), this._mergers.push(1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)
//     }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items")
//   }, e.prototype.add = function (b, c) {
//     var e = this.relative(this._current);
//     c = c === d ? this._items.length : this.normalize(c, !0), b = b instanceof jQuery ? b : a(b), this.trigger("add", {
//       content: b,
//       position: c
//     }), b = this.prepare(b), 0 === this._items.length || c === this._items.length ? (0 === this._items.length && this.$stage.append(b), 0 !== this._items.length && this._items[c - 1].after(b), this._items.push(b), this._mergers.push(1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[c].before(b), this._items.splice(c, 0, b), this._mergers.splice(c, 0, 1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[e] && this.reset(this._items[e].index()), this.invalidate("items"), this.trigger("added", {
//       content: b,
//       position: c
//     })
//   }, e.prototype.remove = function (a) {
//     (a = this.normalize(a, !0)) !== d && (this.trigger("remove", {
//       content: this._items[a],
//       position: a
//     }), this._items[a].remove(), this._items.splice(a, 1), this._mergers.splice(a, 1), this.invalidate("items"), this.trigger("removed", {
//       content: null,
//       position: a
//     }))
//   }, e.prototype.preloadAutoWidthImages = function (b) {
//     b.each(a.proxy(function (b, c) {
//       this.enter("pre-loading"), c = a(c), a(new Image).one("load", a.proxy(function (a) {
//         c.attr("src", a.target.src), c.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh()
//       }, this)).attr("src", c.attr("src") || c.attr("data-src") || c.attr("data-src-retina"))
//     }, this))
//   }, e.prototype.destroy = function () {
//     this.$element.off(".owl.core"), this.$stage.off(".owl.core"), a(c).off(".owl.core"), !1 !== this.settings.responsive && (b.clearTimeout(this.resizeTimer), this.off(b, "resize", this._handlers.onThrottledResize));
//     for (var d in this._plugins) this._plugins[d].destroy();
//     this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$stage.remove(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel")
//   }, e.prototype.op = function (a, b, c) {
//     var d = this.settings.rtl;
//     switch (b) {
//       case "<":
//         return d ? a > c : a < c;
//       case ">":
//         return d ? a < c : a > c;
//       case ">=":
//         return d ? a <= c : a >= c;
//       case "<=":
//         return d ? a >= c : a <= c
//     }
//   }, e.prototype.on = function (a, b, c, d) {
//     a.addEventListener ? a.addEventListener(b, c, d) : a.attachEvent && a.attachEvent("on" + b, c)
//   }, e.prototype.off = function (a, b, c, d) {
//     a.removeEventListener ? a.removeEventListener(b, c, d) : a.detachEvent && a.detachEvent("on" + b, c)
//   }, e.prototype.trigger = function (b, c, d, f, g) {
//     var h = {
//         item: {
//           count: this._items.length,
//           index: this.current()
//         }
//       },
//       i = a.camelCase(a.grep(["on", b, d], function (a) {
//         return a
//       }).join("-").toLowerCase()),
//       j = a.Event([b, "owl", d || "carousel"].join(".").toLowerCase(), a.extend({
//         relatedTarget: this
//       }, h, c));
//     return this._supress[b] || (a.each(this._plugins, function (a, b) {
//       b.onTrigger && b.onTrigger(j)
//     }), this.register({
//       type: e.Type.Event,
//       name: b
//     }), this.$element.trigger(j), this.settings && "function" == typeof this.settings[i] && this.settings[i].call(this, j)), j
//   }, e.prototype.enter = function (b) {
//     a.each([b].concat(this._states.tags[b] || []), a.proxy(function (a, b) {
//       this._states.current[b] === d && (this._states.current[b] = 0), this._states.current[b]++
//     }, this))
//   }, e.prototype.leave = function (b) {
//     a.each([b].concat(this._states.tags[b] || []), a.proxy(function (a, b) {
//       this._states.current[b]--
//     }, this))
//   }, e.prototype.register = function (b) {
//     if (b.type === e.Type.Event) {
//       if (a.event.special[b.name] || (a.event.special[b.name] = {}), !a.event.special[b.name].owl) {
//         var c = a.event.special[b.name]._default;
//         a.event.special[b.name]._default = function (a) {
//           return !c || !c.apply || a.namespace && -1 !== a.namespace.indexOf("owl") ? a.namespace && a.namespace.indexOf("owl") > -1 : c.apply(this, arguments)
//         }, a.event.special[b.name].owl = !0
//       }
//     } else b.type === e.Type.State && (this._states.tags[b.name] ? this._states.tags[b.name] = this._states.tags[b.name].concat(b.tags) : this._states.tags[b.name] = b.tags, this._states.tags[b.name] = a.grep(this._states.tags[b.name], a.proxy(function (c, d) {
//       return a.inArray(c, this._states.tags[b.name]) === d
//     }, this)))
//   }, e.prototype.suppress = function (b) {
//     a.each(b, a.proxy(function (a, b) {
//       this._supress[b] = !0
//     }, this))
//   }, e.prototype.release = function (b) {
//     a.each(b, a.proxy(function (a, b) {
//       delete this._supress[b]
//     }, this))
//   }, e.prototype.pointer = function (a) {
//     var c = {
//       x: null,
//       y: null
//     };
//     return a = a.originalEvent || a || b.event, a = a.touches && a.touches.length ? a.touches[0] : a.changedTouches && a.changedTouches.length ? a.changedTouches[0] : a, a.pageX ? (c.x = a.pageX, c.y = a.pageY) : (c.x = a.clientX, c.y = a.clientY), c
//   }, e.prototype.isNumeric = function (a) {
//     return !isNaN(parseFloat(a))
//   }, e.prototype.difference = function (a, b) {
//     return {
//       x: a.x - b.x,
//       y: a.y - b.y
//     }
//   }, a.fn.owlCarousel = function (b) {
//     var c = Array.prototype.slice.call(arguments, 1);
//     return this.each(function () {
//       var d = a(this),
//         f = d.data("owl.carousel");
//       f || (f = new e(this, "object" == typeof b && b), d.data("owl.carousel", f), a.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (b, c) {
//         f.register({
//           type: e.Type.Event,
//           name: c
//         }), f.$element.on(c + ".owl.carousel.core", a.proxy(function (a) {
//           a.namespace && a.relatedTarget !== this && (this.suppress([c]), f[c].apply(this, [].slice.call(arguments, 1)), this.release([c]))
//         }, f))
//       })), "string" == typeof b && "_" !== b.charAt(0) && f[b].apply(f, c)
//     })
//   }, a.fn.owlCarousel.Constructor = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   var e = function (b) {
//     this._core = b, this._interval = null, this._visible = null, this._handlers = {
//       "initialized.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.settings.autoRefresh && this.watch()
//       }, this)
//     }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers)
//   };
//   e.Defaults = {
//     autoRefresh: !0,
//     autoRefreshInterval: 500
//   }, e.prototype.watch = function () {
//     this._interval || (this._visible = this._core.isVisible(), this._interval = b.setInterval(a.proxy(this.refresh, this), this._core.settings.autoRefreshInterval))
//   }, e.prototype.refresh = function () {
//     this._core.isVisible() !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh())
//   }, e.prototype.destroy = function () {
//     var a, c;
//     b.clearInterval(this._interval);
//     for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
//     for (c in Object.getOwnPropertyNames(this)) "function" != typeof this[c] && (this[c] = null)
//   }, a.fn.owlCarousel.Constructor.Plugins.AutoRefresh = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   var e = function (b) {
//     this._core = b, this._loaded = [], this._handlers = {
//       "initialized.owl.carousel change.owl.carousel resized.owl.carousel": a.proxy(function (b) {
//         if (b.namespace && this._core.settings && this._core.settings.lazyLoad && (b.property && "position" == b.property.name || "initialized" == b.type)) {
//           var c = this._core.settings,
//             e = c.center && Math.ceil(c.items / 2) || c.items,
//             f = c.center && -1 * e || 0,
//             g = (b.property && b.property.value !== d ? b.property.value : this._core.current()) + f,
//             h = this._core.clones().length,
//             i = a.proxy(function (a, b) {
//               this.load(b)
//             }, this);
//           for (c.lazyLoadEager > 0 && (e += c.lazyLoadEager, c.loop && (g -= c.lazyLoadEager, e++)); f++ < e;) this.load(h / 2 + this._core.relative(g)), h && a.each(this._core.clones(this._core.relative(g)), i), g++
//         }
//       }, this)
//     }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers)
//   };
//   e.Defaults = {
//     lazyLoad: !1,
//     lazyLoadEager: 0
//   }, e.prototype.load = function (c) {
//     var d = this._core.$stage.children().eq(c),
//       e = d && d.find(".owl-lazy");
//     !e || a.inArray(d.get(0), this._loaded) > -1 || (e.each(a.proxy(function (c, d) {
//       var e, f = a(d),
//         g = b.devicePixelRatio > 1 && f.attr("data-src-retina") || f.attr("data-src") || f.attr("data-srcset");
//       this._core.trigger("load", {
//         element: f,
//         url: g
//       }, "lazy"), f.is("img") ? f.one("load.owl.lazy", a.proxy(function () {
//         f.css("opacity", 1), this._core.trigger("loaded", {
//           element: f,
//           url: g
//         }, "lazy")
//       }, this)).attr("src", g) : f.is("source") ? f.one("load.owl.lazy", a.proxy(function () {
//         this._core.trigger("loaded", {
//           element: f,
//           url: g
//         }, "lazy")
//       }, this)).attr("srcset", g) : (e = new Image, e.onload = a.proxy(function () {
//         f.css({
//           "background-image": 'url("' + g + '")',
//           opacity: "1"
//         }), this._core.trigger("loaded", {
//           element: f,
//           url: g
//         }, "lazy")
//       }, this), e.src = g)
//     }, this)), this._loaded.push(d.get(0)))
//   }, e.prototype.destroy = function () {
//     var a, b;
//     for (a in this.handlers) this._core.$element.off(a, this.handlers[a]);
//     for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null)
//   }, a.fn.owlCarousel.Constructor.Plugins.Lazy = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   var e = function (c) {
//     this._core = c, this._previousHeight = null, this._handlers = {
//       "initialized.owl.carousel refreshed.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.settings.autoHeight && this.update()
//       }, this),
//       "changed.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.settings.autoHeight && "position" === a.property.name && this.update()
//       }, this),
//       "loaded.owl.lazy": a.proxy(function (a) {
//         a.namespace && this._core.settings.autoHeight && a.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update()
//       }, this)
//     }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers), this._intervalId = null;
//     var d = this;
//     a(b).on("load", function () {
//       d._core.settings.autoHeight && d.update()
//     }), a(b).resize(function () {
//       d._core.settings.autoHeight && (null != d._intervalId && clearTimeout(d._intervalId), d._intervalId = setTimeout(function () {
//         d.update()
//       }, 250))
//     })
//   };
//   e.Defaults = {
//     autoHeight: !1,
//     autoHeightClass: "owl-height"
//   }, e.prototype.update = function () {
//     var b = this._core._current,
//       c = b + this._core.settings.items,
//       d = this._core.settings.lazyLoad,
//       e = this._core.$stage.children().toArray().slice(b, c),
//       f = [],
//       g = 0;
//     a.each(e, function (b, c) {
//       f.push(a(c).height())
//     }), g = Math.max.apply(null, f), g <= 1 && d && this._previousHeight && (g = this._previousHeight), this._previousHeight = g, this._core.$stage.parent().height(g).addClass(this._core.settings.autoHeightClass)
//   }, e.prototype.destroy = function () {
//     var a, b;
//     for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
//     for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null)
//   }, a.fn.owlCarousel.Constructor.Plugins.AutoHeight = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   var e = function (b) {
//     this._core = b, this._videos = {}, this._playing = null, this._handlers = {
//       "initialized.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.register({
//           type: "state",
//           name: "playing",
//           tags: ["interacting"]
//         })
//       }, this),
//       "resize.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.settings.video && this.isInFullScreen() && a.preventDefault()
//       }, this),
//       "refreshed.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove()
//       }, this),
//       "changed.owl.carousel": a.proxy(function (a) {
//         a.namespace && "position" === a.property.name && this._playing && this.stop()
//       }, this),
//       "prepared.owl.carousel": a.proxy(function (b) {
//         if (b.namespace) {
//           var c = a(b.content).find(".owl-video");
//           c.length && (c.css("display", "none"), this.fetch(c, a(b.content)))
//         }
//       }, this)
//     }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", a.proxy(function (a) {
//       this.play(a)
//     }, this))
//   };
//   e.Defaults = {
//     video: !1,
//     videoHeight: !1,
//     videoWidth: !1
//   }, e.prototype.fetch = function (a, b) {
//     var c = function () {
//         return a.attr("data-vimeo-id") ? "vimeo" : a.attr("data-vzaar-id") ? "vzaar" : "youtube"
//       }(),
//       d = a.attr("data-vimeo-id") || a.attr("data-youtube-id") || a.attr("data-vzaar-id"),
//       e = a.attr("data-width") || this._core.settings.videoWidth,
//       f = a.attr("data-height") || this._core.settings.videoHeight,
//       g = a.attr("href");
//     if (!g) throw new Error("Missing video URL.");
//     if (d = g.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/), d[3].indexOf("youtu") > -1) c = "youtube";
//     else if (d[3].indexOf("vimeo") > -1) c = "vimeo";
//     else {
//       if (!(d[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
//       c = "vzaar"
//     }
//     d = d[6], this._videos[g] = {
//       type: c,
//       id: d,
//       width: e,
//       height: f
//     }, b.attr("data-video", g), this.thumbnail(a, this._videos[g])
//   }, e.prototype.thumbnail = function (b, c) {
//     var d, e, f, g = c.width && c.height ? "width:" + c.width + "px;height:" + c.height + "px;" : "",
//       h = b.find("img"),
//       i = "src",
//       j = "",
//       k = this._core.settings,
//       l = function (c) {
//         e = '<div class="owl-video-play-icon"></div>', d = k.lazyLoad ? a("<div/>", {
//           class: "owl-video-tn " + j,
//           srcType: c
//         }) : a("<div/>", {
//           class: "owl-video-tn",
//           style: "opacity:1;background-image:url(" + c + ")"
//         }), b.after(d), b.after(e)
//       };
//     if (b.wrap(a("<div/>", {
//         class: "owl-video-wrapper",
//         style: g
//       })), this._core.settings.lazyLoad && (i = "data-src", j = "owl-lazy"), h.length) return l(h.attr(i)), h.remove(), !1;
//     "youtube" === c.type ? (f = "//img.youtube.com/vi/" + c.id + "/hqdefault.jpg", l(f)) : "vimeo" === c.type ? a.ajax({
//       type: "GET",
//       url: "//vimeo.com/api/v2/video/" + c.id + ".json",
//       jsonp: "callback",
//       dataType: "jsonp",
//       success: function (a) {
//         f = a[0].thumbnail_large, l(f)
//       }
//     }) : "vzaar" === c.type && a.ajax({
//       type: "GET",
//       url: "//vzaar.com/api/videos/" + c.id + ".json",
//       jsonp: "callback",
//       dataType: "jsonp",
//       success: function (a) {
//         f = a.framegrab_url, l(f)
//       }
//     })
//   }, e.prototype.stop = function () {
//     this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video")
//   }, e.prototype.play = function (b) {
//     var c, d = a(b.target),
//       e = d.closest("." + this._core.settings.itemClass),
//       f = this._videos[e.attr("data-video")],
//       g = f.width || "100%",
//       h = f.height || this._core.$stage.height();
//     this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), e = this._core.items(this._core.relative(e.index())), this._core.reset(e.index()), c = a('<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>'), c.attr("height", h), c.attr("width", g), "youtube" === f.type ? c.attr("src", "//www.youtube.com/embed/" + f.id + "?autoplay=1&rel=0&v=" + f.id) : "vimeo" === f.type ? c.attr("src", "//player.vimeo.com/video/" + f.id + "?autoplay=1") : "vzaar" === f.type && c.attr("src", "//view.vzaar.com/" + f.id + "/player?autoplay=true"), a(c).wrap('<div class="owl-video-frame" />').insertAfter(e.find(".owl-video")), this._playing = e.addClass("owl-video-playing"))
//   }, e.prototype.isInFullScreen = function () {
//     var b = c.fullscreenElement || c.mozFullScreenElement || c.webkitFullscreenElement;
//     return b && a(b).parent().hasClass("owl-video-frame")
//   }, e.prototype.destroy = function () {
//     var a, b;
//     this._core.$element.off("click.owl.video");
//     for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
//     for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null)
//   }, a.fn.owlCarousel.Constructor.Plugins.Video = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   var e = function (b) {
//     this.core = b, this.core.options = a.extend({}, e.Defaults, this.core.options), this.swapping = !0, this.previous = d, this.next = d, this.handlers = {
//       "change.owl.carousel": a.proxy(function (a) {
//         a.namespace && "position" == a.property.name && (this.previous = this.core.current(), this.next = a.property.value)
//       }, this),
//       "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": a.proxy(function (a) {
//         a.namespace && (this.swapping = "translated" == a.type)
//       }, this),
//       "translate.owl.carousel": a.proxy(function (a) {
//         a.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap()
//       }, this)
//     }, this.core.$element.on(this.handlers)
//   };
//   e.Defaults = {
//     animateOut: !1,
//     animateIn: !1
//   }, e.prototype.swap = function () {
//     if (1 === this.core.settings.items && a.support.animation && a.support.transition) {
//       this.core.speed(0);
//       var b, c = a.proxy(this.clear, this),
//         d = this.core.$stage.children().eq(this.previous),
//         e = this.core.$stage.children().eq(this.next),
//         f = this.core.settings.animateIn,
//         g = this.core.settings.animateOut;
//       this.core.current() !== this.previous && (g && (b = this.core.coordinates(this.previous) - this.core.coordinates(this.next), d.one(a.support.animation.end, c).css({
//         left: b + "px"
//       }).addClass("animated owl-animated-out").addClass(g)), f && e.one(a.support.animation.end, c).addClass("animated owl-animated-in").addClass(f))
//     }
//   }, e.prototype.clear = function (b) {
//     a(b.target).css({
//       left: ""
//     }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd()
//   }, e.prototype.destroy = function () {
//     var a, b;
//     for (a in this.handlers) this.core.$element.off(a, this.handlers[a]);
//     for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null)
//   }, a.fn.owlCarousel.Constructor.Plugins.Animate = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   var e = function (b) {
//     this._core = b, this._call = null, this._time = 0, this._timeout = 0, this._paused = !0, this._handlers = {
//       "changed.owl.carousel": a.proxy(function (a) {
//         a.namespace && "settings" === a.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : a.namespace && "position" === a.property.name && this._paused && (this._time = 0)
//       }, this),
//       "initialized.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.settings.autoplay && this.play()
//       }, this),
//       "play.owl.autoplay": a.proxy(function (a, b, c) {
//         a.namespace && this.play(b, c)
//       }, this),
//       "stop.owl.autoplay": a.proxy(function (a) {
//         a.namespace && this.stop()
//       }, this),
//       "mouseover.owl.autoplay": a.proxy(function () {
//         this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
//       }, this),
//       "mouseleave.owl.autoplay": a.proxy(function () {
//         this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play()
//       }, this),
//       "touchstart.owl.core": a.proxy(function () {
//         this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause()
//       }, this),
//       "touchend.owl.core": a.proxy(function () {
//         this._core.settings.autoplayHoverPause && this.play()
//       }, this)
//     }, this._core.$element.on(this._handlers), this._core.options = a.extend({}, e.Defaults, this._core.options)
//   };
//   e.Defaults = {
//     autoplay: !1,
//     autoplayTimeout: 5e3,
//     autoplayHoverPause: !1,
//     autoplaySpeed: !1
//   }, e.prototype._next = function (d) {
//     this._call = b.setTimeout(a.proxy(this._next, this, d), this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()), this._core.is("interacting") || c.hidden || this._core.next(d || this._core.settings.autoplaySpeed)
//   }, e.prototype.read = function () {
//     return (new Date).getTime() - this._time
//   }, e.prototype.play = function (c, d) {
//     var e;
//     this._core.is("rotating") || this._core.enter("rotating"), c = c || this._core.settings.autoplayTimeout, e = Math.min(this._time % (this._timeout || c), c), this._paused ? (this._time = this.read(), this._paused = !1) : b.clearTimeout(this._call), this._time += this.read() % c - e, this._timeout = c, this._call = b.setTimeout(a.proxy(this._next, this, d), c - e)
//   }, e.prototype.stop = function () {
//     this._core.is("rotating") && (this._time = 0, this._paused = !0, b.clearTimeout(this._call), this._core.leave("rotating"))
//   }, e.prototype.pause = function () {
//     this._core.is("rotating") && !this._paused && (this._time = this.read(), this._paused = !0, b.clearTimeout(this._call))
//   }, e.prototype.destroy = function () {
//     var a, b;
//     this.stop();
//     for (a in this._handlers) this._core.$element.off(a, this._handlers[a]);
//     for (b in Object.getOwnPropertyNames(this)) "function" != typeof this[b] && (this[b] = null)
//   }, a.fn.owlCarousel.Constructor.Plugins.autoplay = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   "use strict";
//   var e = function (b) {
//     this._core = b, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = {
//       next: this._core.next,
//       prev: this._core.prev,
//       to: this._core.to
//     }, this._handlers = {
//       "prepared.owl.carousel": a.proxy(function (b) {
//         b.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + a(b.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>")
//       }, this),
//       "added.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.settings.dotsData && this._templates.splice(a.position, 0, this._templates.pop())
//       }, this),
//       "remove.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._core.settings.dotsData && this._templates.splice(a.position, 1)
//       }, this),
//       "changed.owl.carousel": a.proxy(function (a) {
//         a.namespace && "position" == a.property.name && this.draw()
//       }, this),
//       "initialized.owl.carousel": a.proxy(function (a) {
//         a.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"))
//       }, this),
//       "refreshed.owl.carousel": a.proxy(function (a) {
//         a.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"))
//       }, this)
//     }, this._core.options = a.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers)
//   };
//   e.Defaults = {
//     nav: !1,
//     navText: ['<span aria-label="Previous">&#x2039;</span>', '<span aria-label="Next">&#x203a;</span>'],
//     navSpeed: !1,
//     navElement: 'button type="button" role="presentation"',
//     navContainer: !1,
//     navContainerClass: "owl-nav",
//     navClass: ["owl-prev", "owl-next"],
//     slideBy: 1,
//     dotClass: "owl-dot",
//     dotsClass: "owl-dots",
//     dots: !0,
//     dotsEach: !1,
//     dotsData: !1,
//     dotsSpeed: !1,
//     dotsContainer: !1
//   }, e.prototype.initialize = function () {
//     var b, c = this._core.settings;
//     this._controls.$relative = (c.navContainer ? a(c.navContainer) : a("<div>").addClass(c.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = a("<" + c.navElement + ">").addClass(c.navClass[0]).html(c.navText[0]).prependTo(this._controls.$relative).on("click", a.proxy(function (a) {
//       this.prev(c.navSpeed)
//     }, this)), this._controls.$next = a("<" + c.navElement + ">").addClass(c.navClass[1]).html(c.navText[1]).appendTo(this._controls.$relative).on("click", a.proxy(function (a) {
//       this.next(c.navSpeed)
//     }, this)), c.dotsData || (this._templates = [a('<button role="button">').addClass(c.dotClass).append(a("<span>")).prop("outerHTML")]), this._controls.$absolute = (c.dotsContainer ? a(c.dotsContainer) : a("<div>").addClass(c.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "button", a.proxy(function (b) {
//       var d = a(b.target).parent().is(this._controls.$absolute) ? a(b.target).index() : a(b.target).parent().index();
//       b.preventDefault(), this.to(d, c.dotsSpeed)
//     }, this));
//     for (b in this._overrides) this._core[b] = a.proxy(this[b], this)
//   }, e.prototype.destroy = function () {
//     var a, b, c, d, e;
//     e = this._core.settings;
//     for (a in this._handlers) this.$element.off(a, this._handlers[a]);
//     for (b in this._controls) "$relative" === b && e.navContainer ? this._controls[b].html("") : this._controls[b].remove();
//     for (d in this.overides) this._core[d] = this._overrides[d];
//     for (c in Object.getOwnPropertyNames(this)) "function" != typeof this[c] && (this[c] = null)
//   }, e.prototype.update = function () {
//     var a, b, c, d = this._core.clones().length / 2,
//       e = d + this._core.items().length,
//       f = this._core.maximum(!0),
//       g = this._core.settings,
//       h = g.center || g.autoWidth || g.dotsData ? 1 : g.dotsEach || g.items;
//     if ("page" !== g.slideBy && (g.slideBy = Math.min(g.slideBy, g.items)), g.dots || "page" == g.slideBy)
//       for (this._pages = [], a = d, b = 0, c = 0; a < e; a++) {
//         if (b >= h || 0 === b) {
//           if (this._pages.push({
//               start: Math.min(f, a - d),
//               end: a - d + h - 1
//             }), Math.min(f, a - d) === f) break;
//           b = 0, ++c
//         }
//         b += this._core.mergers(this._core.relative(a))
//       }
//   }, e.prototype.draw = function () {
//     var b, c = this._core.settings,
//       d = this._core.items().length <= c.items,
//       e = this._core.relative(this._core.current()),
//       f = c.loop || c.rewind;
//     this._controls.$relative.toggleClass("disabled", !c.nav || d), c.nav && (this._controls.$previous.toggleClass("disabled", !f && e <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !f && e >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !c.dots || d), c.dots && (b = this._pages.length - this._controls.$absolute.children().length, c.dotsData && 0 !== b ? this._controls.$absolute.html(this._templates.join("")) : b > 0 ? this._controls.$absolute.append(new Array(b + 1).join(this._templates[0])) : b < 0 && this._controls.$absolute.children().slice(b).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(a.inArray(this.current(), this._pages)).addClass("active"))
//   }, e.prototype.onTrigger = function (b) {
//     var c = this._core.settings;
//     b.page = {
//       index: a.inArray(this.current(), this._pages),
//       count: this._pages.length,
//       size: c && (c.center || c.autoWidth || c.dotsData ? 1 : c.dotsEach || c.items)
//     }
//   }, e.prototype.current = function () {
//     var b = this._core.relative(this._core.current());
//     return a.grep(this._pages, a.proxy(function (a, c) {
//       return a.start <= b && a.end >= b
//     }, this)).pop()
//   }, e.prototype.getPosition = function (b) {
//     var c, d, e = this._core.settings;
//     return "page" == e.slideBy ? (c = a.inArray(this.current(), this._pages), d = this._pages.length, b ? ++c : --c, c = this._pages[(c % d + d) % d].start) : (c = this._core.relative(this._core.current()), d = this._core.items().length, b ? c += e.slideBy : c -= e.slideBy), c
//   }, e.prototype.next = function (b) {
//     a.proxy(this._overrides.to, this._core)(this.getPosition(!0), b)
//   }, e.prototype.prev = function (b) {
//     a.proxy(this._overrides.to, this._core)(this.getPosition(!1), b)
//   }, e.prototype.to = function (b, c, d) {
//     var e;
//     !d && this._pages.length ? (e = this._pages.length, a.proxy(this._overrides.to, this._core)(this._pages[(b % e + e) % e].start, c)) : a.proxy(this._overrides.to, this._core)(b, c)
//   }, a.fn.owlCarousel.Constructor.Plugins.Navigation = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   "use strict";
//   var e = function (c) {
//     this._core = c, this._hashes = {}, this.$element = this._core.$element, this._handlers = {
//       "initialized.owl.carousel": a.proxy(function (c) {
//         c.namespace && "URLHash" === this._core.settings.startPosition && a(b).trigger("hashchange.owl.navigation")
//       }, this),
//       "prepared.owl.carousel": a.proxy(function (b) {
//         if (b.namespace) {
//           var c = a(b.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");
//           if (!c) return;
//           this._hashes[c] = b.content
//         }
//       }, this),
//       "changed.owl.carousel": a.proxy(function (c) {
//         if (c.namespace && "position" === c.property.name) {
//           var d = this._core.items(this._core.relative(this._core.current())),
//             e = a.map(this._hashes, function (a, b) {
//               return a === d ? b : null
//             }).join();
//           if (!e || b.location.hash.slice(1) === e) return;
//           b.location.hash = e
//         }
//       }, this)
//     }, this._core.options = a.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers), a(b).on("hashchange.owl.navigation", a.proxy(function (a) {
//       var c = b.location.hash.substring(1),
//         e = this._core.$stage.children(),
//         f = this._hashes[c] && e.index(this._hashes[c]);
//       f !== d && f !== this._core.current() && this._core.to(this._core.relative(f), !1, !0)
//     }, this))
//   };
//   e.Defaults = {
//     URLhashListener: !1
//   }, e.prototype.destroy = function () {
//     var c, d;
//     a(b).off("hashchange.owl.navigation");
//     for (c in this._handlers) this._core.$element.off(c, this._handlers[c]);
//     for (d in Object.getOwnPropertyNames(this)) "function" != typeof this[d] && (this[d] = null)
//   }, a.fn.owlCarousel.Constructor.Plugins.Hash = e
// }(window.Zepto || window.jQuery, window, document),
// function (a, b, c, d) {
//   function e(b, c) {
//     var e = !1,
//       f = b.charAt(0).toUpperCase() + b.slice(1);
//     return a.each((b + " " + h.join(f + " ") + f).split(" "), function (a, b) {
//       if (g[b] !== d) return e = !c || b, !1
//     }), e
//   }

//   function f(a) {
//     return e(a, !0)
//   }
//   var g = a("<support>").get(0).style,
//     h = "Webkit Moz O ms".split(" "),
//     i = {
//       transition: {
//         end: {
//           WebkitTransition: "webkitTransitionEnd",
//           MozTransition: "transitionend",
//           OTransition: "oTransitionEnd",
//           transition: "transitionend"
//         }
//       },
//       animation: {
//         end: {
//           WebkitAnimation: "webkitAnimationEnd",
//           MozAnimation: "animationend",
//           OAnimation: "oAnimationEnd",
//           animation: "animationend"
//         }
//       }
//     },
//     j = {
//       csstransforms: function () {
//         return !!e("transform")
//       },
//       csstransforms3d: function () {
//         return !!e("perspective")
//       },
//       csstransitions: function () {
//         return !!e("transition")
//       },
//       cssanimations: function () {
//         return !!e("animation")
//       }
//     };
//   j.csstransitions() && (a.support.transition = new String(f("transition")), a.support.transition.end = i.transition.end[a.support.transition]), j.cssanimations() && (a.support.animation = new String(f("animation")), a.support.animation.end = i.animation.end[a.support.animation]), j.csstransforms() && (a.support.transform = new String(f("transform")), a.support.transform3d = j.csstransforms3d())
// }(window.Zepto || window.jQuery, window, document);
/**
 * Owl Carousel v2.3.4
 * Copyright 2013-2018 David Deutsch
 * Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE
 */
/**
 * Owl carousel
 * @version 2.3.4
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 * @todo Lazy Load Icon
 * @todo prevent animationend bubling
 * @todo itemsScaleUp
 * @todo Test Zepto
 * @todo stagePadding calculate wrong active classes
 */
;(function($, window, document, undefined) {

	/**
	 * Creates a carousel.
	 * @class The Owl Carousel.
	 * @public
	 * @param {HTMLElement|jQuery} element - The element to create the carousel for.
	 * @param {Object} [options] - The options
	 */
	function Owl(element, options) {

		/**
		 * Current settings for the carousel.
		 * @public
		 */
		this.settings = null;

		/**
		 * Current options set by the caller including defaults.
		 * @public
		 */
		this.options = $.extend({}, Owl.Defaults, options);

		/**
		 * Plugin element.
		 * @public
		 */
		this.$element = $(element);

		/**
		 * Proxied event handlers.
		 * @protected
		 */
		this._handlers = {};

		/**
		 * References to the running plugins of this carousel.
		 * @protected
		 */
		this._plugins = {};

		/**
		 * Currently suppressed events to prevent them from being retriggered.
		 * @protected
		 */
		this._supress = {};

		/**
		 * Absolute current position.
		 * @protected
		 */
		this._current = null;

		/**
		 * Animation speed in milliseconds.
		 * @protected
		 */
		this._speed = null;

		/**
		 * Coordinates of all items in pixel.
		 * @todo The name of this member is missleading.
		 * @protected
		 */
		this._coordinates = [];

		/**
		 * Current breakpoint.
		 * @todo Real media queries would be nice.
		 * @protected
		 */
		this._breakpoint = null;

		/**
		 * Current width of the plugin element.
		 */
		this._width = null;

		/**
		 * All real items.
		 * @protected
		 */
		this._items = [];

		/**
		 * All cloned items.
		 * @protected
		 */
		this._clones = [];

		/**
		 * Merge values of all items.
		 * @todo Maybe this could be part of a plugin.
		 * @protected
		 */
		this._mergers = [];

		/**
		 * Widths of all items.
		 */
		this._widths = [];

		/**
		 * Invalidated parts within the update process.
		 * @protected
		 */
		this._invalidated = {};

		/**
		 * Ordered list of workers for the update process.
		 * @protected
		 */
		this._pipe = [];

		/**
		 * Current state information for the drag operation.
		 * @todo #261
		 * @protected
		 */
		this._drag = {
			time: null,
			target: null,
			pointer: null,
			stage: {
				start: null,
				current: null
			},
			direction: null
		};

		/**
		 * Current state information and their tags.
		 * @type {Object}
		 * @protected
		 */
		this._states = {
			current: {},
			tags: {
				'initializing': [ 'busy' ],
				'animating': [ 'busy' ],
				'dragging': [ 'interacting' ]
			}
		};

		$.each([ 'onResize', 'onThrottledResize' ], $.proxy(function(i, handler) {
			this._handlers[handler] = $.proxy(this[handler], this);
		}, this));

		$.each(Owl.Plugins, $.proxy(function(key, plugin) {
			this._plugins[key.charAt(0).toLowerCase() + key.slice(1)]
				= new plugin(this);
		}, this));

		$.each(Owl.Workers, $.proxy(function(priority, worker) {
			this._pipe.push({
				'filter': worker.filter,
				'run': $.proxy(worker.run, this)
			});
		}, this));

		this.setup();
		this.initialize();
	}

	/**
	 * Default options for the carousel.
	 * @public
	 */
	Owl.Defaults = {
		items: 3,
		loop: false,
		center: false,
		rewind: false,
		checkVisibility: true,

		mouseDrag: true,
		touchDrag: true,
		pullDrag: true,
		freeDrag: false,

		margin: 0,
		stagePadding: 0,

		merge: false,
		mergeFit: true,
		autoWidth: false,

		startPosition: 0,
		rtl: false,

		smartSpeed: 250,
		fluidSpeed: false,
		dragEndSpeed: false,

		responsive: {},
		responsiveRefreshRate: 200,
		responsiveBaseElement: window,

		fallbackEasing: 'swing',
		slideTransition: '',

		info: false,

		nestedItemSelector: false,
		itemElement: 'div',
		stageElement: 'div',

		refreshClass: 'owl-refresh',
		loadedClass: 'owl-loaded',
		loadingClass: 'owl-loading',
		rtlClass: 'owl-rtl',
		responsiveClass: 'owl-responsive',
		dragClass: 'owl-drag',
		itemClass: 'owl-item',
		stageClass: 'owl-stage',
		stageOuterClass: 'owl-stage-outer',
		grabClass: 'owl-grab'
	};

	/**
	 * Enumeration for width.
	 * @public
	 * @readonly
	 * @enum {String}
	 */
	Owl.Width = {
		Default: 'default',
		Inner: 'inner',
		Outer: 'outer'
	};

	/**
	 * Enumeration for types.
	 * @public
	 * @readonly
	 * @enum {String}
	 */
	Owl.Type = {
		Event: 'event',
		State: 'state'
	};

	/**
	 * Contains all registered plugins.
	 * @public
	 */
	Owl.Plugins = {};

	/**
	 * List of workers involved in the update process.
	 */
	Owl.Workers = [ {
		filter: [ 'width', 'settings' ],
		run: function() {
			this._width = this.$element.width();
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			cache.current = this._items && this._items[this.relative(this._current)];
		}
	}, {
		filter: [ 'items', 'settings' ],
		run: function() {
			this.$stage.children('.cloned').remove();
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			var margin = this.settings.margin || '',
				grid = !this.settings.autoWidth,
				rtl = this.settings.rtl,
				css = {
					'width': 'auto',
					'margin-left': rtl ? margin : '',
					'margin-right': rtl ? '' : margin
				};

			!grid && this.$stage.children().css(css);

			cache.css = css;
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			var width = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
				merge = null,
				iterator = this._items.length,
				grid = !this.settings.autoWidth,
				widths = [];

			cache.items = {
				merge: false,
				width: width
			};

			while (iterator--) {
				merge = this._mergers[iterator];
				merge = this.settings.mergeFit && Math.min(merge, this.settings.items) || merge;

				cache.items.merge = merge > 1 || cache.items.merge;

				widths[iterator] = !grid ? this._items[iterator].width() : width * merge;
			}

			this._widths = widths;
		}
	}, {
		filter: [ 'items', 'settings' ],
		run: function() {
			var clones = [],
				items = this._items,
				settings = this.settings,
				// TODO: Should be computed from number of min width items in stage
				view = Math.max(settings.items * 2, 4),
				size = Math.ceil(items.length / 2) * 2,
				repeat = settings.loop && items.length ? settings.rewind ? view : Math.max(view, size) : 0,
				append = '',
				prepend = '';

			repeat /= 2;

			while (repeat > 0) {
				// Switch to only using appended clones
				clones.push(this.normalize(clones.length / 2, true));
				append = append + items[clones[clones.length - 1]][0].outerHTML;
				clones.push(this.normalize(items.length - 1 - (clones.length - 1) / 2, true));
				prepend = items[clones[clones.length - 1]][0].outerHTML + prepend;
				repeat -= 1;
			}

			this._clones = clones;

			$(append).addClass('cloned').appendTo(this.$stage);
			$(prepend).addClass('cloned').prependTo(this.$stage);
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function() {
			var rtl = this.settings.rtl ? 1 : -1,
				size = this._clones.length + this._items.length,
				iterator = -1,
				previous = 0,
				current = 0,
				coordinates = [];

			while (++iterator < size) {
				previous = coordinates[iterator - 1] || 0;
				current = this._widths[this.relative(iterator)] + this.settings.margin;
				coordinates.push(previous + current * rtl);
			}

			this._coordinates = coordinates;
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function() {
			var padding = this.settings.stagePadding,
				coordinates = this._coordinates,
				css = {
					'width': Math.ceil(Math.abs(coordinates[coordinates.length - 1])) + padding * 2,
					'padding-left': padding || '',
					'padding-right': padding || ''
				};

			this.$stage.css(css);
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			var iterator = this._coordinates.length,
				grid = !this.settings.autoWidth,
				items = this.$stage.children();

			if (grid && cache.items.merge) {
				while (iterator--) {
					cache.css.width = this._widths[this.relative(iterator)];
					items.eq(iterator).css(cache.css);
				}
			} else if (grid) {
				cache.css.width = cache.items.width;
				items.css(cache.css);
			}
		}
	}, {
		filter: [ 'items' ],
		run: function() {
			this._coordinates.length < 1 && this.$stage.removeAttr('style');
		}
	}, {
		filter: [ 'width', 'items', 'settings' ],
		run: function(cache) {
			cache.current = cache.current ? this.$stage.children().index(cache.current) : 0;
			cache.current = Math.max(this.minimum(), Math.min(this.maximum(), cache.current));
			this.reset(cache.current);
		}
	}, {
		filter: [ 'position' ],
		run: function() {
			this.animate(this.coordinates(this._current));
		}
	}, {
		filter: [ 'width', 'position', 'items', 'settings' ],
		run: function() {
			var rtl = this.settings.rtl ? 1 : -1,
				padding = this.settings.stagePadding * 2,
				begin = this.coordinates(this.current()) + padding,
				end = begin + this.width() * rtl,
				inner, outer, matches = [], i, n;

			for (i = 0, n = this._coordinates.length; i < n; i++) {
				inner = this._coordinates[i - 1] || 0;
				outer = Math.abs(this._coordinates[i]) + padding * rtl;

				if ((this.op(inner, '<=', begin) && (this.op(inner, '>', end)))
					|| (this.op(outer, '<', begin) && this.op(outer, '>', end))) {
					matches.push(i);
				}
			}

			this.$stage.children('.active').removeClass('active');
			this.$stage.children(':eq(' + matches.join('), :eq(') + ')').addClass('active');

			this.$stage.children('.center').removeClass('center');
			if (this.settings.center) {
				this.$stage.children().eq(this.current()).addClass('center');
			}
		}
	} ];

	/**
	 * Create the stage DOM element
	 */
	Owl.prototype.initializeStage = function() {
		this.$stage = this.$element.find('.' + this.settings.stageClass);

		// if the stage is already in the DOM, grab it and skip stage initialization
		if (this.$stage.length) {
			return;
		}

		this.$element.addClass(this.options.loadingClass);

		// create stage
		this.$stage = $('<' + this.settings.stageElement + '>', {
			"class": this.settings.stageClass
		}).wrap( $( '<div/>', {
			"class": this.settings.stageOuterClass
		}));

		// append stage
		this.$element.append(this.$stage.parent());
	};

	/**
	 * Create item DOM elements
	 */
	Owl.prototype.initializeItems = function() {
		var $items = this.$element.find('.owl-item');

		// if the items are already in the DOM, grab them and skip item initialization
		if ($items.length) {
			this._items = $items.get().map(function(item) {
				return $(item);
			});

			this._mergers = this._items.map(function() {
				return 1;
			});

			this.refresh();

			return;
		}

		// append content
		this.replace(this.$element.children().not(this.$stage.parent()));

		// check visibility
		if (this.isVisible()) {
			// update view
			this.refresh();
		} else {
			// invalidate width
			this.invalidate('width');
		}

		this.$element
			.removeClass(this.options.loadingClass)
			.addClass(this.options.loadedClass);
	};

	/**
	 * Initializes the carousel.
	 * @protected
	 */
	Owl.prototype.initialize = function() {
		this.enter('initializing');
		this.trigger('initialize');

		this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl);

		if (this.settings.autoWidth && !this.is('pre-loading')) {
			var imgs, nestedSelector, width;
			imgs = this.$element.find('img');
			nestedSelector = this.settings.nestedItemSelector ? '.' + this.settings.nestedItemSelector : undefined;
			width = this.$element.children(nestedSelector).width();

			if (imgs.length && width <= 0) {
				this.preloadAutoWidthImages(imgs);
			}
		}

		this.initializeStage();
		this.initializeItems();

		// register event handlers
		this.registerEventHandlers();

		this.leave('initializing');
		this.trigger('initialized');
	};

	/**
	 * @returns {Boolean} visibility of $element
	 *                    if you know the carousel will always be visible you can set `checkVisibility` to `false` to
	 *                    prevent the expensive browser layout forced reflow the $element.is(':visible') does
	 */
	Owl.prototype.isVisible = function() {
		return this.settings.checkVisibility
			? this.$element.is(':visible')
			: true;
	};

	/**
	 * Setups the current settings.
	 * @todo Remove responsive classes. Why should adaptive designs be brought into IE8?
	 * @todo Support for media queries by using `matchMedia` would be nice.
	 * @public
	 */
	Owl.prototype.setup = function() {
		var viewport = this.viewport(),
			overwrites = this.options.responsive,
			match = -1,
			settings = null;

		if (!overwrites) {
			settings = $.extend({}, this.options);
		} else {
			$.each(overwrites, function(breakpoint) {
				if (breakpoint <= viewport && breakpoint > match) {
					match = Number(breakpoint);
				}
			});

			settings = $.extend({}, this.options, overwrites[match]);
			if (typeof settings.stagePadding === 'function') {
				settings.stagePadding = settings.stagePadding();
			}
			delete settings.responsive;

			// responsive class
			if (settings.responsiveClass) {
				this.$element.attr('class',
					this.$element.attr('class').replace(new RegExp('(' + this.options.responsiveClass + '-)\\S+\\s', 'g'), '$1' + match)
				);
			}
		}

		this.trigger('change', { property: { name: 'settings', value: settings } });
		this._breakpoint = match;
		this.settings = settings;
		this.invalidate('settings');
		this.trigger('changed', { property: { name: 'settings', value: this.settings } });
	};

	/**
	 * Updates option logic if necessery.
	 * @protected
	 */
	Owl.prototype.optionsLogic = function() {
		if (this.settings.autoWidth) {
			this.settings.stagePadding = false;
			this.settings.merge = false;
		}
	};

	/**
	 * Prepares an item before add.
	 * @todo Rename event parameter `content` to `item`.
	 * @protected
	 * @returns {jQuery|HTMLElement} - The item container.
	 */
	Owl.prototype.prepare = function(item) {
		var event = this.trigger('prepare', { content: item });

		if (!event.data) {
			event.data = $('<' + this.settings.itemElement + '/>')
				.addClass(this.options.itemClass).append(item)
		}

		this.trigger('prepared', { content: event.data });

		return event.data;
	};

	/**
	 * Updates the view.
	 * @public
	 */
	Owl.prototype.update = function() {
		var i = 0,
			n = this._pipe.length,
			filter = $.proxy(function(p) { return this[p] }, this._invalidated),
			cache = {};

		while (i < n) {
			if (this._invalidated.all || $.grep(this._pipe[i].filter, filter).length > 0) {
				this._pipe[i].run(cache);
			}
			i++;
		}

		this._invalidated = {};

		!this.is('valid') && this.enter('valid');
	};

	/**
	 * Gets the width of the view.
	 * @public
	 * @param {Owl.Width} [dimension=Owl.Width.Default] - The dimension to return.
	 * @returns {Number} - The width of the view in pixel.
	 */
	Owl.prototype.width = function(dimension) {
		dimension = dimension || Owl.Width.Default;
		switch (dimension) {
			case Owl.Width.Inner:
			case Owl.Width.Outer:
				return this._width;
			default:
				return this._width - this.settings.stagePadding * 2 + this.settings.margin;
		}
	};

	/**
	 * Refreshes the carousel primarily for adaptive purposes.
	 * @public
	 */
	Owl.prototype.refresh = function() {
		this.enter('refreshing');
		this.trigger('refresh');

		this.setup();

		this.optionsLogic();

		this.$element.addClass(this.options.refreshClass);

		this.update();

		this.$element.removeClass(this.options.refreshClass);

		this.leave('refreshing');
		this.trigger('refreshed');
	};

	/**
	 * Checks window `resize` event.
	 * @protected
	 */
	Owl.prototype.onThrottledResize = function() {
		window.clearTimeout(this.resizeTimer);
		this.resizeTimer = window.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
	};

	/**
	 * Checks window `resize` event.
	 * @protected
	 */
	Owl.prototype.onResize = function() {
		if (!this._items.length) {
			return false;
		}

		if (this._width === this.$element.width()) {
			return false;
		}

		if (!this.isVisible()) {
			return false;
		}

		this.enter('resizing');

		if (this.trigger('resize').isDefaultPrevented()) {
			this.leave('resizing');
			return false;
		}

		this.invalidate('width');

		this.refresh();

		this.leave('resizing');
		this.trigger('resized');
	};

	/**
	 * Registers event handlers.
	 * @todo Check `msPointerEnabled`
	 * @todo #261
	 * @protected
	 */
	Owl.prototype.registerEventHandlers = function() {
		if ($.support.transition) {
			this.$stage.on($.support.transition.end + '.owl.core', $.proxy(this.onTransitionEnd, this));
		}

		if (this.settings.responsive !== false) {
			this.on(window, 'resize', this._handlers.onThrottledResize);
		}

		if (this.settings.mouseDrag) {
			this.$element.addClass(this.options.dragClass);
			this.$stage.on('mousedown.owl.core', $.proxy(this.onDragStart, this));
			this.$stage.on('dragstart.owl.core selectstart.owl.core', function() { return false });
		}

		if (this.settings.touchDrag){
			this.$stage.on('touchstart.owl.core', $.proxy(this.onDragStart, this));
			this.$stage.on('touchcancel.owl.core', $.proxy(this.onDragEnd, this));
		}
	};

	/**
	 * Handles `touchstart` and `mousedown` events.
	 * @todo Horizontal swipe threshold as option
	 * @todo #261
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragStart = function(event) {
		var stage = null;

		if (event.which === 3) {
			return;
		}

		if ($.support.transform) {
			stage = this.$stage.css('transform').replace(/.*\(|\)| /g, '').split(',');
			stage = {
				x: stage[stage.length === 16 ? 12 : 4],
				y: stage[stage.length === 16 ? 13 : 5]
			};
		} else {
			stage = this.$stage.position();
			stage = {
				x: this.settings.rtl ?
					stage.left + this.$stage.width() - this.width() + this.settings.margin :
					stage.left,
				y: stage.top
			};
		}

		if (this.is('animating')) {
			$.support.transform ? this.animate(stage.x) : this.$stage.stop()
			this.invalidate('position');
		}

		this.$element.toggleClass(this.options.grabClass, event.type === 'mousedown');

		this.speed(0);

		this._drag.time = new Date().getTime();
		this._drag.target = $(event.target);
		this._drag.stage.start = stage;
		this._drag.stage.current = stage;
		this._drag.pointer = this.pointer(event);

		$(document).on('mouseup.owl.core touchend.owl.core', $.proxy(this.onDragEnd, this));

		$(document).one('mousemove.owl.core touchmove.owl.core', $.proxy(function(event) {
			var delta = this.difference(this._drag.pointer, this.pointer(event));

			$(document).on('mousemove.owl.core touchmove.owl.core', $.proxy(this.onDragMove, this));

			if (Math.abs(delta.x) < Math.abs(delta.y) && this.is('valid')) {
				return;
			}

			event.preventDefault();

			this.enter('dragging');
			this.trigger('drag');
		}, this));
	};

	/**
	 * Handles the `touchmove` and `mousemove` events.
	 * @todo #261
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragMove = function(event) {
		var minimum = null,
			maximum = null,
			pull = null,
			delta = this.difference(this._drag.pointer, this.pointer(event)),
			stage = this.difference(this._drag.stage.start, delta);

		if (!this.is('dragging')) {
			return;
		}

		event.preventDefault();

		if (this.settings.loop) {
			minimum = this.coordinates(this.minimum());
			maximum = this.coordinates(this.maximum() + 1) - minimum;
			stage.x = (((stage.x - minimum) % maximum + maximum) % maximum) + minimum;
		} else {
			minimum = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum());
			maximum = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum());
			pull = this.settings.pullDrag ? -1 * delta.x / 5 : 0;
			stage.x = Math.max(Math.min(stage.x, minimum + pull), maximum + pull);
		}

		this._drag.stage.current = stage;

		this.animate(stage.x);
	};

	/**
	 * Handles the `touchend` and `mouseup` events.
	 * @todo #261
	 * @todo Threshold for click event
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onDragEnd = function(event) {
		var delta = this.difference(this._drag.pointer, this.pointer(event)),
			stage = this._drag.stage.current,
			direction = delta.x > 0 ^ this.settings.rtl ? 'left' : 'right';

		$(document).off('.owl.core');

		this.$element.removeClass(this.options.grabClass);

		if (delta.x !== 0 && this.is('dragging') || !this.is('valid')) {
			this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed);
			this.current(this.closest(stage.x, delta.x !== 0 ? direction : this._drag.direction));
			this.invalidate('position');
			this.update();

			this._drag.direction = direction;

			if (Math.abs(delta.x) > 3 || new Date().getTime() - this._drag.time > 300) {
				this._drag.target.one('click.owl.core', function() { return false; });
			}
		}

		if (!this.is('dragging')) {
			return;
		}

		this.leave('dragging');
		this.trigger('dragged');
	};

	/**
	 * Gets absolute position of the closest item for a coordinate.
	 * @todo Setting `freeDrag` makes `closest` not reusable. See #165.
	 * @protected
	 * @param {Number} coordinate - The coordinate in pixel.
	 * @param {String} direction - The direction to check for the closest item. Ether `left` or `right`.
	 * @return {Number} - The absolute position of the closest item.
	 */
	Owl.prototype.closest = function(coordinate, direction) {
		var position = -1,
			pull = 30,
			width = this.width(),
			coordinates = this.coordinates();

		if (!this.settings.freeDrag) {
			// check closest item
			$.each(coordinates, $.proxy(function(index, value) {
				// on a left pull, check on current index
				if (direction === 'left' && coordinate > value - pull && coordinate < value + pull) {
					position = index;
				// on a right pull, check on previous index
				// to do so, subtract width from value and set position = index + 1
				} else if (direction === 'right' && coordinate > value - width - pull && coordinate < value - width + pull) {
					position = index + 1;
				} else if (this.op(coordinate, '<', value)
					&& this.op(coordinate, '>', coordinates[index + 1] !== undefined ? coordinates[index + 1] : value - width)) {
					position = direction === 'left' ? index + 1 : index;
				}
				return position === -1;
			}, this));
		}

		if (!this.settings.loop) {
			// non loop boundries
			if (this.op(coordinate, '>', coordinates[this.minimum()])) {
				position = coordinate = this.minimum();
			} else if (this.op(coordinate, '<', coordinates[this.maximum()])) {
				position = coordinate = this.maximum();
			}
		}

		return position;
	};

	/**
	 * Animates the stage.
	 * @todo #270
	 * @public
	 * @param {Number} coordinate - The coordinate in pixels.
	 */
	Owl.prototype.animate = function(coordinate) {
		var animate = this.speed() > 0;

		this.is('animating') && this.onTransitionEnd();

		if (animate) {
			this.enter('animating');
			this.trigger('translate');
		}

		if ($.support.transform3d && $.support.transition) {
			this.$stage.css({
				transform: 'translate3d(' + coordinate + 'px,0px,0px)',
				transition: (this.speed() / 1000) + 's' + (
					this.settings.slideTransition ? ' ' + this.settings.slideTransition : ''
				)
			});
		} else if (animate) {
			this.$stage.animate({
				left: coordinate + 'px'
			}, this.speed(), this.settings.fallbackEasing, $.proxy(this.onTransitionEnd, this));
		} else {
			this.$stage.css({
				left: coordinate + 'px'
			});
		}
	};

	/**
	 * Checks whether the carousel is in a specific state or not.
	 * @param {String} state - The state to check.
	 * @returns {Boolean} - The flag which indicates if the carousel is busy.
	 */
	Owl.prototype.is = function(state) {
		return this._states.current[state] && this._states.current[state] > 0;
	};

	/**
	 * Sets the absolute position of the current item.
	 * @public
	 * @param {Number} [position] - The new absolute position or nothing to leave it unchanged.
	 * @returns {Number} - The absolute position of the current item.
	 */
	Owl.prototype.current = function(position) {
		if (position === undefined) {
			return this._current;
		}

		if (this._items.length === 0) {
			return undefined;
		}

		position = this.normalize(position);

		if (this._current !== position) {
			var event = this.trigger('change', { property: { name: 'position', value: position } });

			if (event.data !== undefined) {
				position = this.normalize(event.data);
			}

			this._current = position;

			this.invalidate('position');

			this.trigger('changed', { property: { name: 'position', value: this._current } });
		}

		return this._current;
	};

	/**
	 * Invalidates the given part of the update routine.
	 * @param {String} [part] - The part to invalidate.
	 * @returns {Array.<String>} - The invalidated parts.
	 */
	Owl.prototype.invalidate = function(part) {
		if ($.type(part) === 'string') {
			this._invalidated[part] = true;
			this.is('valid') && this.leave('valid');
		}
		return $.map(this._invalidated, function(v, i) { return i });
	};

	/**
	 * Resets the absolute position of the current item.
	 * @public
	 * @param {Number} position - The absolute position of the new item.
	 */
	Owl.prototype.reset = function(position) {
		position = this.normalize(position);

		if (position === undefined) {
			return;
		}

		this._speed = 0;
		this._current = position;

		this.suppress([ 'translate', 'translated' ]);

		this.animate(this.coordinates(position));

		this.release([ 'translate', 'translated' ]);
	};

	/**
	 * Normalizes an absolute or a relative position of an item.
	 * @public
	 * @param {Number} position - The absolute or relative position to normalize.
	 * @param {Boolean} [relative=false] - Whether the given position is relative or not.
	 * @returns {Number} - The normalized position.
	 */
	Owl.prototype.normalize = function(position, relative) {
		var n = this._items.length,
			m = relative ? 0 : this._clones.length;

		if (!this.isNumeric(position) || n < 1) {
			position = undefined;
		} else if (position < 0 || position >= n + m) {
			position = ((position - m / 2) % n + n) % n + m / 2;
		}

		return position;
	};

	/**
	 * Converts an absolute position of an item into a relative one.
	 * @public
	 * @param {Number} position - The absolute position to convert.
	 * @returns {Number} - The converted position.
	 */
	Owl.prototype.relative = function(position) {
		position -= this._clones.length / 2;
		return this.normalize(position, true);
	};

	/**
	 * Gets the maximum position for the current item.
	 * @public
	 * @param {Boolean} [relative=false] - Whether to return an absolute position or a relative position.
	 * @returns {Number}
	 */
	Owl.prototype.maximum = function(relative) {
		var settings = this.settings,
			maximum = this._coordinates.length,
			iterator,
			reciprocalItemsWidth,
			elementWidth;

		if (settings.loop) {
			maximum = this._clones.length / 2 + this._items.length - 1;
		} else if (settings.autoWidth || settings.merge) {
			iterator = this._items.length;
			if (iterator) {
				reciprocalItemsWidth = this._items[--iterator].width();
				elementWidth = this.$element.width();
				while (iterator--) {
					reciprocalItemsWidth += this._items[iterator].width() + this.settings.margin;
					if (reciprocalItemsWidth > elementWidth) {
						break;
					}
				}
			}
			maximum = iterator + 1;
		} else if (settings.center) {
			maximum = this._items.length - 1;
		} else {
			maximum = this._items.length - settings.items;
		}

		if (relative) {
			maximum -= this._clones.length / 2;
		}

		return Math.max(maximum, 0);
	};

	/**
	 * Gets the minimum position for the current item.
	 * @public
	 * @param {Boolean} [relative=false] - Whether to return an absolute position or a relative position.
	 * @returns {Number}
	 */
	Owl.prototype.minimum = function(relative) {
		return relative ? 0 : this._clones.length / 2;
	};

	/**
	 * Gets an item at the specified relative position.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @return {jQuery|Array.<jQuery>} - The item at the given position or all items if no position was given.
	 */
	Owl.prototype.items = function(position) {
		if (position === undefined) {
			return this._items.slice();
		}

		position = this.normalize(position, true);
		return this._items[position];
	};

	/**
	 * Gets an item at the specified relative position.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @return {jQuery|Array.<jQuery>} - The item at the given position or all items if no position was given.
	 */
	Owl.prototype.mergers = function(position) {
		if (position === undefined) {
			return this._mergers.slice();
		}

		position = this.normalize(position, true);
		return this._mergers[position];
	};

	/**
	 * Gets the absolute positions of clones for an item.
	 * @public
	 * @param {Number} [position] - The relative position of the item.
	 * @returns {Array.<Number>} - The absolute positions of clones for the item or all if no position was given.
	 */
	Owl.prototype.clones = function(position) {
		var odd = this._clones.length / 2,
			even = odd + this._items.length,
			map = function(index) { return index % 2 === 0 ? even + index / 2 : odd - (index + 1) / 2 };

		if (position === undefined) {
			return $.map(this._clones, function(v, i) { return map(i) });
		}

		return $.map(this._clones, function(v, i) { return v === position ? map(i) : null });
	};

	/**
	 * Sets the current animation speed.
	 * @public
	 * @param {Number} [speed] - The animation speed in milliseconds or nothing to leave it unchanged.
	 * @returns {Number} - The current animation speed in milliseconds.
	 */
	Owl.prototype.speed = function(speed) {
		if (speed !== undefined) {
			this._speed = speed;
		}

		return this._speed;
	};

	/**
	 * Gets the coordinate of an item.
	 * @todo The name of this method is missleanding.
	 * @public
	 * @param {Number} position - The absolute position of the item within `minimum()` and `maximum()`.
	 * @returns {Number|Array.<Number>} - The coordinate of the item in pixel or all coordinates.
	 */
	Owl.prototype.coordinates = function(position) {
		var multiplier = 1,
			newPosition = position - 1,
			coordinate;

		if (position === undefined) {
			return $.map(this._coordinates, $.proxy(function(coordinate, index) {
				return this.coordinates(index);
			}, this));
		}

		if (this.settings.center) {
			if (this.settings.rtl) {
				multiplier = -1;
				newPosition = position + 1;
			}

			coordinate = this._coordinates[position];
			coordinate += (this.width() - coordinate + (this._coordinates[newPosition] || 0)) / 2 * multiplier;
		} else {
			coordinate = this._coordinates[newPosition] || 0;
		}

		coordinate = Math.ceil(coordinate);

		return coordinate;
	};

	/**
	 * Calculates the speed for a translation.
	 * @protected
	 * @param {Number} from - The absolute position of the start item.
	 * @param {Number} to - The absolute position of the target item.
	 * @param {Number} [factor=undefined] - The time factor in milliseconds.
	 * @returns {Number} - The time in milliseconds for the translation.
	 */
	Owl.prototype.duration = function(from, to, factor) {
		if (factor === 0) {
			return 0;
		}

		return Math.min(Math.max(Math.abs(to - from), 1), 6) * Math.abs((factor || this.settings.smartSpeed));
	};

	/**
	 * Slides to the specified item.
	 * @public
	 * @param {Number} position - The position of the item.
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.to = function(position, speed) {
		var current = this.current(),
			revert = null,
			distance = position - this.relative(current),
			direction = (distance > 0) - (distance < 0),
			items = this._items.length,
			minimum = this.minimum(),
			maximum = this.maximum();

		if (this.settings.loop) {
			if (!this.settings.rewind && Math.abs(distance) > items / 2) {
				distance += direction * -1 * items;
			}

			position = current + distance;
			revert = ((position - minimum) % items + items) % items + minimum;

			if (revert !== position && revert - distance <= maximum && revert - distance > 0) {
				current = revert - distance;
				position = revert;
				this.reset(current);
			}
		} else if (this.settings.rewind) {
			maximum += 1;
			position = (position % maximum + maximum) % maximum;
		} else {
			position = Math.max(minimum, Math.min(maximum, position));
		}

		this.speed(this.duration(current, position, speed));
		this.current(position);

		if (this.isVisible()) {
			this.update();
		}
	};

	/**
	 * Slides to the next item.
	 * @public
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.next = function(speed) {
		speed = speed || false;
		this.to(this.relative(this.current()) + 1, speed);
	};

	/**
	 * Slides to the previous item.
	 * @public
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 */
	Owl.prototype.prev = function(speed) {
		speed = speed || false;
		this.to(this.relative(this.current()) - 1, speed);
	};

	/**
	 * Handles the end of an animation.
	 * @protected
	 * @param {Event} event - The event arguments.
	 */
	Owl.prototype.onTransitionEnd = function(event) {

		// if css2 animation then event object is undefined
		if (event !== undefined) {
			event.stopPropagation();

			// Catch only owl-stage transitionEnd event
			if ((event.target || event.srcElement || event.originalTarget) !== this.$stage.get(0)) {
				return false;
			}
		}

		this.leave('animating');
		this.trigger('translated');
	};

	/**
	 * Gets viewport width.
	 * @protected
	 * @return {Number} - The width in pixel.
	 */
	Owl.prototype.viewport = function() {
		var width;
		if (this.options.responsiveBaseElement !== window) {
			width = $(this.options.responsiveBaseElement).width();
		} else if (window.innerWidth) {
			width = window.innerWidth;
		} else if (document.documentElement && document.documentElement.clientWidth) {
			width = document.documentElement.clientWidth;
		} else {
			console.warn('Can not detect viewport width.');
		}
		return width;
	};

	/**
	 * Replaces the current content.
	 * @public
	 * @param {HTMLElement|jQuery|String} content - The new content.
	 */
	Owl.prototype.replace = function(content) {
		this.$stage.empty();
		this._items = [];

		if (content) {
			content = (content instanceof jQuery) ? content : $(content);
		}

		if (this.settings.nestedItemSelector) {
			content = content.find('.' + this.settings.nestedItemSelector);
		}

		content.filter(function() {
			return this.nodeType === 1;
		}).each($.proxy(function(index, item) {
			item = this.prepare(item);
			this.$stage.append(item);
			this._items.push(item);
			this._mergers.push(item.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		}, this));

		this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0);

		this.invalidate('items');
	};

	/**
	 * Adds an item.
	 * @todo Use `item` instead of `content` for the event arguments.
	 * @public
	 * @param {HTMLElement|jQuery|String} content - The item content to add.
	 * @param {Number} [position] - The relative position at which to insert the item otherwise the item will be added to the end.
	 */
	Owl.prototype.add = function(content, position) {
		var current = this.relative(this._current);

		position = position === undefined ? this._items.length : this.normalize(position, true);
		content = content instanceof jQuery ? content : $(content);

		this.trigger('add', { content: content, position: position });

		content = this.prepare(content);

		if (this._items.length === 0 || position === this._items.length) {
			this._items.length === 0 && this.$stage.append(content);
			this._items.length !== 0 && this._items[position - 1].after(content);
			this._items.push(content);
			this._mergers.push(content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		} else {
			this._items[position].before(content);
			this._items.splice(position, 0, content);
			this._mergers.splice(position, 0, content.find('[data-merge]').addBack('[data-merge]').attr('data-merge') * 1 || 1);
		}

		this._items[current] && this.reset(this._items[current].index());

		this.invalidate('items');

		this.trigger('added', { content: content, position: position });
	};

	/**
	 * Removes an item by its position.
	 * @todo Use `item` instead of `content` for the event arguments.
	 * @public
	 * @param {Number} position - The relative position of the item to remove.
	 */
	Owl.prototype.remove = function(position) {
		position = this.normalize(position, true);

		if (position === undefined) {
			return;
		}

		this.trigger('remove', { content: this._items[position], position: position });

		this._items[position].remove();
		this._items.splice(position, 1);
		this._mergers.splice(position, 1);

		this.invalidate('items');

		this.trigger('removed', { content: null, position: position });
	};

	/**
	 * Preloads images with auto width.
	 * @todo Replace by a more generic approach
	 * @protected
	 */
	Owl.prototype.preloadAutoWidthImages = function(images) {
		images.each($.proxy(function(i, element) {
			this.enter('pre-loading');
			element = $(element);
			$(new Image()).one('load', $.proxy(function(e) {
				element.attr('src', e.target.src);
				element.css('opacity', 1);
				this.leave('pre-loading');
				!this.is('pre-loading') && !this.is('initializing') && this.refresh();
			}, this)).attr('src', element.attr('src') || element.attr('data-src') || element.attr('data-src-retina'));
		}, this));
	};

	/**
	 * Destroys the carousel.
	 * @public
	 */
	Owl.prototype.destroy = function() {

		this.$element.off('.owl.core');
		this.$stage.off('.owl.core');
		$(document).off('.owl.core');

		if (this.settings.responsive !== false) {
			window.clearTimeout(this.resizeTimer);
			this.off(window, 'resize', this._handlers.onThrottledResize);
		}

		for (var i in this._plugins) {
			this._plugins[i].destroy();
		}

		this.$stage.children('.cloned').remove();

		this.$stage.unwrap();
		this.$stage.children().contents().unwrap();
		this.$stage.children().unwrap();
		this.$stage.remove();
		this.$element
			.removeClass(this.options.refreshClass)
			.removeClass(this.options.loadingClass)
			.removeClass(this.options.loadedClass)
			.removeClass(this.options.rtlClass)
			.removeClass(this.options.dragClass)
			.removeClass(this.options.grabClass)
			.attr('class', this.$element.attr('class').replace(new RegExp(this.options.responsiveClass + '-\\S+\\s', 'g'), ''))
			.removeData('owl.carousel');
	};

	/**
	 * Operators to calculate right-to-left and left-to-right.
	 * @protected
	 * @param {Number} [a] - The left side operand.
	 * @param {String} [o] - The operator.
	 * @param {Number} [b] - The right side operand.
	 */
	Owl.prototype.op = function(a, o, b) {
		var rtl = this.settings.rtl;
		switch (o) {
			case '<':
				return rtl ? a > b : a < b;
			case '>':
				return rtl ? a < b : a > b;
			case '>=':
				return rtl ? a <= b : a >= b;
			case '<=':
				return rtl ? a >= b : a <= b;
			default:
				break;
		}
	};

	/**
	 * Attaches to an internal event.
	 * @protected
	 * @param {HTMLElement} element - The event source.
	 * @param {String} event - The event name.
	 * @param {Function} listener - The event handler to attach.
	 * @param {Boolean} capture - Wether the event should be handled at the capturing phase or not.
	 */
	Owl.prototype.on = function(element, event, listener, capture) {
		if (element.addEventListener) {
			element.addEventListener(event, listener, capture);
		} else if (element.attachEvent) {
			element.attachEvent('on' + event, listener);
		}
	};

	/**
	 * Detaches from an internal event.
	 * @protected
	 * @param {HTMLElement} element - The event source.
	 * @param {String} event - The event name.
	 * @param {Function} listener - The attached event handler to detach.
	 * @param {Boolean} capture - Wether the attached event handler was registered as a capturing listener or not.
	 */
	Owl.prototype.off = function(element, event, listener, capture) {
		if (element.removeEventListener) {
			element.removeEventListener(event, listener, capture);
		} else if (element.detachEvent) {
			element.detachEvent('on' + event, listener);
		}
	};

	/**
	 * Triggers a public event.
	 * @todo Remove `status`, `relatedTarget` should be used instead.
	 * @protected
	 * @param {String} name - The event name.
	 * @param {*} [data=null] - The event data.
	 * @param {String} [namespace=carousel] - The event namespace.
	 * @param {String} [state] - The state which is associated with the event.
	 * @param {Boolean} [enter=false] - Indicates if the call enters the specified state or not.
	 * @returns {Event} - The event arguments.
	 */
	Owl.prototype.trigger = function(name, data, namespace, state, enter) {
		var status = {
			item: { count: this._items.length, index: this.current() }
		}, handler = $.camelCase(
			$.grep([ 'on', name, namespace ], function(v) { return v })
				.join('-').toLowerCase()
		), event = $.Event(
			[ name, 'owl', namespace || 'carousel' ].join('.').toLowerCase(),
			$.extend({ relatedTarget: this }, status, data)
		);

		if (!this._supress[name]) {
			$.each(this._plugins, function(name, plugin) {
				if (plugin.onTrigger) {
					plugin.onTrigger(event);
				}
			});

			this.register({ type: Owl.Type.Event, name: name });
			this.$element.trigger(event);

			if (this.settings && typeof this.settings[handler] === 'function') {
				this.settings[handler].call(this, event);
			}
		}

		return event;
	};

	/**
	 * Enters a state.
	 * @param name - The state name.
	 */
	Owl.prototype.enter = function(name) {
		$.each([ name ].concat(this._states.tags[name] || []), $.proxy(function(i, name) {
			if (this._states.current[name] === undefined) {
				this._states.current[name] = 0;
			}

			this._states.current[name]++;
		}, this));
	};

	/**
	 * Leaves a state.
	 * @param name - The state name.
	 */
	Owl.prototype.leave = function(name) {
		$.each([ name ].concat(this._states.tags[name] || []), $.proxy(function(i, name) {
			this._states.current[name]--;
		}, this));
	};

	/**
	 * Registers an event or state.
	 * @public
	 * @param {Object} object - The event or state to register.
	 */
	Owl.prototype.register = function(object) {
		if (object.type === Owl.Type.Event) {
			if (!$.event.special[object.name]) {
				$.event.special[object.name] = {};
			}

			if (!$.event.special[object.name].owl) {
				var _default = $.event.special[object.name]._default;
				$.event.special[object.name]._default = function(e) {
					if (_default && _default.apply && (!e.namespace || e.namespace.indexOf('owl') === -1)) {
						return _default.apply(this, arguments);
					}
					return e.namespace && e.namespace.indexOf('owl') > -1;
				};
				$.event.special[object.name].owl = true;
			}
		} else if (object.type === Owl.Type.State) {
			if (!this._states.tags[object.name]) {
				this._states.tags[object.name] = object.tags;
			} else {
				this._states.tags[object.name] = this._states.tags[object.name].concat(object.tags);
			}

			this._states.tags[object.name] = $.grep(this._states.tags[object.name], $.proxy(function(tag, i) {
				return $.inArray(tag, this._states.tags[object.name]) === i;
			}, this));
		}
	};

	/**
	 * Suppresses events.
	 * @protected
	 * @param {Array.<String>} events - The events to suppress.
	 */
	Owl.prototype.suppress = function(events) {
		$.each(events, $.proxy(function(index, event) {
			this._supress[event] = true;
		}, this));
	};

	/**
	 * Releases suppressed events.
	 * @protected
	 * @param {Array.<String>} events - The events to release.
	 */
	Owl.prototype.release = function(events) {
		$.each(events, $.proxy(function(index, event) {
			delete this._supress[event];
		}, this));
	};

	/**
	 * Gets unified pointer coordinates from event.
	 * @todo #261
	 * @protected
	 * @param {Event} - The `mousedown` or `touchstart` event.
	 * @returns {Object} - Contains `x` and `y` coordinates of current pointer position.
	 */
	Owl.prototype.pointer = function(event) {
		var result = { x: null, y: null };

		event = event.originalEvent || event || window.event;

		event = event.touches && event.touches.length ?
			event.touches[0] : event.changedTouches && event.changedTouches.length ?
				event.changedTouches[0] : event;

		if (event.pageX) {
			result.x = event.pageX;
			result.y = event.pageY;
		} else {
			result.x = event.clientX;
			result.y = event.clientY;
		}

		return result;
	};

	/**
	 * Determines if the input is a Number or something that can be coerced to a Number
	 * @protected
	 * @param {Number|String|Object|Array|Boolean|RegExp|Function|Symbol} - The input to be tested
	 * @returns {Boolean} - An indication if the input is a Number or can be coerced to a Number
	 */
	Owl.prototype.isNumeric = function(number) {
		return !isNaN(parseFloat(number));
	};

	/**
	 * Gets the difference of two vectors.
	 * @todo #261
	 * @protected
	 * @param {Object} - The first vector.
	 * @param {Object} - The second vector.
	 * @returns {Object} - The difference.
	 */
	Owl.prototype.difference = function(first, second) {
		return {
			x: first.x - second.x,
			y: first.y - second.y
		};
	};

	/**
	 * The jQuery Plugin for the Owl Carousel
	 * @todo Navigation plugin `next` and `prev`
	 * @public
	 */
	$.fn.owlCarousel = function(option) {
		var args = Array.prototype.slice.call(arguments, 1);

		return this.each(function() {
			var $this = $(this),
				data = $this.data('owl.carousel');

			if (!data) {
				data = new Owl(this, typeof option == 'object' && option);
				$this.data('owl.carousel', data);

				$.each([
					'next', 'prev', 'to', 'destroy', 'refresh', 'replace', 'add', 'remove'
				], function(i, event) {
					data.register({ type: Owl.Type.Event, name: event });
					data.$element.on(event + '.owl.carousel.core', $.proxy(function(e) {
						if (e.namespace && e.relatedTarget !== this) {
							this.suppress([ event ]);
							data[event].apply(this, [].slice.call(arguments, 1));
							this.release([ event ]);
						}
					}, data));
				});
			}

			if (typeof option == 'string' && option.charAt(0) !== '_') {
				data[option].apply(data, args);
			}
		});
	};

	/**
	 * The constructor for the jQuery Plugin
	 * @public
	 */
	$.fn.owlCarousel.Constructor = Owl;

})(window.Zepto || window.jQuery, window, document);

/**
 * AutoRefresh Plugin
 * @version 2.3.4
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the auto refresh plugin.
	 * @class The Auto Refresh Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var AutoRefresh = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Refresh interval.
		 * @protected
		 * @type {number}
		 */
		this._interval = null;

		/**
		 * Whether the element is currently visible or not.
		 * @protected
		 * @type {Boolean}
		 */
		this._visible = null;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoRefresh) {
					this.watch();
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, AutoRefresh.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	AutoRefresh.Defaults = {
		autoRefresh: true,
		autoRefreshInterval: 500
	};

	/**
	 * Watches the element.
	 */
	AutoRefresh.prototype.watch = function() {
		if (this._interval) {
			return;
		}

		this._visible = this._core.isVisible();
		this._interval = window.setInterval($.proxy(this.refresh, this), this._core.settings.autoRefreshInterval);
	};

	/**
	 * Refreshes the element.
	 */
	AutoRefresh.prototype.refresh = function() {
		if (this._core.isVisible() === this._visible) {
			return;
		}

		this._visible = !this._visible;

		this._core.$element.toggleClass('owl-hidden', !this._visible);

		this._visible && (this._core.invalidate('width') && this._core.refresh());
	};

	/**
	 * Destroys the plugin.
	 */
	AutoRefresh.prototype.destroy = function() {
		var handler, property;

		window.clearInterval(this._interval);

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.AutoRefresh = AutoRefresh;

})(window.Zepto || window.jQuery, window, document);

/**
 * Lazy Plugin
 * @version 2.3.4
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the lazy plugin.
	 * @class The Lazy Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Lazy = function(carousel) {

		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Already loaded items.
		 * @protected
		 * @type {Array.<jQuery>}
		 */
		this._loaded = [];

		/**
		 * Event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel change.owl.carousel resized.owl.carousel': $.proxy(function(e) {
				if (!e.namespace) {
					return;
				}

				if (!this._core.settings || !this._core.settings.lazyLoad) {
					return;
				}

				if ((e.property && e.property.name == 'position') || e.type == 'initialized') {
					var settings = this._core.settings,
						n = (settings.center && Math.ceil(settings.items / 2) || settings.items),
						i = ((settings.center && n * -1) || 0),
						position = (e.property && e.property.value !== undefined ? e.property.value : this._core.current()) + i,
						clones = this._core.clones().length,
						load = $.proxy(function(i, v) { this.load(v) }, this);
					//TODO: Need documentation for this new option
					if (settings.lazyLoadEager > 0) {
						n += settings.lazyLoadEager;
						// If the carousel is looping also preload images that are to the "left"
						if (settings.loop) {
              position -= settings.lazyLoadEager;
              n++;
            }
					}

					while (i++ < n) {
						this.load(clones / 2 + this._core.relative(position));
						clones && $.each(this._core.clones(this._core.relative(position)), load);
						position++;
					}
				}
			}, this)
		};

		// set the default options
		this._core.options = $.extend({}, Lazy.Defaults, this._core.options);

		// register event handler
		this._core.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	Lazy.Defaults = {
		lazyLoad: false,
		lazyLoadEager: 0
	};

	/**
	 * Loads all resources of an item at the specified position.
	 * @param {Number} position - The absolute position of the item.
	 * @protected
	 */
	Lazy.prototype.load = function(position) {
		var $item = this._core.$stage.children().eq(position),
			$elements = $item && $item.find('.owl-lazy');

		if (!$elements || $.inArray($item.get(0), this._loaded) > -1) {
			return;
		}

		$elements.each($.proxy(function(index, element) {
			var $element = $(element), image,
                url = (window.devicePixelRatio > 1 && $element.attr('data-src-retina')) || $element.attr('data-src') || $element.attr('data-srcset');

			this._core.trigger('load', { element: $element, url: url }, 'lazy');

			if ($element.is('img')) {
				$element.one('load.owl.lazy', $.proxy(function() {
					$element.css('opacity', 1);
					this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
				}, this)).attr('src', url);
            } else if ($element.is('source')) {
                $element.one('load.owl.lazy', $.proxy(function() {
                    this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
                }, this)).attr('srcset', url);
			} else {
				image = new Image();
				image.onload = $.proxy(function() {
					$element.css({
						'background-image': 'url("' + url + '")',
						'opacity': '1'
					});
					this._core.trigger('loaded', { element: $element, url: url }, 'lazy');
				}, this);
				image.src = url;
			}
		}, this));

		this._loaded.push($item.get(0));
	};

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Lazy.prototype.destroy = function() {
		var handler, property;

		for (handler in this.handlers) {
			this._core.$element.off(handler, this.handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Lazy = Lazy;

})(window.Zepto || window.jQuery, window, document);

/**
 * AutoHeight Plugin
 * @version 2.3.4
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the auto height plugin.
	 * @class The Auto Height Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var AutoHeight = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		this._previousHeight = null;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight) {
					this.update();
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight && e.property.name === 'position'){
					this.update();
				}
			}, this),
			'loaded.owl.lazy': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoHeight
					&& e.element.closest('.' + this._core.settings.itemClass).index() === this._core.current()) {
					this.update();
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, AutoHeight.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);
		this._intervalId = null;
		var refThis = this;

		// These changes have been taken from a PR by gavrochelegnou proposed in #1575
		// and have been made compatible with the latest jQuery version
		$(window).on('load', function() {
			if (refThis._core.settings.autoHeight) {
				refThis.update();
			}
		});

		// Autoresize the height of the carousel when window is resized
		// When carousel has images, the height is dependent on the width
		// and should also change on resize
		$(window).resize(function() {
			if (refThis._core.settings.autoHeight) {
				if (refThis._intervalId != null) {
					clearTimeout(refThis._intervalId);
				}

				refThis._intervalId = setTimeout(function() {
					refThis.update();
				}, 250);
			}
		});

	};

	/**
	 * Default options.
	 * @public
	 */
	AutoHeight.Defaults = {
		autoHeight: false,
		autoHeightClass: 'owl-height'
	};

	/**
	 * Updates the view.
	 */
	AutoHeight.prototype.update = function() {
		var start = this._core._current,
			end = start + this._core.settings.items,
			lazyLoadEnabled = this._core.settings.lazyLoad,
			visible = this._core.$stage.children().toArray().slice(start, end),
			heights = [],
			maxheight = 0;

		$.each(visible, function(index, item) {
			heights.push($(item).height());
		});

		maxheight = Math.max.apply(null, heights);

		if (maxheight <= 1 && lazyLoadEnabled && this._previousHeight) {
			maxheight = this._previousHeight;
		}

		this._previousHeight = maxheight;

		this._core.$stage.parent()
			.height(maxheight)
			.addClass(this._core.settings.autoHeightClass);
	};

	AutoHeight.prototype.destroy = function() {
		var handler, property;

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] !== 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.AutoHeight = AutoHeight;

})(window.Zepto || window.jQuery, window, document);

/**
 * Video Plugin
 * @version 2.3.4
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the video plugin.
	 * @class The Video Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Video = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Cache all video URLs.
		 * @protected
		 * @type {Object}
		 */
		this._videos = {};

		/**
		 * Current playing item.
		 * @protected
		 * @type {jQuery}
		 */
		this._playing = null;

		/**
		 * All event handlers.
		 * @todo The cloned content removale is too late
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					this._core.register({ type: 'state', name: 'playing', tags: [ 'interacting' ] });
				}
			}, this),
			'resize.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.video && this.isInFullScreen()) {
					e.preventDefault();
				}
			}, this),
			'refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.is('resizing')) {
					this._core.$stage.find('.cloned .owl-video-frame').remove();
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'position' && this._playing) {
					this.stop();
				}
			}, this),
			'prepared.owl.carousel': $.proxy(function(e) {
				if (!e.namespace) {
					return;
				}

				var $element = $(e.content).find('.owl-video');

				if ($element.length) {
					$element.css('display', 'none');
					this.fetch($element, $(e.content));
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Video.Defaults, this._core.options);

		// register event handlers
		this._core.$element.on(this._handlers);

		this._core.$element.on('click.owl.video', '.owl-video-play-icon', $.proxy(function(e) {
			this.play(e);
		}, this));
	};

	/**
	 * Default options.
	 * @public
	 */
	Video.Defaults = {
		video: false,
		videoHeight: false,
		videoWidth: false
	};

	/**
	 * Gets the video ID and the type (YouTube/Vimeo/vzaar only).
	 * @protected
	 * @param {jQuery} target - The target containing the video data.
	 * @param {jQuery} item - The item containing the video.
	 */
	Video.prototype.fetch = function(target, item) {
			var type = (function() {
					if (target.attr('data-vimeo-id')) {
						return 'vimeo';
					} else if (target.attr('data-vzaar-id')) {
						return 'vzaar'
					} else {
						return 'youtube';
					}
				})(),
				id = target.attr('data-vimeo-id') || target.attr('data-youtube-id') || target.attr('data-vzaar-id'),
				width = target.attr('data-width') || this._core.settings.videoWidth,
				height = target.attr('data-height') || this._core.settings.videoHeight,
				url = target.attr('href');

		if (url) {

			/*
					Parses the id's out of the following urls (and probably more):
					https://www.youtube.com/watch?v=:id
					https://youtu.be/:id
					https://vimeo.com/:id
					https://vimeo.com/channels/:channel/:id
					https://vimeo.com/groups/:group/videos/:id
					https://app.vzaar.com/videos/:id

					Visual example: https://regexper.com/#(http%3A%7Chttps%3A%7C)%5C%2F%5C%2F(player.%7Cwww.%7Capp.)%3F(vimeo%5C.com%7Cyoutu(be%5C.com%7C%5C.be%7Cbe%5C.googleapis%5C.com)%7Cvzaar%5C.com)%5C%2F(video%5C%2F%7Cvideos%5C%2F%7Cembed%5C%2F%7Cchannels%5C%2F.%2B%5C%2F%7Cgroups%5C%2F.%2B%5C%2F%7Cwatch%5C%3Fv%3D%7Cv%5C%2F)%3F(%5BA-Za-z0-9._%25-%5D*)(%5C%26%5CS%2B)%3F
			*/

			id = url.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

			if (id[3].indexOf('youtu') > -1) {
				type = 'youtube';
			} else if (id[3].indexOf('vimeo') > -1) {
				type = 'vimeo';
			} else if (id[3].indexOf('vzaar') > -1) {
				type = 'vzaar';
			} else {
				throw new Error('Video URL not supported.');
			}
			id = id[6];
		} else {
			throw new Error('Missing video URL.');
		}

		this._videos[url] = {
			type: type,
			id: id,
			width: width,
			height: height
		};

		item.attr('data-video', url);

		this.thumbnail(target, this._videos[url]);
	};

	/**
	 * Creates video thumbnail.
	 * @protected
	 * @param {jQuery} target - The target containing the video data.
	 * @param {Object} info - The video info object.
	 * @see `fetch`
	 */
	Video.prototype.thumbnail = function(target, video) {
		var tnLink,
			icon,
			path,
			dimensions = video.width && video.height ? 'width:' + video.width + 'px;height:' + video.height + 'px;' : '',
			customTn = target.find('img'),
			srcType = 'src',
			lazyClass = '',
			settings = this._core.settings,
			create = function(path) {
				icon = '<div class="owl-video-play-icon"></div>';

				if (settings.lazyLoad) {
					tnLink = $('<div/>',{
						"class": 'owl-video-tn ' + lazyClass,
						"srcType": path
					});
				} else {
					tnLink = $( '<div/>', {
						"class": "owl-video-tn",
						"style": 'opacity:1;background-image:url(' + path + ')'
					});
				}
				target.after(tnLink);
				target.after(icon);
			};

		// wrap video content into owl-video-wrapper div
		target.wrap( $( '<div/>', {
			"class": "owl-video-wrapper",
			"style": dimensions
		}));

		if (this._core.settings.lazyLoad) {
			srcType = 'data-src';
			lazyClass = 'owl-lazy';
		}

		// custom thumbnail
		if (customTn.length) {
			create(customTn.attr(srcType));
			customTn.remove();
			return false;
		}

		if (video.type === 'youtube') {
			path = "//img.youtube.com/vi/" + video.id + "/hqdefault.jpg";
			create(path);
		} else if (video.type === 'vimeo') {
			$.ajax({
				type: 'GET',
				url: '//vimeo.com/api/v2/video/' + video.id + '.json',
				jsonp: 'callback',
				dataType: 'jsonp',
				success: function(data) {
					path = data[0].thumbnail_large;
					create(path);
				}
			});
		} else if (video.type === 'vzaar') {
			$.ajax({
				type: 'GET',
				url: '//vzaar.com/api/videos/' + video.id + '.json',
				jsonp: 'callback',
				dataType: 'jsonp',
				success: function(data) {
					path = data.framegrab_url;
					create(path);
				}
			});
		}
	};

	/**
	 * Stops the current video.
	 * @public
	 */
	Video.prototype.stop = function() {
		this._core.trigger('stop', null, 'video');
		this._playing.find('.owl-video-frame').remove();
		this._playing.removeClass('owl-video-playing');
		this._playing = null;
		this._core.leave('playing');
		this._core.trigger('stopped', null, 'video');
	};

	/**
	 * Starts the current video.
	 * @public
	 * @param {Event} event - The event arguments.
	 */
	Video.prototype.play = function(event) {
		var target = $(event.target),
			item = target.closest('.' + this._core.settings.itemClass),
			video = this._videos[item.attr('data-video')],
			width = video.width || '100%',
			height = video.height || this._core.$stage.height(),
			html,
			iframe;

		if (this._playing) {
			return;
		}

		this._core.enter('playing');
		this._core.trigger('play', null, 'video');

		item = this._core.items(this._core.relative(item.index()));

		this._core.reset(item.index());

		html = $( '<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>' );
		html.attr( 'height', height );
		html.attr( 'width', width );
		if (video.type === 'youtube') {
			html.attr( 'src', '//www.youtube.com/embed/' + video.id + '?autoplay=1&rel=0&v=' + video.id );
		} else if (video.type === 'vimeo') {
			html.attr( 'src', '//player.vimeo.com/video/' + video.id + '?autoplay=1' );
		} else if (video.type === 'vzaar') {
			html.attr( 'src', '//view.vzaar.com/' + video.id + '/player?autoplay=true' );
		}

		iframe = $(html).wrap( '<div class="owl-video-frame" />' ).insertAfter(item.find('.owl-video'));

		this._playing = item.addClass('owl-video-playing');
	};

	/**
	 * Checks whether an video is currently in full screen mode or not.
	 * @todo Bad style because looks like a readonly method but changes members.
	 * @protected
	 * @returns {Boolean}
	 */
	Video.prototype.isInFullScreen = function() {
		var element = document.fullscreenElement || document.mozFullScreenElement ||
				document.webkitFullscreenElement;

		return element && $(element).parent().hasClass('owl-video-frame');
	};

	/**
	 * Destroys the plugin.
	 */
	Video.prototype.destroy = function() {
		var handler, property;

		this._core.$element.off('click.owl.video');

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Video = Video;

})(window.Zepto || window.jQuery, window, document);

/**
 * Animate Plugin
 * @version 2.3.4
 * @author Bartosz Wojciechowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the animate plugin.
	 * @class The Navigation Plugin
	 * @param {Owl} scope - The Owl Carousel
	 */
	var Animate = function(scope) {
		this.core = scope;
		this.core.options = $.extend({}, Animate.Defaults, this.core.options);
		this.swapping = true;
		this.previous = undefined;
		this.next = undefined;

		this.handlers = {
			'change.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name == 'position') {
					this.previous = this.core.current();
					this.next = e.property.value;
				}
			}, this),
			'drag.owl.carousel dragged.owl.carousel translated.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					this.swapping = e.type == 'translated';
				}
			}, this),
			'translate.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn)) {
					this.swap();
				}
			}, this)
		};

		this.core.$element.on(this.handlers);
	};

	/**
	 * Default options.
	 * @public
	 */
	Animate.Defaults = {
		animateOut: false,
		animateIn: false
	};

	/**
	 * Toggles the animation classes whenever an translations starts.
	 * @protected
	 * @returns {Boolean|undefined}
	 */
	Animate.prototype.swap = function() {

		if (this.core.settings.items !== 1) {
			return;
		}

		if (!$.support.animation || !$.support.transition) {
			return;
		}

		this.core.speed(0);

		var left,
			clear = $.proxy(this.clear, this),
			previous = this.core.$stage.children().eq(this.previous),
			next = this.core.$stage.children().eq(this.next),
			incoming = this.core.settings.animateIn,
			outgoing = this.core.settings.animateOut;

		if (this.core.current() === this.previous) {
			return;
		}

		if (outgoing) {
			left = this.core.coordinates(this.previous) - this.core.coordinates(this.next);
			previous.one($.support.animation.end, clear)
				.css( { 'left': left + 'px' } )
				.addClass('animated owl-animated-out')
				.addClass(outgoing);
		}

		if (incoming) {
			next.one($.support.animation.end, clear)
				.addClass('animated owl-animated-in')
				.addClass(incoming);
		}
	};

	Animate.prototype.clear = function(e) {
		$(e.target).css( { 'left': '' } )
			.removeClass('animated owl-animated-out owl-animated-in')
			.removeClass(this.core.settings.animateIn)
			.removeClass(this.core.settings.animateOut);
		this.core.onTransitionEnd();
	};

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Animate.prototype.destroy = function() {
		var handler, property;

		for (handler in this.handlers) {
			this.core.$element.off(handler, this.handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Animate = Animate;

})(window.Zepto || window.jQuery, window, document);

/**
 * Autoplay Plugin
 * @version 2.3.4
 * @author Bartosz Wojciechowski
 * @author Artus Kolanowski
 * @author David Deutsch
 * @author Tom De Caluwé
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	/**
	 * Creates the autoplay plugin.
	 * @class The Autoplay Plugin
	 * @param {Owl} scope - The Owl Carousel
	 */
	var Autoplay = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * The autoplay timeout id.
		 * @type {Number}
		 */
		this._call = null;

		/**
		 * Depending on the state of the plugin, this variable contains either
		 * the start time of the timer or the current timer value if it's
		 * paused. Since we start in a paused state we initialize the timer
		 * value.
		 * @type {Number}
		 */
		this._time = 0;

		/**
		 * Stores the timeout currently used.
		 * @type {Number}
		 */
		this._timeout = 0;

		/**
		 * Indicates whenever the autoplay is paused.
		 * @type {Boolean}
		 */
		this._paused = true;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'settings') {
					if (this._core.settings.autoplay) {
						this.play();
					} else {
						this.stop();
					}
				} else if (e.namespace && e.property.name === 'position' && this._paused) {
					// Reset the timer. This code is triggered when the position
					// of the carousel was changed through user interaction.
					this._time = 0;
				}
			}, this),
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.autoplay) {
					this.play();
				}
			}, this),
			'play.owl.autoplay': $.proxy(function(e, t, s) {
				if (e.namespace) {
					this.play(t, s);
				}
			}, this),
			'stop.owl.autoplay': $.proxy(function(e) {
				if (e.namespace) {
					this.stop();
				}
			}, this),
			'mouseover.owl.autoplay': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
					this.pause();
				}
			}, this),
			'mouseleave.owl.autoplay': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
					this.play();
				}
			}, this),
			'touchstart.owl.core': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause && this._core.is('rotating')) {
					this.pause();
				}
			}, this),
			'touchend.owl.core': $.proxy(function() {
				if (this._core.settings.autoplayHoverPause) {
					this.play();
				}
			}, this)
		};

		// register event handlers
		this._core.$element.on(this._handlers);

		// set default options
		this._core.options = $.extend({}, Autoplay.Defaults, this._core.options);
	};

	/**
	 * Default options.
	 * @public
	 */
	Autoplay.Defaults = {
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: false,
		autoplaySpeed: false
	};

	/**
	 * Transition to the next slide and set a timeout for the next transition.
	 * @private
	 * @param {Number} [speed] - The animation speed for the animations.
	 */
	Autoplay.prototype._next = function(speed) {
		this._call = window.setTimeout(
			$.proxy(this._next, this, speed),
			this._timeout * (Math.round(this.read() / this._timeout) + 1) - this.read()
		);

		if (this._core.is('interacting') || document.hidden) {
			return;
		}
		this._core.next(speed || this._core.settings.autoplaySpeed);
	}

	/**
	 * Reads the current timer value when the timer is playing.
	 * @public
	 */
	Autoplay.prototype.read = function() {
		return new Date().getTime() - this._time;
	};

	/**
	 * Starts the autoplay.
	 * @public
	 * @param {Number} [timeout] - The interval before the next animation starts.
	 * @param {Number} [speed] - The animation speed for the animations.
	 */
	Autoplay.prototype.play = function(timeout, speed) {
		var elapsed;

		if (!this._core.is('rotating')) {
			this._core.enter('rotating');
		}

		timeout = timeout || this._core.settings.autoplayTimeout;

		// Calculate the elapsed time since the last transition. If the carousel
		// wasn't playing this calculation will yield zero.
		elapsed = Math.min(this._time % (this._timeout || timeout), timeout);

		if (this._paused) {
			// Start the clock.
			this._time = this.read();
			this._paused = false;
		} else {
			// Clear the active timeout to allow replacement.
			window.clearTimeout(this._call);
		}

		// Adjust the origin of the timer to match the new timeout value.
		this._time += this.read() % timeout - elapsed;

		this._timeout = timeout;
		this._call = window.setTimeout($.proxy(this._next, this, speed), timeout - elapsed);
	};

	/**
	 * Stops the autoplay.
	 * @public
	 */
	Autoplay.prototype.stop = function() {
		if (this._core.is('rotating')) {
			// Reset the clock.
			this._time = 0;
			this._paused = true;

			window.clearTimeout(this._call);
			this._core.leave('rotating');
		}
	};

	/**
	 * Pauses the autoplay.
	 * @public
	 */
	Autoplay.prototype.pause = function() {
		if (this._core.is('rotating') && !this._paused) {
			// Pause the clock.
			this._time = this.read();
			this._paused = true;

			window.clearTimeout(this._call);
		}
	};

	/**
	 * Destroys the plugin.
	 */
	Autoplay.prototype.destroy = function() {
		var handler, property;

		this.stop();

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.autoplay = Autoplay;

})(window.Zepto || window.jQuery, window, document);

/**
 * Navigation Plugin
 * @version 2.3.4
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {
	'use strict';

	/**
	 * Creates the navigation plugin.
	 * @class The Navigation Plugin
	 * @param {Owl} carousel - The Owl Carousel.
	 */
	var Navigation = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Indicates whether the plugin is initialized or not.
		 * @protected
		 * @type {Boolean}
		 */
		this._initialized = false;

		/**
		 * The current paging indexes.
		 * @protected
		 * @type {Array}
		 */
		this._pages = [];

		/**
		 * All DOM elements of the user interface.
		 * @protected
		 * @type {Object}
		 */
		this._controls = {};

		/**
		 * Markup for an indicator.
		 * @protected
		 * @type {Array.<String>}
		 */
		this._templates = [];

		/**
		 * The carousel element.
		 * @type {jQuery}
		 */
		this.$element = this._core.$element;

		/**
		 * Overridden methods of the carousel.
		 * @protected
		 * @type {Object}
		 */
		this._overrides = {
			next: this._core.next,
			prev: this._core.prev,
			to: this._core.to
		};

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'prepared.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.push('<div class="' + this._core.settings.dotClass + '">' +
						$(e.content).find('[data-dot]').addBack('[data-dot]').attr('data-dot') + '</div>');
				}
			}, this),
			'added.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.splice(e.position, 0, this._templates.pop());
				}
			}, this),
			'remove.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.dotsData) {
					this._templates.splice(e.position, 1);
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name == 'position') {
					this.draw();
				}
			}, this),
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && !this._initialized) {
					this._core.trigger('initialize', null, 'navigation');
					this.initialize();
					this.update();
					this.draw();
					this._initialized = true;
					this._core.trigger('initialized', null, 'navigation');
				}
			}, this),
			'refreshed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._initialized) {
					this._core.trigger('refresh', null, 'navigation');
					this.update();
					this.draw();
					this._core.trigger('refreshed', null, 'navigation');
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Navigation.Defaults, this._core.options);

		// register event handlers
		this.$element.on(this._handlers);
	};

	/**
	 * Default options.
	 * @public
	 * @todo Rename `slideBy` to `navBy`
	 */
	Navigation.Defaults = {
		nav: false,
		navText: [
			'<span aria-label="' + 'Previous' + '">&#x2039;</span>',
			'<span aria-label="' + 'Next' + '">&#x203a;</span>'
		],
		navSpeed: false,
		navElement: 'button type="button" role="presentation"',
		navContainer: false,
		navContainerClass: 'owl-nav',
		navClass: [
			'owl-prev',
			'owl-next'
		],
		slideBy: 1,
		dotClass: 'owl-dot',
		dotsClass: 'owl-dots',
		dots: true,
		dotsEach: false,
		dotsData: false,
		dotsSpeed: false,
		dotsContainer: false
	};

	/**
	 * Initializes the layout of the plugin and extends the carousel.
	 * @protected
	 */
	Navigation.prototype.initialize = function() {
		var override,
			settings = this._core.settings;

		// create DOM structure for relative navigation
		this._controls.$relative = (settings.navContainer ? $(settings.navContainer)
			: $('<div>').addClass(settings.navContainerClass).appendTo(this.$element)).addClass('disabled');

		this._controls.$previous = $('<' + settings.navElement + '>')
			.addClass(settings.navClass[0])
			.html(settings.navText[0])
			.prependTo(this._controls.$relative)
			.on('click', $.proxy(function(e) {
				this.prev(settings.navSpeed);
			}, this));
		this._controls.$next = $('<' + settings.navElement + '>')
			.addClass(settings.navClass[1])
			.html(settings.navText[1])
			.appendTo(this._controls.$relative)
			.on('click', $.proxy(function(e) {
				this.next(settings.navSpeed);
			}, this));

		// create DOM structure for absolute navigation
		if (!settings.dotsData) {
			this._templates = [ $('<button role="button">')
				.addClass(settings.dotClass)
				.append($('<span>'))
				.prop('outerHTML') ];
		}

		this._controls.$absolute = (settings.dotsContainer ? $(settings.dotsContainer)
			: $('<div>').addClass(settings.dotsClass).appendTo(this.$element)).addClass('disabled');

		this._controls.$absolute.on('click', 'button', $.proxy(function(e) {
			var index = $(e.target).parent().is(this._controls.$absolute)
				? $(e.target).index() : $(e.target).parent().index();

			e.preventDefault();

			this.to(index, settings.dotsSpeed);
		}, this));

		/*$el.on('focusin', function() {
			$(document).off(".carousel");

			$(document).on('keydown.carousel', function(e) {
				if(e.keyCode == 37) {
					$el.trigger('prev.owl')
				}
				if(e.keyCode == 39) {
					$el.trigger('next.owl')
				}
			});
		});*/

		// override public methods of the carousel
		for (override in this._overrides) {
			this._core[override] = $.proxy(this[override], this);
		}
	};

	/**
	 * Destroys the plugin.
	 * @protected
	 */
	Navigation.prototype.destroy = function() {
		var handler, control, property, override, settings;
		settings = this._core.settings;

		for (handler in this._handlers) {
			this.$element.off(handler, this._handlers[handler]);
		}
		for (control in this._controls) {
			if (control === '$relative' && settings.navContainer) {
				this._controls[control].html('');
			} else {
				this._controls[control].remove();
			}
		}
		for (override in this.overides) {
			this._core[override] = this._overrides[override];
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	/**
	 * Updates the internal state.
	 * @protected
	 */
	Navigation.prototype.update = function() {
		var i, j, k,
			lower = this._core.clones().length / 2,
			upper = lower + this._core.items().length,
			maximum = this._core.maximum(true),
			settings = this._core.settings,
			size = settings.center || settings.autoWidth || settings.dotsData
				? 1 : settings.dotsEach || settings.items;

		if (settings.slideBy !== 'page') {
			settings.slideBy = Math.min(settings.slideBy, settings.items);
		}

		if (settings.dots || settings.slideBy == 'page') {
			this._pages = [];

			for (i = lower, j = 0, k = 0; i < upper; i++) {
				if (j >= size || j === 0) {
					this._pages.push({
						start: Math.min(maximum, i - lower),
						end: i - lower + size - 1
					});
					if (Math.min(maximum, i - lower) === maximum) {
						break;
					}
					j = 0, ++k;
				}
				j += this._core.mergers(this._core.relative(i));
			}
		}
	};

	/**
	 * Draws the user interface.
	 * @todo The option `dotsData` wont work.
	 * @protected
	 */
	Navigation.prototype.draw = function() {
		var difference,
			settings = this._core.settings,
			disabled = this._core.items().length <= settings.items,
			index = this._core.relative(this._core.current()),
			loop = settings.loop || settings.rewind;

		this._controls.$relative.toggleClass('disabled', !settings.nav || disabled);

		if (settings.nav) {
			this._controls.$previous.toggleClass('disabled', !loop && index <= this._core.minimum(true));
			this._controls.$next.toggleClass('disabled', !loop && index >= this._core.maximum(true));
		}

		this._controls.$absolute.toggleClass('disabled', !settings.dots || disabled);

		if (settings.dots) {
			difference = this._pages.length - this._controls.$absolute.children().length;

			if (settings.dotsData && difference !== 0) {
				this._controls.$absolute.html(this._templates.join(''));
			} else if (difference > 0) {
				this._controls.$absolute.append(new Array(difference + 1).join(this._templates[0]));
			} else if (difference < 0) {
				this._controls.$absolute.children().slice(difference).remove();
			}

			this._controls.$absolute.find('.active').removeClass('active');
			this._controls.$absolute.children().eq($.inArray(this.current(), this._pages)).addClass('active');
		}
	};

	/**
	 * Extends event data.
	 * @protected
	 * @param {Event} event - The event object which gets thrown.
	 */
	Navigation.prototype.onTrigger = function(event) {
		var settings = this._core.settings;

		event.page = {
			index: $.inArray(this.current(), this._pages),
			count: this._pages.length,
			size: settings && (settings.center || settings.autoWidth || settings.dotsData
				? 1 : settings.dotsEach || settings.items)
		};
	};

	/**
	 * Gets the current page position of the carousel.
	 * @protected
	 * @returns {Number}
	 */
	Navigation.prototype.current = function() {
		var current = this._core.relative(this._core.current());
		return $.grep(this._pages, $.proxy(function(page, index) {
			return page.start <= current && page.end >= current;
		}, this)).pop();
	};

	/**
	 * Gets the current succesor/predecessor position.
	 * @protected
	 * @returns {Number}
	 */
	Navigation.prototype.getPosition = function(successor) {
		var position, length,
			settings = this._core.settings;

		if (settings.slideBy == 'page') {
			position = $.inArray(this.current(), this._pages);
			length = this._pages.length;
			successor ? ++position : --position;
			position = this._pages[((position % length) + length) % length].start;
		} else {
			position = this._core.relative(this._core.current());
			length = this._core.items().length;
			successor ? position += settings.slideBy : position -= settings.slideBy;
		}

		return position;
	};

	/**
	 * Slides to the next item or page.
	 * @public
	 * @param {Number} [speed=false] - The time in milliseconds for the transition.
	 */
	Navigation.prototype.next = function(speed) {
		$.proxy(this._overrides.to, this._core)(this.getPosition(true), speed);
	};

	/**
	 * Slides to the previous item or page.
	 * @public
	 * @param {Number} [speed=false] - The time in milliseconds for the transition.
	 */
	Navigation.prototype.prev = function(speed) {
		$.proxy(this._overrides.to, this._core)(this.getPosition(false), speed);
	};

	/**
	 * Slides to the specified item or page.
	 * @public
	 * @param {Number} position - The position of the item or page.
	 * @param {Number} [speed] - The time in milliseconds for the transition.
	 * @param {Boolean} [standard=false] - Whether to use the standard behaviour or not.
	 */
	Navigation.prototype.to = function(position, speed, standard) {
		var length;

		if (!standard && this._pages.length) {
			length = this._pages.length;
			$.proxy(this._overrides.to, this._core)(this._pages[((position % length) + length) % length].start, speed);
		} else {
			$.proxy(this._overrides.to, this._core)(position, speed);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Navigation = Navigation;

})(window.Zepto || window.jQuery, window, document);

/**
 * Hash Plugin
 * @version 2.3.4
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {
	'use strict';

	/**
	 * Creates the hash plugin.
	 * @class The Hash Plugin
	 * @param {Owl} carousel - The Owl Carousel
	 */
	var Hash = function(carousel) {
		/**
		 * Reference to the core.
		 * @protected
		 * @type {Owl}
		 */
		this._core = carousel;

		/**
		 * Hash index for the items.
		 * @protected
		 * @type {Object}
		 */
		this._hashes = {};

		/**
		 * The carousel element.
		 * @type {jQuery}
		 */
		this.$element = this._core.$element;

		/**
		 * All event handlers.
		 * @protected
		 * @type {Object}
		 */
		this._handlers = {
			'initialized.owl.carousel': $.proxy(function(e) {
				if (e.namespace && this._core.settings.startPosition === 'URLHash') {
					$(window).trigger('hashchange.owl.navigation');
				}
			}, this),
			'prepared.owl.carousel': $.proxy(function(e) {
				if (e.namespace) {
					var hash = $(e.content).find('[data-hash]').addBack('[data-hash]').attr('data-hash');

					if (!hash) {
						return;
					}

					this._hashes[hash] = e.content;
				}
			}, this),
			'changed.owl.carousel': $.proxy(function(e) {
				if (e.namespace && e.property.name === 'position') {
					var current = this._core.items(this._core.relative(this._core.current())),
						hash = $.map(this._hashes, function(item, hash) {
							return item === current ? hash : null;
						}).join();

					if (!hash || window.location.hash.slice(1) === hash) {
						return;
					}

					window.location.hash = hash;
				}
			}, this)
		};

		// set default options
		this._core.options = $.extend({}, Hash.Defaults, this._core.options);

		// register the event handlers
		this.$element.on(this._handlers);

		// register event listener for hash navigation
		$(window).on('hashchange.owl.navigation', $.proxy(function(e) {
			var hash = window.location.hash.substring(1),
				items = this._core.$stage.children(),
				position = this._hashes[hash] && items.index(this._hashes[hash]);

			if (position === undefined || position === this._core.current()) {
				return;
			}

			this._core.to(this._core.relative(position), false, true);
		}, this));
	};

	/**
	 * Default options.
	 * @public
	 */
	Hash.Defaults = {
		URLhashListener: false
	};

	/**
	 * Destroys the plugin.
	 * @public
	 */
	Hash.prototype.destroy = function() {
		var handler, property;

		$(window).off('hashchange.owl.navigation');

		for (handler in this._handlers) {
			this._core.$element.off(handler, this._handlers[handler]);
		}
		for (property in Object.getOwnPropertyNames(this)) {
			typeof this[property] != 'function' && (this[property] = null);
		}
	};

	$.fn.owlCarousel.Constructor.Plugins.Hash = Hash;

})(window.Zepto || window.jQuery, window, document);

/**
 * Support Plugin
 *
 * @version 2.3.4
 * @author Vivid Planet Software GmbH
 * @author Artus Kolanowski
 * @author David Deutsch
 * @license The MIT License (MIT)
 */
;(function($, window, document, undefined) {

	var style = $('<support>').get(0).style,
		prefixes = 'Webkit Moz O ms'.split(' '),
		events = {
			transition: {
				end: {
					WebkitTransition: 'webkitTransitionEnd',
					MozTransition: 'transitionend',
					OTransition: 'oTransitionEnd',
					transition: 'transitionend'
				}
			},
			animation: {
				end: {
					WebkitAnimation: 'webkitAnimationEnd',
					MozAnimation: 'animationend',
					OAnimation: 'oAnimationEnd',
					animation: 'animationend'
				}
			}
		},
		tests = {
			csstransforms: function() {
				return !!test('transform');
			},
			csstransforms3d: function() {
				return !!test('perspective');
			},
			csstransitions: function() {
				return !!test('transition');
			},
			cssanimations: function() {
				return !!test('animation');
			}
		};

	function test(property, prefixed) {
		var result = false,
			upper = property.charAt(0).toUpperCase() + property.slice(1);

		$.each((property + ' ' + prefixes.join(upper + ' ') + upper).split(' '), function(i, property) {
			if (style[property] !== undefined) {
				result = prefixed ? property : true;
				return false;
			}
		});

		return result;
	}

	function prefixed(property) {
		return test(property, true);
	}

	if (tests.csstransitions()) {
		/* jshint -W053 */
		$.support.transition = new String(prefixed('transition'))
		$.support.transition.end = events.transition.end[ $.support.transition ];
	}

	if (tests.cssanimations()) {
		/* jshint -W053 */
		$.support.animation = new String(prefixed('animation'))
		$.support.animation.end = events.animation.end[ $.support.animation ];
	}

	if (tests.csstransforms()) {
		/* jshint -W053 */
		$.support.transform = new String(prefixed('transform'));
		$.support.transform3d = tests.csstransforms3d();
	}

})(window.Zepto || window.jQuery, window, document);


$(document).ready(function () {
  $('.carousel-responsive').owlCarousel({
    loop: true,
    nav: false,
    margin: 10,
    responsiveClass: true,
    autoplay: true,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      992: {
        items: 3
      },
      1200: {
        items: 4

      }
    }
  });

  $('[data-toggle="popover"]').popover();
  $('.bd-toggle-animated-progress').on('click', function () {
    $(this).siblings('.progress').find('.progress-bar-striped').toggleClass('progress-bar-animated');
  });
  $('[data-toggle="tooltip"]').tooltip();
  // Demos within modals
  $('.tooltip-test').tooltip();
  $('.popover-test').popover();

  // Modal relatedTarget demo
  $('#exampleModal').on('show.bs.modal', function (event) {
    var $button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = $button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var $modal = $(this)
    $modal.find('.modal-title').text('New message to ' + recipient)
    $modal.find('.modal-body input').val(recipient)
  });



  $.fn.preload = function () {
    this.each(function () {
      $('<img/>')[0].src = this;
    });
  }

  $(['images/asuo_logo_reg.png']).preload();

  // Determine each external link
  // $('a').each(function () {
  //   if (linkIsExternal(this)) {
  //     $(this).addClass('ext');
  //   }
  // });

  // Wire up ext.
  //   var validateEmail = function(elementValue) {
  //     // var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

  //     var emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


  //     return emailPattern.test(elementValue);
  //  }


  //   check_validated = false;

  //  $('#mail').keyup(function() {
  //      check_validated = false;
  //      var value = $(this).val();
  //      var valid = validateEmail(value);
  //      if (!valid) {
  //        $(this).css('color', 'red');



  //        check_validated = true;
  //        } else {
  //        $(this).css('color', '#5C6670');

  //        check_validated = false;
  //    }
  //  });

  //  check_validated_ben = false;
  //  $('#beneficiary_mail').keyup(function() {
  //    check_validated_ben = false;
  //     var value = $(this).val();
  //     var valid = validateEmail(value);
  //     if (!valid) {
  //        $(this).css('color', 'red');
  //        check_validated_ben = true;
  //        } else {
  //        $(this).css('color', '#5C6670');
  //        check_validated_ben = false;
  //    }
  //  });

  $('a.ext').on('click keypress', function (e) {
    var href = $(this).attr('href');
    var target = '_blank';

    // Define the modal attributes.
    $('.extlink-continue').attr({
      'href': href,
      'target': target
    });
    $('#extlink-modal').remove();
    // Define the modal html.
    var html = '<div id="extlink-modal" style="display:block;">' +
      '<a class="modal-close"><i class="fa fa-close"></i></a>' +
      '<img src="images/asuo_logo_reg.png" /><p>This link will open up a new tab.</p><a href="' + href + '" target="' + target +
      '" class="btn btn-primary extlink-continue">' + 'Continue</a></div>';
    // Prevent the default function so we can force the confirm.
    e.preventDefault();

    return Confirm(html);

    function Confirm($content) {
      $('body').prepend($content);
      $('.extlink-continue').off('click keypress').on('click keypress', function () {
        $('#extlink-modal').remove();
      });
      $('.modal-close').off('click keypress').on('click keypress', function () {
        $('#extlink-modal').remove();
      });
    }
  });

  function linkIsExternal(linkElement) {
    if (linkElement.href.indexOf('tel:') != -1) {
      return false;
    }
    return (linkElement.host !== window.location.host);
  }
});

! function (d, $) {
  "object" == typeof exports && "object" == typeof module ? module.exports = $() : "function" == typeof define && define.amd ? define("libphonenumber", [], $) : "object" == typeof exports ? exports.libphonenumber = $() : d.libphonenumber = $()
}(this, function () {
  return function (d) {
    function $(n) {
      if (t[n]) return t[n].exports;
      var e = t[n] = {
        i: n,
        l: !1,
        exports: {}
      };
      return d[n].call(e.exports, e, e.exports, $), e.l = !0, e.exports
    }
    var t = {};
    return $.m = d, $.c = t, $.i = function (d) {
      return d
    }, $.d = function (d, t, n) {
      $.o(d, t) || Object.defineProperty(d, t, {
        configurable: !1,
        enumerable: !0,
        get: n
      })
    }, $.n = function (d) {
      var t = d && d.__esModule ? function () {
        return d.default
      } : function () {
        return d
      };
      return $.d(t, "a", t), t
    }, $.o = function (d, $) {
      return Object.prototype.hasOwnProperty.call(d, $)
    }, $.p = "", $($.s = 73)
  }([function (d, $) {
    var t = d.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")();
    "number" == typeof __g && (__g = t)
  }, function (d, $, t) {
    var n = t(29)("wks"),
      e = t(31),
      r = t(0).Symbol,
      l = "function" == typeof r,
      u = d.exports = function (d) {
        return n[d] || (n[d] = l && r[d] || (l ? r : e)("Symbol." + d))
      };
    u.store = n
  }, function (d, $) {
    var t = d.exports = {
      version: "2.4.0"
    };
    "number" == typeof __e && (__e = t)
  }, function (d, $, t) {
    d.exports = !t(16)(function () {
      return 7 != Object.defineProperty({}, "a", {
        get: function () {
          return 7
        }
      }).a
    })
  }, function (d, $, t) {
    var n = t(10),
      e = t(27);
    d.exports = t(3) ? function (d, $, t) {
      return n.f(d, $, e(1, t))
    } : function (d, $, t) {
      return d[$] = t, d
    }
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d[0]
    }

    function e(d) {
      return d[1]
    }

    function r(d) {
      return d[2] || []
    }

    function l(d) {
      return d[3]
    }

    function u(d) {
      return d[4]
    }

    function i(d) {
      var $ = d[5];
      return $ || ($ = l(d)), $
    }

    function o(d) {
      return d[6]
    }

    function a(d) {
      return d[7]
    }

    function _(d) {
      return d[8]
    }

    function f(d) {
      return d[0]
    }

    function c(d) {
      return d[1]
    }

    function s(d) {
      return d[2] || []
    }

    function p(d, $) {
      return d[3] || u($)
    }

    function h(d, $) {
      return d[4] || a($)
    }

    function m(d, $) {
      return p(d, $) && !h(d, $)
    }

    function y(d) {
      return d[5] || c(d)
    }

    function v(d, $) {
      var t = $.country_phone_code_to_countries[d][0];
      return $.countries[t]
    }

    function g(d) {
      return d[9]
    }

    function b(d, $) {
      return g(d) ? g(d)[$] : void 0
    }

    function x(d) {
      return b(d, 0)
    }

    function M(d) {
      return b(d, 1)
    }

    function S(d) {
      return b(d, 2)
    }

    function O(d) {
      return b(d, 3)
    }

    function E(d) {
      return b(d, 4)
    }

    function T(d) {
      return b(d, 5)
    }

    function A(d) {
      return b(d, 6)
    }

    function I(d) {
      return b(d, 7)
    }

    function P(d) {
      return b(d, 8)
    }

    function R(d) {
      return b(d, 9)
    }
    Object.defineProperty($, "__esModule", {
      value: !0
    }), $.get_phone_code = n, $.get_national_number_pattern = e, $.get_formats = r, $.get_national_prefix = l, $.get_national_prefix_formatting_rule = u, $.get_national_prefix_for_parsing = i, $.get_national_prefix_transform_rule = o, $.get_national_prefix_is_optional_when_formatting = a, $.get_leading_digits = _, $.get_format_pattern = f, $.get_format_format = c, $.get_format_leading_digits_patterns = s, $.get_format_national_prefix_formatting_rule = p, $.get_format_national_prefix_is_optional_when_formatting = h, $.get_format_national_prefix_is_mandatory_when_formatting = m, $.get_format_international_format = y, $.get_metadata_by_country_phone_code = v, $.get_types = g, $.get_type_fixed_line = x, $.get_type_mobile = M, $.get_type_toll_free = S, $.get_type_premium_rate = O, $.get_type_personal_number = E, $.get_type_voice_mail = T, $.get_type_uan = A, $.get_type_pager = I, $.get_type_voip = P, $.get_type_shared_cost = R
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d && d.__esModule ? d : {
        default: d
      }
    }

    function e(d, $) {
      if ("string" == typeof $) {
        var t = $;
        $ = (0, v.default)({}, B, {
          country: {
            restrict: t
          }
        })
      }
      $ || ($ = (0, v.default)({}, B)), this.metadata.countries[$.country.default] || ($ = (0, v.default)({}, $), delete $.country.default), this.metadata.countries[$.country.restrict] || ($ = (0, v.default)({}, $), delete $.country.restrict);
      var n = i(d);
      if (!u(n)) return {};
      var e = a(n, this.metadata),
        l = e.country_phone_code,
        o = e.number;
      if (!l && !o) return {};
      var c = void 0,
        s = void 0,
        h = !1;
      if (l) {
        if (h = !0, $.country.restrict && l !== (0, b.get_phone_code)(this.metadata.countries[$.country.restrict])) return {};
        s = (0, b.get_metadata_by_country_phone_code)(l, this.metadata)
      } else($.country.default || $.country.restrict) && (c = $.country.default || $.country.restrict, s = this.metadata.countries[c], o = r(d));
      if (!s) return {};
      var m = _(o, s),
        y = m !== o;
      if (!h && !y && p(m, s)) return {};
      if (!c && (c = f(l, m, this.metadata), !c)) return {};
      if (m.length > L) return {};
      var x = new RegExp((0, b.get_national_number_pattern)(s));
      return (0, g.matches_entirely)(m, x) ? {
        country: c,
        phone: m
      } : {}
    }

    function r(d) {
      return l(d, C)
    }

    function l(d, $) {
      var t = "",
        n = !0,
        e = !1,
        r = void 0;
      try {
        for (var l, u = (0, m.default)(d); !(n = (l = u.next()).done); n = !0) {
          var i = l.value,
            o = $[i.toUpperCase()];
          void 0 !== o && (t += o)
        }
      } catch (d) {
        e = !0, r = d
      } finally {
        try {
          !n && u.return && u.return()
        } finally {
          if (e) throw r
        }
      }
      return t
    }

    function u(d) {
      return d.length >= N && (0, g.matches_entirely)(d, A)
    }

    function i(d) {
      if (!d || d.length > w) return "";
      var $ = d.search(I);
      return $ < 0 ? "" : d.slice($).replace(P, "")
    }

    function o(d) {
      if (!d) return "";
      var $ = R.test(d);
      return d = r(d), $ ? "+" + d : d
    }

    function a(d, $) {
      if (d = o(d), !d) return {};
      if ("+" !== d[0]) return {
        number: d
      };
      if (d = d.slice(1), "0" === d[0]) return {};
      for (var t = 1; t <= G && t <= d.length;) {
        var n = d.slice(0, t);
        if ($.country_phone_code_to_countries[n]) return {
          country_phone_code: n,
          number: d.slice(t)
        };
        t++
      }
      return {}
    }

    function _(d, $) {
      var t = (0, b.get_national_prefix_for_parsing)($);
      if (!d || !t) return d;
      var n = new RegExp("^(?:" + t + ")"),
        e = n.exec(d);
      if (!e) return d;
      var r = (0, b.get_national_prefix_transform_rule)($),
        l = void 0,
        u = e[e.length - 1];
      l = r && u ? d.replace(n, r) : d.slice(e[0].length);
      var i = new RegExp((0, b.get_national_number_pattern)($));
      return (0, g.matches_entirely)(d, i) && !(0, g.matches_entirely)(l, i) ? d : l
    }

    function f(d, $, t) {
      var n = t.country_phone_code_to_countries[d];
      if (1 === n.length) return n[0];
      var e = !0,
        r = !1,
        l = void 0;
      try {
        for (var u, i = (0, m.default)(n); !(e = (u = i.next()).done); e = !0) {
          var o = u.value,
            a = t.countries[o];
          if ((0, b.get_leading_digits)(a)) {
            if ($ && 0 === $.search((0, b.get_leading_digits)(a))) return o
          } else if (c($, a)) return o
        }
      } catch (d) {
        r = !0, l = d
      } finally {
        try {
          !e && i.return && i.return()
        } finally {
          if (r) throw l
        }
      }
    }

    function c(d, $) {
      if (s(d, (0, b.get_national_number_pattern)($))) return s(d, (0, b.get_type_mobile)($)) ? (0, b.get_type_fixed_line)($) ? "MOBILE" : "FIXED_LINE_OR_MOBILE" : s(d, (0, b.get_type_fixed_line)($)) ? (0, b.get_type_mobile)($) ? "FIXED_LINE" : "FIXED_LINE_OR_MOBILE" : s(d, (0, b.get_type_toll_free)($)) ? "TOLL_FREE" : s(d, (0, b.get_type_premium_rate)($)) ? "PREMIUM_RATE" : s(d, (0, b.get_type_personal_number)($)) ? "PERSONAL_NUMBER" : s(d, (0, b.get_type_voice_mail)($)) ? "VOICEMAIL" : s(d, (0, b.get_type_uan)($)) ? "UAN" : s(d, (0, b.get_type_pager)($)) ? "PAGER" : s(d, (0, b.get_type_voip)($)) ? "VOIP" : s(d, (0, b.get_type_shared_cost)($)) ? "SHARED_COST" : void 0
    }

    function s(d, $) {
      return (0, g.matches_entirely)(d, $)
    }

    function p(d, $) {
      var t = (0, x.choose_format_for_number)((0, b.get_formats)($), d);
      if (t) return (0, b.get_format_national_prefix_is_mandatory_when_formatting)(t, $)
    }
    Object.defineProperty($, "__esModule", {
      value: !0
    }), $.VALID_PUNCTUATION = $.VALID_DIGITS = $.PLUS_CHARS = void 0, $.default = e, $.normalize = r, $.replace_characters = l, $.is_viable_phone_number = u, $.extract_formatted_phone_number = i, $.parse_phone_number = o, $.parse_phone_number_and_country_phone_code = a, $.strip_national_prefix = _, $.find_country_code = f, $.get_number_type = c, $.is_of_type = s, $.is_national_prefix_required = p;
    var h = t(13),
      m = n(h),
      y = t(40),
      v = n(y),
      g = t(11),
      b = t(5),
      x = t(12),
      M = $.PLUS_CHARS = "+＋",
      S = $.VALID_DIGITS = "0-9０-９٠-٩۰-۹",
      O = $.VALID_PUNCTUATION = "-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～",
      E = "[" + S + "]{" + N + "}",
      T = "[" + M + "]{0,1}(?:[" + O + "]*[" + S + "]){3,}[" + O + S + "]*",
      A = new RegExp("^" + E + "$|^" + T + "$", "i"),
      I = new RegExp("[" + M + S + "]"),
      P = new RegExp("[^" + S + "]+$"),
      R = new RegExp("^[" + M + "]+"),
      C = {
        0: "0",
        1: "1",
        2: "2",
        3: "3",
        4: "4",
        5: "5",
        6: "6",
        7: "7",
        8: "8",
        9: "9",
        "０": "0",
        "１": "1",
        "２": "2",
        "３": "3",
        "４": "4",
        "５": "5",
        "６": "6",
        "７": "7",
        "８": "8",
        "９": "9",
        "٠": "0",
        "١": "1",
        "٢": "2",
        "٣": "3",
        "٤": "4",
        "٥": "5",
        "٦": "6",
        "٧": "7",
        "٨": "8",
        "٩": "9",
        "۰": "0",
        "۱": "1",
        "۲": "2",
        "۳": "3",
        "۴": "4",
        "۵": "5",
        "۶": "6",
        "۷": "7",
        "۸": "8",
        "۹": "9"
      },
      G = 3,
      N = 2,
      L = 17,
      w = 250,
      B = {
        country: {}
      }
  }, function (d, $, t) {
    var n = t(17);
    d.exports = function (d) {
      if (!n(d)) throw TypeError(d + " is not an object!");
      return d
    }
  }, function (d, $) {
    var t = {}.hasOwnProperty;
    d.exports = function (d, $) {
      return t.call(d, $)
    }
  }, function (d, $) {
    d.exports = {}
  }, function (d, $, t) {
    var n = t(7),
      e = t(50),
      r = t(65),
      l = Object.defineProperty;
    $.f = t(3) ? Object.defineProperty : function (d, $, t) {
      if (n(d), $ = r($, !0), n(t), e) try {
        return l(d, $, t)
      } catch (d) {}
      if ("get" in t || "set" in t) throw TypeError("Accessors not supported!");
      return "value" in t && (d[$] = t.value), d
    }
  }, function (d, $, t) {
    "use strict";

    function n() {
      var d = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
        $ = arguments[1];
      "string" == typeof $ && ($ = "^(?:" + $ + ")$");
      var t = d.match($);
      return t && t[0].length === d.length
    }
    Object.defineProperty($, "__esModule", {
      value: !0
    }), $.matches_entirely = n
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d && d.__esModule ? d : {
        default: d
      }
    }

    function e() {
      var d = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
        $ = arguments[1],
        t = arguments[2];
      "string" == typeof d && ("string" == typeof t ? (d = {
        phone: d,
        country: $
      }, $ = t) : d = {
        phone: d
      });
      var n = void 0;
      d.country && (n = this.metadata.countries[d.country]);
      var e = (0, f.parse_phone_number_and_country_phone_code)(d.phone, this.metadata),
        r = e.country_phone_code,
        u = e.number;
      if (r) {
        if (d.country && r !== (0, c.get_phone_code)(n)) return d.phone;
        n = (0, c.get_metadata_by_country_phone_code)(r, this.metadata)
      }
      if (!n) return d.phone;
      switch ($) {
        case "International":
          if (!u) return "+" + (0, c.get_phone_code)(n);
          var i = l(u, "International", !1, n);
          return "+" + (0, c.get_phone_code)(n) + " " + i;
        case "International_plaintext":
          return "+" + (0, c.get_phone_code)(n) + d.phone;
        case "National":
          return u ? l(u, "National", !1, n) : ""
      }
    }

    function r(d, $, t, n, e) {
      var r = new RegExp((0, c.get_format_pattern)($)),
        l = (0, c.get_format_national_prefix_formatting_rule)($, e),
        u = !l || l && (0, c.get_format_national_prefix_is_optional_when_formatting)($, e) && !n;
      if (!t && !u) return d.replace(r, (0, c.get_format_format)($).replace(s, l));
      var o = d.replace(r, t ? (0, c.get_format_international_format)($) : (0, c.get_format_format)($));
      return t ? i(o) : o
    }

    function l(d, $, t, n) {
      var e = u((0, c.get_formats)(n), d);
      return e ? r(d, e, "International" === $, t, n) : d
    }

    function u(d, $) {
      var t = !0,
        n = !1,
        e = void 0;
      try {
        for (var r, l = (0, a.default)(d); !(t = (r = l.next()).done); t = !0) {
          var u = r.value;
          if ((0, c.get_format_leading_digits_patterns)(u).length > 0) {
            var i = (0, c.get_format_leading_digits_patterns)(u)[(0, c.get_format_leading_digits_patterns)(u).length - 1];
            if (0 !== $.search(i)) continue
          }
          if ((0, _.matches_entirely)($, new RegExp((0, c.get_format_pattern)(u)))) return u
        }
      } catch (d) {
        n = !0, e = d
      } finally {
        try {
          !t && l.return && l.return()
        } finally {
          if (n) throw e
        }
      }
    }

    function i(d) {
      return d.replace(/[\(\)]/g, "").replace(/\-/g, " ").trim()
    }
    Object.defineProperty($, "__esModule", {
      value: !0
    }), $.FIRST_GROUP_PATTERN = void 0, $.default = e, $.format_national_number_using_format = r, $.format_national_number = l, $.choose_format_for_number = u, $.local_to_international_style = i;
    var o = t(13),
      a = n(o),
      _ = t(11),
      f = t(6),
      c = t(5),
      s = $.FIRST_GROUP_PATTERN = /(\$\d)/
  }, function (d, $, t) {
    d.exports = {
      default: t(41),
      __esModule: !0
    }
  }, function (d, $) {
    d.exports = function (d) {
      if (void 0 == d) throw TypeError("Can't call method on  " + d);
      return d
    }
  }, function (d, $, t) {
    var n = t(0),
      e = t(2),
      r = t(48),
      l = t(4),
      u = "prototype",
      i = function (d, $, t) {
        var o, a, _, f = d & i.F,
          c = d & i.G,
          s = d & i.S,
          p = d & i.P,
          h = d & i.B,
          m = d & i.W,
          y = c ? e : e[$] || (e[$] = {}),
          v = y[u],
          g = c ? n : s ? n[$] : (n[$] || {})[u];
        c && (t = $);
        for (o in t) a = !f && g && void 0 !== g[o], a && o in y || (_ = a ? g[o] : t[o], y[o] = c && "function" != typeof g[o] ? t[o] : h && a ? r(_, n) : m && g[o] == _ ? function (d) {
          var $ = function ($, t, n) {
            if (this instanceof d) {
              switch (arguments.length) {
                case 0:
                  return new d;
                case 1:
                  return new d($);
                case 2:
                  return new d($, t)
              }
              return new d($, t, n)
            }
            return d.apply(this, arguments)
          };
          return $[u] = d[u], $
        }(_) : p && "function" == typeof _ ? r(Function.call, _) : _, p && ((y.virtual || (y.virtual = {}))[o] = _, d & i.R && v && !v[o] && l(v, o, _)))
      };
    i.F = 1, i.G = 2, i.S = 4, i.P = 8, i.B = 16, i.W = 32, i.U = 64, i.R = 128, d.exports = i
  }, function (d, $) {
    d.exports = function (d) {
      try {
        return !!d()
      } catch (d) {
        return !0
      }
    }
  }, function (d, $) {
    d.exports = function (d) {
      return "object" == typeof d ? null !== d : "function" == typeof d
    }
  }, function (d, $, t) {
    var n = t(29)("keys"),
      e = t(31);
    d.exports = function (d) {
      return n[d] || (n[d] = e(d))
    }
  }, function (d, $) {
    var t = Math.ceil,
      n = Math.floor;
    d.exports = function (d) {
      return isNaN(d = +d) ? 0 : (d > 0 ? n : t)(d)
    }
  }, function (d, $, t) {
    var n = t(24),
      e = t(14);
    d.exports = function (d) {
      return n(e(d))
    }
  }, function (d, $) {
    var t = {}.toString;
    d.exports = function (d) {
      return t.call(d).slice(8, -1)
    }
  }, function (d, $, t) {
    var n = t(17),
      e = t(0).document,
      r = n(e) && n(e.createElement);
    d.exports = function (d) {
      return r ? e.createElement(d) : {}
    }
  }, function (d, $) {
    d.exports = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")
  }, function (d, $, t) {
    var n = t(21);
    d.exports = Object("z").propertyIsEnumerable(0) ? Object : function (d) {
      return "String" == n(d) ? d.split("") : Object(d)
    }
  }, function (d, $, t) {
    "use strict";
    var n = t(53),
      e = t(15),
      r = t(61),
      l = t(4),
      u = t(8),
      i = t(9),
      o = t(51),
      a = t(28),
      _ = t(58),
      f = t(1)("iterator"),
      c = !([].keys && "next" in [].keys()),
      s = "@@iterator",
      p = "keys",
      h = "values",
      m = function () {
        return this
      };
    d.exports = function (d, $, t, y, v, g, b) {
      o(t, $, y);
      var x, M, S, O = function (d) {
          if (!c && d in I) return I[d];
          switch (d) {
            case p:
              return function () {
                return new t(this, d)
              };
            case h:
              return function () {
                return new t(this, d)
              }
          }
          return function () {
            return new t(this, d)
          }
        },
        E = $ + " Iterator",
        T = v == h,
        A = !1,
        I = d.prototype,
        P = I[f] || I[s] || v && I[v],
        R = P || O(v),
        C = v ? T ? O("entries") : R : void 0,
        G = "Array" == $ ? I.entries || P : P;
      if (G && (S = _(G.call(new d)), S !== Object.prototype && (a(S, E, !0), n || u(S, f) || l(S, f, m))), T && P && P.name !== h && (A = !0, R = function () {
          return P.call(this)
        }), n && !b || !c && !A && I[f] || l(I, f, R), i[$] = R, i[E] = m, v)
        if (x = {
            values: T ? R : O(h),
            keys: g ? R : O(p),
            entries: C
          }, b)
          for (M in x) M in I || r(I, M, x[M]);
        else e(e.P + e.F * (c || A), $, x);
      return x
    }
  }, function (d, $, t) {
    var n = t(59),
      e = t(23);
    d.exports = Object.keys || function (d) {
      return n(d, e)
    }
  }, function (d, $) {
    d.exports = function (d, $) {
      return {
        enumerable: !(1 & d),
        configurable: !(2 & d),
        writable: !(4 & d),
        value: $
      }
    }
  }, function (d, $, t) {
    var n = t(10).f,
      e = t(8),
      r = t(1)("toStringTag");
    d.exports = function (d, $, t) {
      d && !e(d = t ? d : d.prototype, r) && n(d, r, {
        configurable: !0,
        value: $
      })
    }
  }, function (d, $, t) {
    var n = t(0),
      e = "__core-js_shared__",
      r = n[e] || (n[e] = {});
    d.exports = function (d) {
      return r[d] || (r[d] = {})
    }
  }, function (d, $, t) {
    var n = t(14);
    d.exports = function (d) {
      return Object(n(d))
    }
  }, function (d, $) {
    var t = 0,
      n = Math.random();
    d.exports = function (d) {
      return "Symbol(".concat(void 0 === d ? "" : d, ")_", (++t + n).toString(36))
    }
  }, function (d, $) {
    d.exports = {
      country_phone_code_to_countries: {
        1: ["US", "AG", "AI", "AS", "BB", "BM", "BS", "CA", "DM", "DO", "GD", "GU", "JM", "KN", "KY", "LC", "MP", "MS", "PR", "SX", "TC", "TT", "VC", "VG", "VI"],
        7: ["RU", "KZ"],
        20: ["EG"],
        27: ["ZA"],
        30: ["GR"],
        31: ["NL"],
        32: ["BE"],
        33: ["FR"],
        34: ["ES"],
        36: ["HU"],
        39: ["IT", "VA"],
        40: ["RO"],
        41: ["CH"],
        43: ["AT"],
        44: ["GB", "GG", "IM", "JE"],
        45: ["DK"],
        46: ["SE"],
        47: ["NO", "SJ"],
        48: ["PL"],
        49: ["DE"],
        51: ["PE"],
        52: ["MX"],
        53: ["CU"],
        54: ["AR"],
        55: ["BR"],
        56: ["CL"],
        57: ["CO"],
        58: ["VE"],
        60: ["MY"],
        61: ["AU", "CC", "CX"],
        62: ["ID"],
        63: ["PH"],
        64: ["NZ"],
        65: ["SG"],
        66: ["TH"],
        81: ["JP"],
        82: ["KR"],
        84: ["VN"],
        86: ["CN"],
        90: ["TR"],
        91: ["IN"],
        92: ["PK"],
        93: ["AF"],
        94: ["LK"],
        95: ["MM"],
        98: ["IR"],
        211: ["SS"],
        212: ["MA", "EH"],
        213: ["DZ"],
        216: ["TN"],
        218: ["LY"],
        220: ["GM"],
        221: ["SN"],
        222: ["MR"],
        223: ["ML"],
        224: ["GN"],
        225: ["CI"],
        226: ["BF"],
        227: ["NE"],
        228: ["TG"],
        229: ["BJ"],
        230: ["MU"],
        231: ["LR"],
        232: ["SL"],
        233: ["GH"],
        234: ["NG"],
        235: ["TD"],
        236: ["CF"],
        237: ["CM"],
        238: ["CV"],
        239: ["ST"],
        240: ["GQ"],
        241: ["GA"],
        242: ["CG"],
        243: ["CD"],
        244: ["AO"],
        245: ["GW"],
        246: ["IO"],
        247: ["AC"],
        248: ["SC"],
        249: ["SD"],
        250: ["RW"],
        251: ["ET"],
        252: ["SO"],
        253: ["DJ"],
        254: ["KE"],
        255: ["TZ"],
        256: ["UG"],
        257: ["BI"],
        258: ["MZ"],
        260: ["ZM"],
        261: ["MG"],
        262: ["RE", "YT"],
        263: ["ZW"],
        264: ["NA"],
        265: ["MW"],
        266: ["LS"],
        267: ["BW"],
        268: ["SZ"],
        269: ["KM"],
        290: ["SH", "TA"],
        291: ["ER"],
        297: ["AW"],
        298: ["FO"],
        299: ["GL"],
        350: ["GI"],
        351: ["PT"],
        352: ["LU"],
        353: ["IE"],
        354: ["IS"],
        355: ["AL"],
        356: ["MT"],
        357: ["CY"],
        358: ["FI", "AX"],
        359: ["BG"],
        370: ["LT"],
        371: ["LV"],
        372: ["EE"],
        373: ["MD"],
        374: ["AM"],
        375: ["BY"],
        376: ["AD"],
        377: ["MC"],
        378: ["SM"],
        380: ["UA"],
        381: ["RS"],
        382: ["ME"],
        385: ["HR"],
        386: ["SI"],
        387: ["BA"],
        389: ["MK"],
        420: ["CZ"],
        421: ["SK"],
        423: ["LI"],
        500: ["FK"],
        501: ["BZ"],
        502: ["GT"],
        503: ["SV"],
        504: ["HN"],
        505: ["NI"],
        506: ["CR"],
        507: ["PA"],
        508: ["PM"],
        509: ["HT"],
        590: ["GP", "BL", "MF"],
        591: ["BO"],
        592: ["GY"],
        593: ["EC"],
        594: ["GF"],
        595: ["PY"],
        596: ["MQ"],
        597: ["SR"],
        598: ["UY"],
        599: ["CW", "BQ"],
        670: ["TL"],
        672: ["NF"],
        673: ["BN"],
        674: ["NR"],
        675: ["PG"],
        676: ["TO"],
        677: ["SB"],
        678: ["VU"],
        679: ["FJ"],
        680: ["PW"],
        681: ["WF"],
        682: ["CK"],
        683: ["NU"],
        685: ["WS"],
        686: ["KI"],
        687: ["NC"],
        688: ["TV"],
        689: ["PF"],
        690: ["TK"],
        691: ["FM"],
        692: ["MH"],
        800: ["001"],
        808: ["001"],
        850: ["KP"],
        852: ["HK"],
        853: ["MO"],
        855: ["KH"],
        856: ["LA"],
        870: ["001"],
        878: ["001"],
        880: ["BD"],
        881: ["001"],
        882: ["001"],
        883: ["001"],
        886: ["TW"],
        888: ["001"],
        960: ["MV"],
        961: ["LB"],
        962: ["JO"],
        963: ["SY"],
        964: ["IQ"],
        965: ["KW"],
        966: ["SA"],
        967: ["YE"],
        968: ["OM"],
        970: ["PS"],
        971: ["AE"],
        972: ["IL"],
        973: ["BH"],
        974: ["QA"],
        975: ["BT"],
        976: ["MN"],
        977: ["NP"],
        979: ["001"],
        992: ["TJ"],
        993: ["TM"],
        994: ["AZ"],
        995: ["GE"],
        996: ["KG"],
        998: ["UZ"]
      },
      countries: {
        AC: ["247", "[46]\\d{4}|[01589]\\d{5}"],
        AD: ["376", "[16]\\d{5,8}|[37-9]\\d{5}", [
          ["(\\d{3})(\\d{3})", "$1 $2", ["[137-9]|6[0-8]"]],
          ["(\\d{4})(\\d{4})", "$1 $2", ["180", "180[02]"]],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["690"]]
        ]],
        AE: ["971", "[2-79]\\d{7,8}|800\\d{2,9}", [
          ["([2-4679])(\\d{3})(\\d{4})", "$1 $2 $3", ["[2-4679][2-8]"]],
          ["(5\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["5"]],
          ["([479]00)(\\d)(\\d{5})", "$1 $2 $3", ["[479]0"], "$1"],
          ["([68]00)(\\d{2,9})", "$1 $2", ["60|8"], "$1"]
        ], "0", "0$1"],
        AF: ["93", "[2-7]\\d{8}", [
          ["([2-7]\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["[2-7]"]]
        ], "0", "0$1"],
        AG: ["1", "[2589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "268"],
        AI: ["1", "[2589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "264"],
        AL: ["355", "[2-57]\\d{7}|6\\d{8}|8\\d{5,7}|9\\d{5}", [
          ["(4)(\\d{3})(\\d{4})", "$1 $2 $3", ["4[0-6]"]],
          ["(6[6-9])(\\d{3})(\\d{4})", "$1 $2 $3", ["6"]],
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["[2358][2-5]|4[7-9]"]],
          ["(\\d{3})(\\d{3,5})", "$1 $2", ["[235][16-9]|8[016-9]|[79]"]]
        ], "0", "0$1"],
        AM: ["374", "[1-9]\\d{7}", [
          ["(\\d{2})(\\d{6})", "$1 $2", ["1|47"]],
          ["(\\d{2})(\\d{6})", "$1 $2", ["4[1349]|[5-7]|9[1-9]"], "0$1"],
          ["(\\d{3})(\\d{5})", "$1 $2", ["[23]"]],
          ["(\\d{3})(\\d{2})(\\d{3})", "$1 $2 $3", ["8|90"], "0 $1"]
        ], "0", "(0$1)"],
        AO: ["244", "[29]\\d{8}", [
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3"]
        ]],
        AR: ["54", "11\\d{8}|[2368]\\d{9}|9\\d{10}", [
          ["([68]\\d{2})(\\d{3})(\\d{4})", "$1-$2-$3", ["[68]"]],
          ["(9)(11)(\\d{4})(\\d{4})", "$2 15-$3-$4", ["911"], null, null, "$1 $2 $3-$4"],
          ["(9)(\\d{3})(\\d{3})(\\d{4})", "$2 15-$3-$4", ["9(?:2[234689]|3[3-8])", "9(?:2(?:2[013]|3[067]|49|6[01346]|80|9[147-9])|3(?:36|4[1-358]|5[138]|6[24]|7[069]|8[013578]))", "9(?:2(?:2(?:0[013-9]|[13])|3(?:0[013-9]|[67])|49|6(?:[0136]|4[0-59])|8|9(?:[19]|44|7[013-9]|8[14]))|3(?:36|4(?:[12]|3[456]|[58]4)|5(?:1|3[0-24-689]|8[46])|6|7[069]|8(?:[01]|34|[578][45])))", "9(?:2(?:2(?:0[013-9]|[13])|3(?:0[013-9]|[67])|49|6(?:[0136]|4[0-59])|8|9(?:[19]|44|7[013-9]|8[14]))|3(?:36|4(?:[12]|3(?:4|5[014]|6[1239])|[58]4)|5(?:1|3[0-24-689]|8[46])|6|7[069]|8(?:[01]|34|[578][45])))"], null, null, "$1 $2 $3-$4"],
          ["(9)(\\d{4})(\\d{2})(\\d{4})", "$2 15-$3-$4", ["9[23]"], null, null, "$1 $2 $3-$4"],
          ["(11)(\\d{4})(\\d{4})", "$1 $2-$3", ["1"], null, "true"],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2-$3", ["2(?:2[013]|3[067]|49|6[01346]|80|9[147-9])|3(?:36|4[1-358]|5[138]|6[24]|7[069]|8[013578])", "2(?:2(?:0[013-9]|[13])|3(?:0[013-9]|[67])|49|6(?:[0136]|4[0-59])|8|9(?:[19]|44|7[013-9]|8[14]))|3(?:36|4(?:[12]|3[456]|[58]4)|5(?:1|3[0-24-689]|8[46])|6|7[069]|8(?:[01]|34|[578][45]))", "2(?:2(?:0[013-9]|[13])|3(?:0[013-9]|[67])|49|6(?:[0136]|4[0-59])|8|9(?:[19]|44|7[013-9]|8[14]))|3(?:36|4(?:[12]|3(?:4|5[014]|6[1239])|[58]4)|5(?:1|3[0-24-689]|8[46])|6|7[069]|8(?:[01]|34|[578][45]))"], null, "true"],
          ["(\\d{4})(\\d{2})(\\d{4})", "$1 $2-$3", ["[23]"], null, "true"]
        ], "0", "0$1", "0?(?:(11|2(?:2(?:02?|[13]|2[13-79]|4[1-6]|5[2457]|6[124-8]|7[1-4]|8[13-6]|9[1267])|3(?:02?|1[467]|2[03-6]|3[13-8]|[49][2-6]|5[2-8]|[67])|4(?:7[3-578]|9)|6(?:[0136]|2[24-6]|4[6-8]?|5[15-8])|80|9(?:0[1-3]|[19]|2\\d|3[1-6]|4[02568]?|5[2-4]|6[2-46]|72?|8[23]?))|3(?:3(?:2[79]|6|8[2578])|4(?:0[0-24-9]|[12]|3[5-8]?|4[24-7]|5[4-68]?|6[02-9]|7[126]|8[2379]?|9[1-36-8])|5(?:1|2[1245]|3[237]?|4[1-46-9]|6[2-4]|7[1-6]|8[2-5]?)|6[24]|7(?:[069]|1[1568]|2[15]|3[145]|4[13]|5[14-8]|7[2-57]|8[126])|8(?:[01]|2[15-7]|3[2578]?|4[13-6]|5[4-8]?|6[1-357-9]|7[36-8]?|8[5-8]?|9[124])))?15)?", "9$1"],
        AS: ["1", "[5689]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "684"],
        AT: ["43", "[1-9]\\d{3,12}", [
          ["(116\\d{3})", "$1", ["116"], "$1"],
          ["(1)(\\d{3,12})", "$1 $2", ["1"]],
          ["(5\\d)(\\d{3,5})", "$1 $2", ["5[079]"]],
          ["(5\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["5[079]"]],
          ["(5\\d)(\\d{4})(\\d{4,7})", "$1 $2 $3", ["5[079]"]],
          ["(\\d{3})(\\d{3,10})", "$1 $2", ["316|46|51|732|6(?:5[0-3579]|[6-9])|7(?:[28]0)|[89]"]],
          ["(\\d{4})(\\d{3,9})", "$1 $2", ["2|3(?:1[1-578]|[3-8])|4[2378]|5[2-6]|6(?:[12]|4[1-9]|5[468])|7(?:2[1-8]|35|4[1-8]|[5-79])"]]
        ], "0", "0$1"],
        AU: ["61", "[1-578]\\d{5,9}", [
          ["([2378])(\\d{4})(\\d{4})", "$1 $2 $3", ["[2378]"], "(0$1)"],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["[45]|14"], "0$1"],
          ["(16)(\\d{3})(\\d{2,4})", "$1 $2 $3", ["16"], "0$1"],
          ["(1[389]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["1(?:[38]0|90)", "1(?:[38]00|90)"], "$1"],
          ["(180)(2\\d{3})", "$1 $2", ["180", "1802"], "$1"],
          ["(19\\d)(\\d{3})", "$1 $2", ["19[13]"], "$1"],
          ["(19\\d{2})(\\d{4})", "$1 $2", ["19[679]"], "$1"],
          ["(13)(\\d{2})(\\d{2})", "$1 $2 $3", ["13[1-9]"], "$1"]
        ], "0", null, null, null, null, null, ["[237]\\d{8}|8(?:[6-8]\\d{3}|9(?:[02-9]\\d{2}|1(?:[0-57-9]\\d|6[0135-9])))\\d{4}", "14(?:5\\d|71)\\d{5}|4(?:[0-3]\\d|4[47-9]|5[0-25-9]|6[6-9]|7[02-9]|8[147-9]|9[017-9])\\d{6}", "180(?:0\\d{3}|2)\\d{3}", "19(?:0[0126]\\d|[679])\\d{5}", "500\\d{6}", null, null, "16\\d{3,7}", "550\\d{6}"]],
        AW: ["297", "[25-9]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2"]
        ]],
        AX: ["358", "[135]\\d{5,9}|[27]\\d{4,9}|4\\d{5,10}|6\\d{7,8}|8\\d{6,9}", [
          ["(\\d{3})(\\d{3,7})", "$1 $2", ["(?:[1-3]00|[6-8]0)"]],
          ["(116\\d{3})", "$1", ["116"], "$1"],
          ["(\\d{2})(\\d{4,10})", "$1 $2", ["[14]|2[09]|50|7[135]"]],
          ["(\\d)(\\d{4,11})", "$1 $2", ["[25689][1-8]|3"]]
        ], "0", "0$1", null, null, null, null, ["18[1-8]\\d{3,9}", "4\\d{5,10}|50\\d{4,8}", "800\\d{4,7}", "[67]00\\d{5,6}", null, null, "[13]0\\d{4,8}|2(?:0(?:[016-8]\\d{3,7}|[2-59]\\d{2,7})|9\\d{4,8})|60(?:[12]\\d{5,6}|6\\d{7})|7(?:1\\d{7}|3\\d{8}|5[03-9]\\d{2,7})"]],
        AZ: ["994", "[1-9]\\d{8}", [
          ["(\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["(?:1[28]|2(?:[45]2|[0-36])|365)"]],
          ["(\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[4-8]"], "0$1"],
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["9"], "0$1"]
        ], "0", "(0$1)"],
        BA: ["387", "[3-9]\\d{7,8}", [
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2-$3", ["[3-5]"]],
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["6[1-356]|[7-9]"]],
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3 $4", ["6[047]"]]
        ], "0", "0$1"],
        BB: ["1", "[2589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "246"],
        BD: ["880", "[2-79]\\d{5,9}|1\\d{9}|8[0-7]\\d{4,8}", [
          ["(2)(\\d{7,8})", "$1-$2", ["2"]],
          ["(\\d{2})(\\d{4,6})", "$1-$2", ["[3-79]1"]],
          ["(\\d{4})(\\d{3,6})", "$1-$2", ["1|3(?:0|[2-58]2)|4(?:0|[25]2|3[23]|[4689][25])|5(?:[02-578]2|6[25])|6(?:[0347-9]2|[26][25])|7[02-9]2|8(?:[023][23]|[4-7]2)|9(?:[02][23]|[458]2|6[016])"]],
          ["(\\d{3})(\\d{3,7})", "$1-$2", ["[3-79][2-9]|8"]]
        ], "0", "0$1"],
        BE: ["32", "[1-9]\\d{7,8}", [
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["4[6-9]"]],
          ["(\\d)(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[23]|4[23]|9[2-4]"]],
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[156]|7[018]|8(?:0[1-9]|[1-79])"]],
          ["(\\d{3})(\\d{2})(\\d{3})", "$1 $2 $3", ["(?:80|9)0"]]
        ], "0", "0$1"],
        BF: ["226", "[25-7]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        BG: ["359", "[23567]\\d{5,7}|[489]\\d{6,8}", [
          ["(2)(\\d)(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["2"]],
          ["(2)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["2"]],
          ["(\\d{3})(\\d{4})", "$1 $2", ["43[124-7]|70[1-9]"]],
          ["(\\d{3})(\\d{3})(\\d{2})", "$1 $2 $3", ["43[124-7]|70[1-9]"]],
          ["(\\d{3})(\\d{2})(\\d{3})", "$1 $2 $3", ["[78]00"]],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["999"]],
          ["(\\d{2})(\\d{3})(\\d{2,3})", "$1 $2 $3", ["[356]|4[124-7]|7[1-9]|8[1-6]|9[1-7]"]],
          ["(\\d{2})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["48|8[7-9]|9[08]"]]
        ], "0", "0$1"],
        BH: ["973", "[136-9]\\d{7}", [
          ["(\\d{4})(\\d{4})", "$1 $2"]
        ]],
        BI: ["257", "[267]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        BJ: ["229", "[2689]\\d{7}|7\\d{3}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        BL: ["590", "[56]\\d{8}", [
          ["([56]90)(\\d{2})(\\d{4})", "$1 $2-$3"]
        ], "0", null, null, null, null, null, ["590(?:2[7-9]|5[12]|87)\\d{4}", "690(?:0[0-7]|[1-9]\\d)\\d{4}"]],
        BM: ["1", "[4589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "441"],
        BN: ["673", "[2-578]\\d{6}", [
          ["([2-578]\\d{2})(\\d{4})", "$1 $2"]
        ]],
        BO: ["591", "[23467]\\d{7}", [
          ["([234])(\\d{7})", "$1 $2", ["[234]"]],
          ["([67]\\d{7})", "$1", ["[67]"]]
        ], "0", null, "0(1\\d)?"],
        BQ: ["599", "[347]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2", ["[13-7]"]],
          ["(9)(\\d{3})(\\d{4})", "$1 $2 $3", ["9"]]
        ], null, null, null, null, null, null, ["(?:318[023]|416[023]|7(?:1[578]|50)\\d)\\d{3}", "(?:318[14-68]|416[15-9]|7(?:0[01]|7[07]|[89]\\d)\\d)\\d{3}"]],
        BR: ["55", "[1-46-9]\\d{7,10}|5(?:[0-4]\\d{7,9}|5(?:[2-8]\\d{7}|9\\d{7,8}))", [
          ["(\\d{2})(\\d{5})(\\d{4})", "$1 $2-$3", ["(?:[14689][1-9]|2[12478]|3[1-578]|5[1-5]|7[13-579])9"], "($1)"],
          ["(\\d{2})(\\d{4})(\\d{4})", "$1 $2-$3", ["[1-9][1-9]"], "($1)"],
          ["(\\d{4})(\\d{4})", "$1-$2", ["(?:300|40(?:0|20))"]],
          ["([3589]00)(\\d{2,3})(\\d{4})", "$1 $2 $3", ["[3589]00"], "0$1"]
        ], "0", null, "0(?:(1[245]|2[1-35]|31|4[13]|[56]5|99)(\\d{10,11}))?", "$2"],
        BS: ["1", "[2589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "242"],
        BT: ["975", "[1-8]\\d{6,7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["1|77"]],
          ["([2-8])(\\d{3})(\\d{3})", "$1 $2 $3", ["[2-68]|7[246]"]]
        ]],
        BW: ["267", "[2-79]\\d{6,7}", [
          ["(\\d{3})(\\d{4})", "$1 $2", ["[2-6]"]],
          ["(7\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["7"]],
          ["(90)(\\d{5})", "$1 $2", ["9"]]
        ]],
        BY: ["375", "[1-4]\\d{8}|800\\d{3,7}|[89]\\d{9,10}", [
          ["(\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1 $2-$3-$4", ["17[0-3589]|2[4-9]|[34]", "17(?:[02358]|1[0-2]|9[0189])|2[4-9]|[34]"], "8 0$1"],
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2-$3-$4", ["1(?:5[24]|6[235]|7[467])|2(?:1[246]|2[25]|3[26])", "1(?:5[24]|6(?:2|3[04-9]|5[0346-9])|7(?:[46]|7[37-9]))|2(?:1[246]|2[25]|3[26])"], "8 0$1"],
          ["(\\d{4})(\\d{2})(\\d{3})", "$1 $2-$3", ["1(?:5[169]|6[3-5]|7[179])|2(?:1[35]|2[34]|3[3-5])", "1(?:5[169]|6(?:3[1-3]|4|5[125])|7(?:1[3-9]|7[0-24-6]|9[2-7]))|2(?:1[35]|2[34]|3[3-5])"], "8 0$1"],
          ["([89]\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["8[01]|9"], "8 $1"],
          ["(82\\d)(\\d{4})(\\d{4})", "$1 $2 $3", ["82"], "8 $1"],
          ["(800)(\\d{3})", "$1 $2", ["800"], "8 $1"],
          ["(800)(\\d{2})(\\d{2,4})", "$1 $2 $3", ["800"], "8 $1"]
        ], "8", null, "8?0?"],
        BZ: ["501", "[2-8]\\d{6}|0\\d{10}", [
          ["(\\d{3})(\\d{4})", "$1-$2", ["[2-8]"]],
          ["(0)(800)(\\d{4})(\\d{3})", "$1-$2-$3-$4", ["0"]]
        ]],
        CA: ["1", "[2-9]\\d{9}|3\\d{6}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, null, ["(?:2(?:04|[23]6|[48]9|50)|3(?:06|43|65)|4(?:03|1[68]|3[178]|50)|5(?:06|1[49]|48|79|8[17])|6(?:0[04]|13|22|39|47)|7(?:0[59]|78|8[02])|8(?:[06]7|19|25|73)|90[25])[2-9]\\d{6}|310\\d{4}", "(?:2(?:04|[23]6|[48]9|50)|3(?:06|43|65)|4(?:03|1[68]|3[178]|50)|5(?:06|1[49]|48|79|8[17])|6(?:0[04]|13|22|39|47)|7(?:0[59]|78|8[02])|8(?:[06]7|19|25|73)|90[25])[2-9]\\d{6}", "8(?:00|44|55|66|77|88)[2-9]\\d{6}|310\\d{4}", "900[2-9]\\d{6}", "5(?:00|22|33|44|66|77|88)[2-9]\\d{6}"]],
        CC: ["61", "[1458]\\d{5,9}", [
          ["([2378])(\\d{4})(\\d{4})", "$1 $2 $3", ["[2378]"], "(0$1)"],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["[45]|14"], "0$1"],
          ["(16)(\\d{3})(\\d{2,4})", "$1 $2 $3", ["16"], "0$1"],
          ["(1[389]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["1(?:[38]0|90)", "1(?:[38]00|90)"], "$1"],
          ["(180)(2\\d{3})", "$1 $2", ["180", "1802"], "$1"],
          ["(19\\d)(\\d{3})", "$1 $2", ["19[13]"], "$1"],
          ["(19\\d{2})(\\d{4})", "$1 $2", ["19[679]"], "$1"],
          ["(13)(\\d{2})(\\d{2})", "$1 $2 $3", ["13[1-9]"], "$1"]
        ], "0", null, null, null, null, null, ["89162\\d{4}", "14(?:5\\d|71)\\d{5}|4(?:[0-2]\\d|3[0-57-9]|4[47-9]|5[0-25-9]|6[6-9]|7[02-9]|8[147-9]|9[017-9])\\d{6}", "180(?:0\\d{3}|2)\\d{3}", "190[0126]\\d{6}", "500\\d{6}", null, null, null, "550\\d{6}"]],
        CD: ["243", "[2-6]\\d{6}|[18]\\d{6,8}|9\\d{8}", [
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["12"]],
          ["([89]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["8[0-2459]|9"]],
          ["(\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3", ["88"]],
          ["(\\d{2})(\\d{5})", "$1 $2", ["[1-6]"]]
        ], "0", "0$1"],
        CF: ["236", "[278]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        CG: ["242", "[028]\\d{8}", [
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["[02]"]],
          ["(\\d)(\\d{4})(\\d{4})", "$1 $2 $3", ["8"]]
        ]],
        CH: ["41", "[2-9]\\d{8}|860\\d{9}", [
          ["([2-9]\\d)(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[2-7]|[89]1"]],
          ["([89]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["8[047]|90"]],
          ["(\\d{3})(\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4 $5", ["860"]]
        ], "0", "0$1"],
        CI: ["225", "[02-8]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        CK: ["682", "[2-8]\\d{4}", [
          ["(\\d{2})(\\d{3})", "$1 $2"]
        ]],
        CL: ["56", "(?:[2-9]|600|123)\\d{7,8}", [
          ["(\\d)(\\d{4})(\\d{4})", "$1 $2 $3", ["2[23]"], "($1)"],
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["[357]|4[1-35]|6[13-57]"], "($1)"],
          ["(9)(\\d{4})(\\d{4})", "$1 $2 $3", ["9"]],
          ["(44)(\\d{3})(\\d{4})", "$1 $2 $3", ["44"]],
          ["([68]00)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["60|8"], "$1"],
          ["(600)(\\d{3})(\\d{2})(\\d{3})", "$1 $2 $3 $4", ["60"], "$1"],
          ["(1230)(\\d{3})(\\d{4})", "$1 $2 $3", ["1"], "$1"],
          ["(\\d{5})(\\d{4})", "$1 $2", ["219"], "($1)"]
        ], "0", "0$1", "0|(1(?:1[0-69]|2[0-57]|5[13-58]|69|7[0167]|8[018]))"],
        CM: ["237", "[2368]\\d{7,8}", [
          ["([26])(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4 $5", ["[26]"]],
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[23]|88"]],
          ["(800)(\\d{2})(\\d{3})", "$1 $2 $3", ["80"]]
        ]],
        CN: ["86", "[1-7]\\d{6,11}|8[0-357-9]\\d{6,9}|9\\d{7,10}", [
          ["(80\\d{2})(\\d{4})", "$1 $2", ["80[2678]"], "0$1", "true"],
          ["([48]00)(\\d{3})(\\d{4})", "$1 $2 $3", ["[48]00"]],
          ["(\\d{2})(\\d{5,6})", "$1 $2", ["(?:10|2\\d)[19]", "(?:10|2\\d)(?:10|9[56])", "(?:10|2\\d)(?:100|9[56])"], "0$1"],
          ["(\\d{3})(\\d{5,6})", "$1 $2", ["[3-9]", "[3-9]\\d{2}[19]", "[3-9]\\d{2}(?:10|9[56])"], "0$1"],
          ["(21)(\\d{4})(\\d{4,6})", "$1 $2 $3", ["21"], "0$1", "true"],
          ["([12]\\d)(\\d{4})(\\d{4})", "$1 $2 $3", ["10[1-9]|2[02-9]", "10[1-9]|2[02-9]", "10(?:[1-79]|8(?:[1-9]|0[1-9]))|2[02-9]"], "0$1", "true"],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["3(?:1[02-9]|35|49|5|7[02-68]|9[1-68])|4(?:1[02-9]|2[179]|[35][2-9]|6[4789]|7\\d|8[23])|5(?:3[03-9]|4[36]|5[02-9]|6[1-46]|7[028]|80|9[2-46-9])|6(?:3[1-5]|6[0238]|9[12])|7(?:01|[1579]|2[248]|3[04-9]|4[3-6]|6[2368])|8(?:1[236-8]|2[5-7]|3|5[1-9]|7[02-9]|8[3678]|9[1-7])|9(?:0[1-3689]|1[1-79]|[379]|4[13]|5[1-5])"], "0$1", "true"],
          ["(\\d{3})(\\d{4})(\\d{4})", "$1 $2 $3", ["3(?:11|7[179])|4(?:[15]1|3[1-35])|5(?:1|2[37]|3[12]|51|7[13-79]|9[15])|7(?:31|5[457]|6[09]|91)|8(?:[57]1|98)"], "0$1", "true"],
          ["(\\d{4})(\\d{3})(\\d{4})", "$1 $2 $3", ["807", "8078"], "0$1", "true"],
          ["(\\d{3})(\\d{4})(\\d{4})", "$1 $2 $3", ["1[3-578]"]],
          ["(10800)(\\d{3})(\\d{4})", "$1 $2 $3", ["108", "1080", "10800"]],
          ["(\\d{3})(\\d{7,8})", "$1 $2", ["950"]]
        ], "0", null, "(1(?:[129]\\d{3}|79\\d{2}))|0"],
        CO: ["57", "(?:[13]\\d{0,3}|[24-8])\\d{7}", [
          ["(\\d)(\\d{7})", "$1 $2", ["1(?:8[2-9]|9[0-3]|[2-7])|[24-8]", "1(?:8[2-9]|9(?:09|[1-3])|[2-7])|[24-8]"], "($1)"],
          ["(\\d{3})(\\d{7})", "$1 $2", ["3"]],
          ["(1)(\\d{3})(\\d{7})", "$1-$2-$3", ["1(?:80|9[04])", "1(?:800|9(?:0[01]|4[78]))"], "0$1", null, "$1 $2 $3"]
        ], "0", null, "0([3579]|4(?:44|56))?"],
        CR: ["506", "[24-9]\\d{7,9}", [
          ["(\\d{4})(\\d{4})", "$1 $2", ["[24-7]|8[3-9]"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1-$2-$3", ["[89]0"]]
        ], null, null, "(19(?:0[012468]|1[09]|20|66|77|99))"],
        CU: ["53", "[2-57]\\d{5,7}", [
          ["(\\d)(\\d{6,7})", "$1 $2", ["7"]],
          ["(\\d{2})(\\d{4,6})", "$1 $2", ["[2-4]"]],
          ["(\\d)(\\d{7})", "$1 $2", ["5"], "0$1"]
        ], "0", "(0$1)"],
        CV: ["238", "[259]\\d{6}", [
          ["(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3"]
        ]],
        CW: ["599", "[169]\\d{6,7}", [
          ["(\\d{3})(\\d{4})", "$1 $2", ["[13-7]"]],
          ["(9)(\\d{3})(\\d{4})", "$1 $2 $3", ["9"]]
        ], null, null, null, null, null, null, ["9(?:[48]\\d{2}|50\\d|7(?:2[0-24]|[34]\\d|6[35-7]|77|8[7-9]))\\d{4}", "9(?:5(?:[12467]\\d|3[01])|6(?:[15-9]\\d|3[01]))\\d{4}", null, null, null, null, null, "955\\d{5}"]],
        CX: ["61", "[1458]\\d{5,9}", [
          ["([2378])(\\d{4})(\\d{4})", "$1 $2 $3", ["[2378]"], "(0$1)"],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["[45]|14"], "0$1"],
          ["(16)(\\d{3})(\\d{2,4})", "$1 $2 $3", ["16"], "0$1"],
          ["(1[389]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["1(?:[38]0|90)", "1(?:[38]00|90)"], "$1"],
          ["(180)(2\\d{3})", "$1 $2", ["180", "1802"], "$1"],
          ["(19\\d)(\\d{3})", "$1 $2", ["19[13]"], "$1"],
          ["(19\\d{2})(\\d{4})", "$1 $2", ["19[679]"], "$1"],
          ["(13)(\\d{2})(\\d{2})", "$1 $2 $3", ["13[1-9]"], "$1"]
        ], "0", null, null, null, null, null, ["89164\\d{4}", "14(?:5\\d|71)\\d{5}|4(?:[0-2]\\d|3[0-57-9]|4[47-9]|5[0-25-9]|6[6-9]|7[02-9]|8[147-9]|9[017-9])\\d{6}", "180(?:0\\d{3}|2)\\d{3}", "190[0126]\\d{6}", "500\\d{6}", null, null, null, "550\\d{6}"]],
        CY: ["357", "[257-9]\\d{7}", [
          ["(\\d{2})(\\d{6})", "$1 $2"]
        ]],
        CZ: ["420", "[2-8]\\d{8}|9\\d{8,11}", [
          ["([2-9]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["[2-8]|9[015-7]"]],
          ["(96\\d)(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["96"]],
          ["(9\\d)(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["9[36]"]]
        ]],
        DE: ["49", "[1-35-9]\\d{3,14}|4(?:[0-8]\\d{4,12}|9(?:[0-37]\\d|4(?:[1-35-8]|4\\d?)|5\\d{1,2}|6[1-8]\\d?)\\d{2,8})", [
          ["(1\\d{2})(\\d{7,8})", "$1 $2", ["1[67]"]],
          ["(15\\d{3})(\\d{6})", "$1 $2", ["15[0568]"]],
          ["(1\\d{3})(\\d{7})", "$1 $2", ["15"]],
          ["(\\d{2})(\\d{3,11})", "$1 $2", ["3[02]|40|[68]9"]],
          ["(\\d{3})(\\d{3,11})", "$1 $2", ["2(?:\\d1|0[2389]|1[24]|28|34)|3(?:[3-9][15]|40)|[4-8][1-9]1|9(?:06|[1-9]1)"]],
          ["(\\d{4})(\\d{2,11})", "$1 $2", ["[24-6]|[7-9](?:\\d[1-9]|[1-9]\\d)|3(?:[3569][02-46-9]|4[2-4679]|7[2-467]|8[2-46-8])", "[24-6]|[7-9](?:\\d[1-9]|[1-9]\\d)|3(?:3(?:0[1-467]|2[127-9]|3[124578]|[46][1246]|7[1257-9]|8[1256]|9[145])|4(?:2[135]|3[1357]|4[13578]|6[1246]|7[1356]|9[1346])|5(?:0[14]|2[1-3589]|3[1357]|4[1246]|6[1-4]|7[1346]|8[13568]|9[1246])|6(?:0[356]|2[1-489]|3[124-6]|4[1347]|6[13]|7[12579]|8[1-356]|9[135])|7(?:2[1-7]|3[1357]|4[145]|6[1-5]|7[1-4])|8(?:21|3[1468]|4[1347]|6[0135-9]|7[1467]|8[136])|9(?:0[12479]|2[1358]|3[1357]|4[134679]|6[1-9]|7[136]|8[147]|9[1468]))"]],
          ["(3\\d{4})(\\d{1,10})", "$1 $2", ["3"]],
          ["(800)(\\d{7,12})", "$1 $2", ["800"]],
          ["(\\d{3})(\\d)(\\d{4,10})", "$1 $2 $3", ["(?:18|90)0|137", "1(?:37|80)|900[1359]"]],
          ["(1\\d{2})(\\d{5,11})", "$1 $2", ["181"]],
          ["(18\\d{3})(\\d{6})", "$1 $2", ["185", "1850", "18500"]],
          ["(18\\d{2})(\\d{7})", "$1 $2", ["18[68]"]],
          ["(18\\d)(\\d{8})", "$1 $2", ["18[2-579]"]],
          ["(700)(\\d{4})(\\d{4})", "$1 $2 $3", ["700"]],
          ["(138)(\\d{4})", "$1 $2", ["138"]],
          ["(15[013-68])(\\d{2})(\\d{8})", "$1 $2 $3", ["15[013-68]"]],
          ["(15[279]\\d)(\\d{2})(\\d{7})", "$1 $2 $3", ["15[279]"]],
          ["(1[67]\\d)(\\d{2})(\\d{7,8})", "$1 $2 $3", ["1(?:6[023]|7)"]]
        ], "0", "0$1"],
        DJ: ["253", "[27]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        DK: ["45", "[2-9]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        DM: ["1", "[57-9]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "767"],
        DO: ["1", "[589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "8[024]9"],
        DZ: ["213", "(?:[1-4]|[5-9]\\d)\\d{7}", [
          ["([1-4]\\d)(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[1-4]"]],
          ["([5-8]\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[5-8]"]],
          ["(9\\d)(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["9"]]
        ], "0", "0$1"],
        EC: ["593", "1\\d{9,10}|[2-8]\\d{7}|9\\d{8}", [
          ["(\\d)(\\d{3})(\\d{4})", "$1 $2-$3", ["[247]|[356][2-8]"], null, null, "$1-$2-$3"],
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["9"], "0$1"],
          ["(1800)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["1"], "$1"]
        ], "0", "(0$1)"],
        EE: ["372", "1\\d{3,4}|[3-9]\\d{6,7}|800\\d{6,7}", [
          ["([3-79]\\d{2})(\\d{4})", "$1 $2", ["[369]|4[3-8]|5(?:[0-2]|5[0-478]|6[45])|7[1-9]", "[369]|4[3-8]|5(?:[02]|1(?:[0-8]|95)|5[0-478]|6(?:4[0-4]|5[1-589]))|7[1-9]"]],
          ["(70)(\\d{2})(\\d{4})", "$1 $2 $3", ["70"]],
          ["(8000)(\\d{3})(\\d{3})", "$1 $2 $3", ["800", "8000"]],
          ["([458]\\d{3})(\\d{3,4})", "$1 $2", ["40|5|8(?:00|[1-5])", "40|5|8(?:00[1-9]|[1-5])"]]
        ]],
        EG: ["20", "1\\d{4,9}|[2456]\\d{8}|3\\d{7}|[89]\\d{8,9}", [
          ["(\\d)(\\d{7,8})", "$1 $2", ["[23]"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["1[012]|[89]00"]],
          ["(\\d{2})(\\d{6,7})", "$1 $2", ["1[35]|[4-6]|[89][2-9]"]]
        ], "0", "0$1"],
        EH: ["212", "[5-9]\\d{8}", [
          ["([5-7]\\d{2})(\\d{6})", "$1-$2", ["5(?:2[015-7]|3[0-4])|[67]"]],
          ["([58]\\d{3})(\\d{5})", "$1-$2", ["5(?:2[2-489]|3[5-9]|92)|892", "5(?:2(?:[2-48]|90)|3(?:[5-79]|80)|924)|892"]],
          ["(5\\d{4})(\\d{4})", "$1-$2", ["5(?:29|38)", "5(?:29|38)[89]"]],
          ["([5]\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["5(?:4[067]|5[03])"]],
          ["(8[09])(\\d{7})", "$1-$2", ["8(?:0|9[013-9])"]]
        ], "0", "0$1", null, null, null, "528[89]"],
        ER: ["291", "[178]\\d{6}", [
          ["(\\d)(\\d{3})(\\d{3})", "$1 $2 $3"]
        ], "0", "0$1"],
        ES: ["34", "[5-9]\\d{8}", [
          ["([89]00)(\\d{3})(\\d{3})", "$1 $2 $3", ["[89]00"]],
          ["([5-9]\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[568]|[79][0-8]"]]
        ]],
        ET: ["251", "[1-59]\\d{8}", [
          ["([1-59]\\d)(\\d{3})(\\d{4})", "$1 $2 $3"]
        ], "0", "0$1"],
        FI: ["358", "1\\d{4,11}|[2-9]\\d{4,10}", [
          ["(\\d{3})(\\d{3,7})", "$1 $2", ["(?:[1-3]00|[6-8]0)"]],
          ["(116\\d{3})", "$1", ["116"], "$1"],
          ["(\\d{2})(\\d{4,10})", "$1 $2", ["[14]|2[09]|50|7[135]"]],
          ["(\\d)(\\d{4,11})", "$1 $2", ["[25689][1-8]|3"]]
        ], "0", "0$1", null, null, null, null, ["1(?:[3569][1-8]\\d{3,9}|[47]\\d{5,10})|2[1-8]\\d{3,9}|3(?:[1-8]\\d{3,9}|9\\d{4,8})|[5689][1-8]\\d{3,9}", "4\\d{5,10}|50\\d{4,8}", "800\\d{4,7}", "[67]00\\d{5,6}", null, null, "[13]0\\d{4,8}|2(?:0(?:[016-8]\\d{3,7}|[2-59]\\d{2,7})|9\\d{4,8})|60(?:[12]\\d{5,6}|6\\d{7})|7(?:1\\d{7}|3\\d{8}|5[03-9]\\d{2,7})"]],
        FJ: ["679", "[36-9]\\d{6}|0\\d{10}", [
          ["(\\d{3})(\\d{4})", "$1 $2", ["[36-9]"]],
          ["(\\d{4})(\\d{3})(\\d{4})", "$1 $2 $3", ["0"]]
        ]],
        FK: ["500", "[2-7]\\d{4}"],
        FM: ["691", "[39]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2"]
        ]],
        FO: ["298", "[2-9]\\d{5}", [
          ["(\\d{6})", "$1"]
        ], null, null, "(10(?:01|[12]0|88))"],
        FR: ["33", "[1-9]\\d{8}", [
          ["([1-79])(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4 $5", ["[1-79]"]],
          ["(8\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["8"], "0 $1"]
        ], "0", "0$1"],
        GA: ["241", "0?\\d{7}", [
          ["(\\d)(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[2-7]"], "0$1"],
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["0"]]
        ]],
        GB: ["44", "\\d{7,10}", [
          ["(\\d{2})(\\d{4})(\\d{4})", "$1 $2 $3", ["2|5[56]|7(?:0|6[013-9])", "2|5[56]|7(?:0|6(?:[013-9]|2[0-35-9]))"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["1(?:1|\\d1)|3|9[018]"]],
          ["(\\d{5})(\\d{4,5})", "$1 $2", ["1(?:38|5[23]|69|76|94)", "1(?:387|5(?:24|39)|697|768|946)", "1(?:3873|5(?:242|39[456])|697[347]|768[347]|9467)"]],
          ["(1\\d{3})(\\d{5,6})", "$1 $2", ["1"]],
          ["(7\\d{3})(\\d{6})", "$1 $2", ["7(?:[1-5789]|62)", "7(?:[1-5789]|624)"]],
          ["(800)(\\d{4})", "$1 $2", ["800", "8001", "80011", "800111", "8001111"]],
          ["(845)(46)(4\\d)", "$1 $2 $3", ["845", "8454", "84546", "845464"]],
          ["(8\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["8(?:4[2-5]|7[0-3])"]],
          ["(80\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["80"]],
          ["([58]00)(\\d{6})", "$1 $2", ["[58]00"]]
        ], "0", "0$1", null, null, null, null, ["2(?:0[01378]|3[0189]|4[017]|8[0-46-9]|9[012])\\d{7}|1(?:(?:1(?:3[0-48]|[46][0-4]|5[012789]|7[0-49]|8[01349])|21[0-7]|31[0-8]|[459]1\\d|61[0-46-9]))\\d{6}|1(?:2(?:0[024-9]|2[3-9]|3[3-79]|4[1-689]|[58][02-9]|6[0-4789]|7[013-9]|9\\d)|3(?:0\\d|[25][02-9]|3[02-579]|[468][0-46-9]|7[1235679]|9[24578])|4(?:0[03-9]|[28][02-5789]|[37]\\d|4[02-69]|5[0-8]|[69][0-79])|5(?:0[1235-9]|2[024-9]|3[015689]|4[02-9]|5[03-9]|6\\d|7[0-35-9]|8[0-468]|9[0-5789])|6(?:0[034689]|2[0-35689]|[38][013-9]|4[1-467]|5[0-69]|6[13-9]|7[0-8]|9[0124578])|7(?:0[0246-9]|2\\d|3[023678]|4[03-9]|5[0-46-9]|6[013-9]|7[0-35-9]|8[024-9]|9[02-9])|8(?:0[35-9]|2[1-5789]|3[02-578]|4[0-578]|5[124-9]|6[2-69]|7\\d|8[02-9]|9[02569])|9(?:0[02-589]|2[02-689]|3[1-5789]|4[2-9]|5[0-579]|6[234789]|7[0124578]|8\\d|9[2-57]))\\d{6}|1(?:2(?:0(?:46[1-4]|87[2-9])|545[1-79]|76(?:2\\d|3[1-8]|6[1-6])|9(?:7(?:2[0-4]|3[2-5])|8(?:2[2-8]|7[0-4789]|8[345])))|3(?:638[2-5]|647[23]|8(?:47[04-9]|64[015789]))|4(?:044[1-7]|20(?:2[23]|8\\d)|6(?:0(?:30|5[2-57]|6[1-8]|7[2-8])|140)|8(?:052|87[123]))|5(?:24(?:3[2-79]|6\\d)|276\\d|6(?:26[06-9]|686))|6(?:06(?:4\\d|7[4-79])|295[567]|35[34]\\d|47(?:24|61)|59(?:5[08]|6[67]|74)|955[0-4])|7(?:26(?:6[13-9]|7[0-7])|442\\d|50(?:2[0-3]|[3-68]2|76))|8(?:27[56]\\d|37(?:5[2-5]|8[239])|84(?:3[2-58]))|9(?:0(?:0(?:6[1-8]|85)|52\\d)|3583|4(?:66[1-8]|9(?:2[01]|81))|63(?:23|3[1-4])|9561))\\d{3}|176888[234678]\\d{2}|16977[23]\\d{3}", "7(?:[1-4]\\d\\d|5(?:0[0-8]|[13-9]\\d|2[0-35-9])|7(?:0[1-9]|[1-7]\\d|8[02-9]|9[0-689])|8(?:[014-9]\\d|[23][0-8])|9(?:[04-9]\\d|1[02-9]|2[0-35-9]|3[0-689]))\\d{6}", "80(?:0(?:1111|\\d{6,7})|8\\d{7})|500\\d{6}", "(?:87[123]|9(?:[01]\\d|8[2349]))\\d{7}", "70\\d{8}", null, "(?:3[0347]|55)\\d{8}", "76(?:0[012]|2[356]|4[0134]|5[49]|6[0-369]|77|81|9[39])\\d{6}", "56\\d{8}"]],
        GD: ["1", "[4589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "473"],
        GE: ["995", "[34578]\\d{8}", [
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[348]"], "0$1"],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["7"], "0$1"],
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["5"], "$1"]
        ], "0"],
        GF: ["594", "[56]\\d{8}", [
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ], "0", "0$1"],
        GG: ["44", "[135789]\\d{6,9}", [
          ["(\\d{2})(\\d{4})(\\d{4})", "$1 $2 $3", ["2|5[56]|7(?:0|6[013-9])", "2|5[56]|7(?:0|6(?:[013-9]|2[0-35-9]))"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["1(?:1|\\d1)|3|9[018]"]],
          ["(\\d{5})(\\d{4,5})", "$1 $2", ["1(?:38|5[23]|69|76|94)", "1(?:387|5(?:24|39)|697|768|946)", "1(?:3873|5(?:242|39[456])|697[347]|768[347]|9467)"]],
          ["(1\\d{3})(\\d{5,6})", "$1 $2", ["1"]],
          ["(7\\d{3})(\\d{6})", "$1 $2", ["7(?:[1-5789]|62)", "7(?:[1-5789]|624)"]],
          ["(800)(\\d{4})", "$1 $2", ["800", "8001", "80011", "800111", "8001111"]],
          ["(845)(46)(4\\d)", "$1 $2 $3", ["845", "8454", "84546", "845464"]],
          ["(8\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["8(?:4[2-5]|7[0-3])"]],
          ["(80\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["80"]],
          ["([58]00)(\\d{6})", "$1 $2", ["[58]00"]]
        ], "0", "0$1", null, null, null, null, ["1481\\d{6}", "7(?:781|839|911)\\d{6}", "80(?:0(?:1111|\\d{6,7})|8\\d{7})|500\\d{6}", "(?:87[123]|9(?:[01]\\d|8[0-3]))\\d{7}", "70\\d{8}", null, "(?:3[0347]|55)\\d{8}", "76(?:0[012]|2[356]|4[0134]|5[49]|6[0-369]|77|81|9[39])\\d{6}", "56\\d{8}"]],
        GH: ["233", "[235]\\d{8}|8\\d{7}", [
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["[235]"]],
          ["(\\d{3})(\\d{5})", "$1 $2", ["8"]]
        ], "0", "0$1"],
        GI: ["350", "[2568]\\d{7}", [
          ["(\\d{3})(\\d{5})", "$1 $2", ["2"]]
        ]],
        GL: ["299", "[1-689]\\d{5}", [
          ["(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3"]
        ]],
        GM: ["220", "[2-9]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2"]
        ]],
        GN: ["224", "[367]\\d{7,8}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["3"]],
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[67]"]]
        ]],
        GP: ["590", "[56]\\d{8}", [
          ["([56]90)(\\d{2})(\\d{4})", "$1 $2-$3"]
        ], "0", "0$1", null, null, null, null, ["590(?:0[13468]|1[012]|2[0-68]|3[28]|4[0-8]|5[579]|6[0189]|70|8[0-689]|9\\d)\\d{4}", "690(?:0[0-7]|[1-9]\\d)\\d{4}"]],
        GQ: ["240", "[23589]\\d{8}", [
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["[235]"]],
          ["(\\d{3})(\\d{6})", "$1 $2", ["[89]"]]
        ]],
        GR: ["30", "[26-9]\\d{9}", [
          ["([27]\\d)(\\d{4})(\\d{4})", "$1 $2 $3", ["21|7"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["2[2-9]1|[689]"]],
          ["(2\\d{3})(\\d{6})", "$1 $2", ["2[2-9][02-9]"]]
        ]],
        GT: ["502", "[2-7]\\d{7}|1[89]\\d{9}", [
          ["(\\d{4})(\\d{4})", "$1 $2", ["[2-7]"]],
          ["(\\d{4})(\\d{3})(\\d{4})", "$1 $2 $3", ["1"]]
        ]],
        GU: ["1", "[5689]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "671"],
        GW: ["245", "(?:4(?:0\\d{5}|4\\d{7})|9\\d{8})", [
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["44|9[567]"]],
          ["(\\d{3})(\\d{4})", "$1 $2", ["40"]]
        ]],
        GY: ["592", "[2-4679]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2"]
        ]],
        HK: ["852", "[235-7]\\d{7}|8\\d{7,8}|9\\d{4,10}", [
          ["(\\d{4})(\\d{4})", "$1 $2", ["[235-7]|[89](?:0[1-9]|[1-9])"]],
          ["(800)(\\d{3})(\\d{3})", "$1 $2 $3", ["800"]],
          ["(900)(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["900"]],
          ["(900)(\\d{2,5})", "$1 $2", ["900"]]
        ]],
        HN: ["504", "[237-9]\\d{7}", [
          ["(\\d{4})(\\d{4})", "$1-$2"]
        ]],
        HR: ["385", "[1-7]\\d{5,8}|[89]\\d{6,11}", [
          ["(1)(\\d{4})(\\d{3})", "$1 $2 $3", ["1"]],
          ["(6[09])(\\d{4})(\\d{3})", "$1 $2 $3", ["6[09]"]],
          ["([67]2)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[67]2"]],
          ["([2-5]\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[2-5]"]],
          ["(9\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["9"]],
          ["(9\\d)(\\d{4})(\\d{4})", "$1 $2 $3", ["9"]],
          ["(9\\d)(\\d{3,4})(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["9"]],
          ["(\\d{2})(\\d{2})(\\d{2,3})", "$1 $2 $3", ["6[0145]|7"]],
          ["(\\d{2})(\\d{3,4})(\\d{3})", "$1 $2 $3", ["6[0145]|7"]],
          ["(80[01])(\\d{2})(\\d{2,3})", "$1 $2 $3", ["8"]],
          ["(80[01])(\\d{3,4})(\\d{3})", "$1 $2 $3", ["8"]]
        ], "0", "0$1"],
        HT: ["509", "[2-489]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{4})", "$1 $2 $3"]
        ]],
        HU: ["36", "[1-9]\\d{7,8}", [
          ["(1)(\\d{3})(\\d{4})", "$1 $2 $3", ["1"]],
          ["(\\d{2})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[2-9]"]]
        ], "06", "($1)"],
        ID: ["62", "(?:[1-79]\\d{6,10}|8\\d{7,11})", [
          ["(\\d{2})(\\d{5,8})", "$1 $2", ["2[124]|[36]1"], "(0$1)"],
          ["(\\d{3})(\\d{5,8})", "$1 $2", ["[4579]|2[035-9]|[36][02-9]"], "(0$1)"],
          ["(8\\d{2})(\\d{3,4})(\\d{3,5})", "$1-$2-$3", ["8[1-35-9]"]],
          ["(1)(500)(\\d{3})", "$1 $2 $3", ["15"], "$1"],
          ["(177)(\\d{6,8})", "$1 $2", ["17"]],
          ["(800)(\\d{5,7})", "$1 $2", ["800"]],
          ["(804)(\\d{3})(\\d{4})", "$1 $2 $3", ["804"]],
          ["(80\\d)(\\d)(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["80[79]"]]
        ], "0", "0$1"],
        IE: ["353", "[124-9]\\d{6,9}", [
          ["(1)(\\d{3,4})(\\d{4})", "$1 $2 $3", ["1"]],
          ["(\\d{2})(\\d{5})", "$1 $2", ["2[24-9]|47|58|6[237-9]|9[35-9]"]],
          ["(\\d{3})(\\d{5})", "$1 $2", ["40[24]|50[45]"]],
          ["(48)(\\d{4})(\\d{4})", "$1 $2 $3", ["48"]],
          ["(818)(\\d{3})(\\d{3})", "$1 $2 $3", ["81"]],
          ["(\\d{2})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[24-69]|7[14]"]],
          ["([78]\\d)(\\d{3,4})(\\d{4})", "$1 $2 $3", ["76|8[35-9]"], "0$1"],
          ["(700)(\\d{3})(\\d{3})", "$1 $2 $3", ["70"], "0$1"],
          ["(\\d{4})(\\d{3})(\\d{3})", "$1 $2 $3", ["1(?:8[059]|5)", "1(?:8[059]0|5)"], "$1"]
        ], "0", "(0$1)"],
        IL: ["972", "[17]\\d{6,9}|[2-589]\\d{3}(?:\\d{3,6})?|6\\d{3}", [
          ["([2-489])(\\d{3})(\\d{4})", "$1-$2-$3", ["[2-489]"], "0$1"],
          ["([57]\\d)(\\d{3})(\\d{4})", "$1-$2-$3", ["[57]"], "0$1"],
          ["(1)([7-9]\\d{2})(\\d{3})(\\d{3})", "$1-$2-$3-$4", ["1[7-9]"]],
          ["(1255)(\\d{3})", "$1-$2", ["125"]],
          ["(1200)(\\d{3})(\\d{3})", "$1-$2-$3", ["120"]],
          ["(1212)(\\d{2})(\\d{2})", "$1-$2-$3", ["121"]],
          ["(1599)(\\d{6})", "$1-$2", ["15"]],
          ["(\\d{4})", "*$1", ["[2-689]"]]
        ], "0", "$1"],
        IM: ["44", "[135789]\\d{6,9}", [
          ["(\\d{2})(\\d{4})(\\d{4})", "$1 $2 $3", ["2|5[56]|7(?:0|6[013-9])", "2|5[56]|7(?:0|6(?:[013-9]|2[0-35-9]))"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["1(?:1|\\d1)|3|9[018]"]],
          ["(\\d{5})(\\d{4,5})", "$1 $2", ["1(?:38|5[23]|69|76|94)", "1(?:387|5(?:24|39)|697|768|946)", "1(?:3873|5(?:242|39[456])|697[347]|768[347]|9467)"]],
          ["(1\\d{3})(\\d{5,6})", "$1 $2", ["1"]],
          ["(7\\d{3})(\\d{6})", "$1 $2", ["7(?:[1-5789]|62)", "7(?:[1-5789]|624)"]],
          ["(800)(\\d{4})", "$1 $2", ["800", "8001", "80011", "800111", "8001111"]],
          ["(845)(46)(4\\d)", "$1 $2 $3", ["845", "8454", "84546", "845464"]],
          ["(8\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["8(?:4[2-5]|7[0-3])"]],
          ["(80\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["80"]],
          ["([58]00)(\\d{6})", "$1 $2", ["[58]00"]]
        ], "0", "0$1", null, null, null, null, ["1624\\d{6}", "7[569]24\\d{6}", "808162\\d{4}", "(?:872299|90[0167]624)\\d{4}", "70\\d{8}", null, "3(?:08162\\d|3\\d{5}|4(?:40[49]06|5624\\d)|7(?:0624\\d|2299\\d))\\d{3}|55\\d{8}", null, "56\\d{8}"]],
        IN: ["91", "008\\d{9}|1\\d{7,12}|[2-9]\\d{9,10}", [
          ["(\\d{5})(\\d{5})", "$1 $2", ["7(?:[02-578]|19|6[0-35-9]|9[07-9])|8(?:0[015-9]|2[02356-9]|3[0-57-9]|[1459]|6[02-9]|7[01-69]|8[0-24-9])|9", "7(?:[078]|19[0-5]|2(?:[02356-9]|[14][017-9]|9[389])|3(?:[025-9]|1[07-9]|[34][017-9])|4(?:[0-35689]|[47][017-9])|5(?:[02346-9]|1[019]|5[017-9])|6(?:[06-9]|1[0-257-9]|2[0-5]|3[19]|5[4589])|9(?:0|7[2-9]|8[0246-9]|9[0-24-9]))|8(?:0(?:[01589]|6[67]|7[2-9])|1(?:[02-57-9]|1[0135-9]|6[089])|2(?:0[08]|[236-9]|5[1-9])|3(?:[0357-9]|17|28|4[1-9])|[45]|6(?:[02457-9]|6[07-9])|7(?:0[07]|[1-69])|8(?:[0-26-9]|44|5[2-9])|9(?:[035-9]|19|2[2-9]|4[0-8]))|9", "7(?:0|19[0-5]|2(?:[0235679]|[14][017-9]|8(?:[0-569]|78|8[089])|9[389])|3(?:[05-8]|1(?:[089]|7[5-9])|2(?:[5-8]|[0-49][089])|3[017-9]|4(?:[07-9]|11)|9(?:[01689]|[2345][089]|40|7[0189]))|4(?:[056]|1(?:[0135-9]|[23][089]|2[089]|4[089])|2(?:0[089]|[1-7][089]|[89])|3(?:[0-8][089]|9)|4(?:[089]|11|7[02-8])|7(?:[089]|11|7[02-8])|8(?:[0-24-7][089]|[389])|9(?:[0-7][089]|[89]))|5(?:[0346-9]|1[019]|2(?:[03-9]|[12][089])|5[017-9])|6(?:[06-9]|1[0-257-9]|2[0-5]|3[19]|5[4589])|7(?:0(?:[02-9]|10)|[1-9])|8(?:[0-79]|8(?:0[0189]|11|8[013-9]|9[012]))|9(?:0|7(?:[2-8]|9[7-9])|8[0246-9]|9(?:[04-9]|11|2[234])))|8(?:0(?:[01589]|6[67]|7(?:[2-7]|86|90))|1(?:[02-57-9]|1(?:[0135-9]|22|44)|6[089])|2(?:0[08]|[236-9]|5[1-9])|3(?:[0357-9]|170|28[0-6]|4[1-9])|[45]|6(?:[02457-9]|6(?:[08]|7[02-8]|9[01]))|7(?:0[07]|[1-69])|8(?:[0-26-9]|44|5[2-9])|9(?:[035-9]|19|2[2-9]|4[0-8]))|9"]],
          ["(\\d{2})(\\d{4})(\\d{4})", "$1 $2 $3", ["11|2[02]|33|4[04]|79[1-9]|80[2-46]"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["1(?:2[0-249]|3[0-25]|4[145]|[569][14]|7[1257]|8[1346]|[68][1-9])|2(?:1[257]|3[013]|4[01]|5[0137]|6[0158]|78|8[1568]|9[14])|3(?:26|4[1-3]|5[34]|6[01489]|7[02-46]|8[159])|4(?:1[36]|2[1-47]|3[15]|5[12]|6[0-26-9]|7[0-24-9]|8[013-57]|9[014-7])|5(?:1[025]|[36][25]|22|4[28]|5[12]|[78]1|9[15])|6(?:12|[2345]1|57|6[13]|7[14]|80)"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["7(?:12|2[14]|3[134]|4[47]|5[15]|[67]1|88)", "7(?:12|2[14]|3[134]|4[47]|5(?:1|5[2-6])|[67]1|88)"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["8(?:16|2[014]|3[126]|6[136]|7[078]|8[34]|91)"]],
          ["(\\d{4})(\\d{3})(\\d{3})", "$1 $2 $3", ["1(?:[23579]|[468][1-9])|[2-8]"]],
          ["(\\d{2})(\\d{3})(\\d{4})(\\d{3})", "$1 $2 $3 $4", ["008"]],
          ["(1600)(\\d{2})(\\d{4})", "$1 $2 $3", ["160", "1600"], "$1"],
          ["(1800)(\\d{4,5})", "$1 $2", ["180", "1800"], "$1"],
          ["(18[06]0)(\\d{2,4})(\\d{4})", "$1 $2 $3", ["18[06]", "18[06]0"], "$1"],
          ["(140)(\\d{3})(\\d{4})", "$1 $2 $3", ["140"], "$1"],
          ["(\\d{4})(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["18[06]", "18(?:0[03]|6[12])"], "$1"]
        ], "0", "0$1", null, null, !0],
        IO: ["246", "3\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2"]
        ]],
        IQ: ["964", "[1-7]\\d{7,9}", [
          ["(1)(\\d{3})(\\d{4})", "$1 $2 $3", ["1"]],
          ["([2-6]\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[2-6]"]],
          ["(7\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["7"]]
        ], "0", "0$1"],
        IR: ["98", "[1-8]\\d{9}|9(?:[0-4]\\d{8}|9\\d{2,8})", [
          ["(21)(\\d{3,5})", "$1 $2", ["21"]],
          ["(\\d{2})(\\d{4})(\\d{4})", "$1 $2 $3", ["[1-8]"]],
          ["(\\d{3})(\\d{3})", "$1 $2", ["9"]],
          ["(\\d{3})(\\d{2})(\\d{2,3})", "$1 $2 $3", ["9"]],
          ["(\\d{3})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["9"]]
        ], "0", "0$1"],
        IS: ["354", "[4-9]\\d{6}|38\\d{7}", [
          ["(\\d{3})(\\d{4})", "$1 $2", ["[4-9]"]],
          ["(3\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["3"]]
        ]],
        IT: ["39", "[01589]\\d{5,10}|3(?:[12457-9]\\d{8}|[36]\\d{7,9})", [
          ["(\\d{2})(\\d{3,4})(\\d{4})", "$1 $2 $3", ["0[26]|55"]],
          ["(0[26])(\\d{4})(\\d{5})", "$1 $2 $3", ["0[26]"]],
          ["(0[26])(\\d{4,6})", "$1 $2", ["0[26]"]],
          ["(0\\d{2})(\\d{3,4})(\\d{4})", "$1 $2 $3", ["0[13-57-9][0159]"]],
          ["(\\d{3})(\\d{3,6})", "$1 $2", ["0[13-57-9][0159]|8(?:03|4[17]|9[245])", "0[13-57-9][0159]|8(?:03|4[17]|9(?:2|[45][0-4]))"]],
          ["(0\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["0[13-57-9][2-46-8]"]],
          ["(0\\d{3})(\\d{2,6})", "$1 $2", ["0[13-57-9][2-46-8]"]],
          ["(\\d{3})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[13]|8(?:00|4[08]|9[59])", "[13]|8(?:00|4[08]|9(?:5[5-9]|9))"]],
          ["(\\d{4})(\\d{4})", "$1 $2", ["894", "894[5-9]"]],
          ["(\\d{3})(\\d{4})(\\d{4})", "$1 $2 $3", ["3"]]
        ], null, null, null, null, null, null, ["0(?:[26]\\d{4,9}|(?:1(?:[0159]\\d|[27][1-5]|31|4[1-4]|6[1356]|8[2-57])|3(?:[0159]\\d|2[1-4]|3[12]|[48][1-6]|6[2-59]|7[1-7])|4(?:[0159]\\d|[23][1-9]|4[245]|6[1-5]|7[1-4]|81)|5(?:[0159]\\d|2[1-5]|3[2-6]|4[1-79]|6[4-6]|7[1-578]|8[3-8])|7(?:[0159]\\d|2[12]|3[1-7]|4[2346]|6[13569]|7[13-6]|8[1-59])|8(?:[0159]\\d|2[34578]|3[1-356]|[6-8][1-5])|9(?:[0159]\\d|[238][1-5]|4[12]|6[1-8]|7[1-6]))\\d{2,7})", "3(?:[12457-9]\\d{8}|6\\d{7,8}|3\\d{7,9})", "80(?:0\\d{6}|3\\d{3})", "0878\\d{5}|1(?:44|6[346])\\d{6}|89(?:2\\d{3}|4(?:[0-4]\\d{2}|[5-9]\\d{4})|5(?:[0-4]\\d{2}|[5-9]\\d{6})|9\\d{6})", "1(?:78\\d|99)\\d{6}", null, null, null, "55\\d{8}"]],
        JE: ["44", "[135789]\\d{6,9}", [
          ["(\\d{2})(\\d{4})(\\d{4})", "$1 $2 $3", ["2|5[56]|7(?:0|6[013-9])", "2|5[56]|7(?:0|6(?:[013-9]|2[0-35-9]))"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["1(?:1|\\d1)|3|9[018]"]],
          ["(\\d{5})(\\d{4,5})", "$1 $2", ["1(?:38|5[23]|69|76|94)", "1(?:387|5(?:24|39)|697|768|946)", "1(?:3873|5(?:242|39[456])|697[347]|768[347]|9467)"]],
          ["(1\\d{3})(\\d{5,6})", "$1 $2", ["1"]],
          ["(7\\d{3})(\\d{6})", "$1 $2", ["7(?:[1-5789]|62)", "7(?:[1-5789]|624)"]],
          ["(800)(\\d{4})", "$1 $2", ["800", "8001", "80011", "800111", "8001111"]],
          ["(845)(46)(4\\d)", "$1 $2 $3", ["845", "8454", "84546", "845464"]],
          ["(8\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["8(?:4[2-5]|7[0-3])"]],
          ["(80\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["80"]],
          ["([58]00)(\\d{6})", "$1 $2", ["[58]00"]]
        ], "0", "0$1", null, null, null, null, ["1534\\d{6}", "7(?:509|7(?:00|97)|829|937)\\d{6}", "80(?:07(?:35|81)|8901)\\d{4}", "(?:871206|90(?:066[59]|1810|71(?:07|55)))\\d{4}", "701511\\d{4}", null, "3(?:0(?:07(?:35|81)|8901)|3\\d{4}|4(?:4(?:4(?:05|42|69)|703)|5(?:041|800))|7(?:0002|1206))\\d{4}|55\\d{8}", "76(?:0[012]|2[356]|4[0134]|5[49]|6[0-369]|77|81|9[39])\\d{6}", "56\\d{8}"]],
        JM: ["1", "[589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "876"],
        JO: ["962", "[235-9]\\d{7,8}", [
          ["(\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["[2356]|87"], "(0$1)"],
          ["(7)(\\d{4})(\\d{4})", "$1 $2 $3", ["7[457-9]"]],
          ["(\\d{3})(\\d{5,6})", "$1 $2", ["70|8[0158]|9"]]
        ], "0", "0$1"],
        JP: ["81", "[1-9]\\d{8,9}|00(?:[36]\\d{7,14}|7\\d{5,7}|8\\d{7})", [
          ["(\\d{3})(\\d{3})(\\d{3})", "$1-$2-$3", ["(?:12|57|99)0"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1-$2-$3", ["800"]],
          ["(\\d{2})(\\d{4})(\\d{4})", "$1-$2-$3", ["[2579]0|80[1-9]"]],
          ["(\\d{4})(\\d)(\\d{4})", "$1-$2-$3", ["1(?:26|3[79]|4[56]|5[4-68]|6[3-5])|5(?:76|97)|499|746|8(?:3[89]|63|47|51)|9(?:49|80|9[16])", "1(?:267|3(?:7[247]|9[278])|4(?:5[67]|66)|5(?:47|58|64|8[67])|6(?:3[245]|48|5[4-68]))|5(?:76|97)9|499[2468]|7468|8(?:3(?:8[78]|96)|636|477|51[24])|9(?:496|802|9(?:1[23]|69))", "1(?:267|3(?:7[247]|9[278])|4(?:5[67]|66)|5(?:47|58|64|8[67])|6(?:3[245]|48|5[4-68]))|5(?:769|979[2-69])|499[2468]|7468|8(?:3(?:8[78]|96[2457-9])|636[2-57-9]|477|51[24])|9(?:496|802|9(?:1[23]|69))"]],
          ["(\\d{3})(\\d{2})(\\d{4})", "$1-$2-$3", ["1(?:2[3-6]|3[3-9]|4[2-6]|5[2-8]|[68][2-7]|7[2-689]|9[1-578])|2(?:2[03-689]|3[3-58]|4[0-468]|5[04-8]|6[013-8]|7[06-9]|8[02-57-9]|9[13])|4(?:2[28]|3[689]|6[035-7]|7[05689]|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9[4-9])|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9[014-9])|8(?:2[49]|3[3-8]|4[5-8]|5[2-9]|6[35-9]|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9[3-7])", "1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9[2-8])|3(?:7[2-6]|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5[4-7]|6[2-9]|8[2-8]|9[236-9])|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3[34]|[4-7]))", "1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9(?:[3578]|20|4[04-9]|6[56]))|3(?:7(?:[2-5]|6[0-59])|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5(?:[467]|5[014-9])|6(?:[2-8]|9[02-69])|8[2-8]|9(?:[236-8]|9[23]))|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3(?:3[02-9]|4[0-24689])|4[2-69]|[5-7]))", "1(?:2[3-6]|3[3-9]|4[2-6]|5(?:[236-8]|[45][2-69])|[68][2-7]|7[2-689]|9[1-578])|2(?:2(?:[04-689]|3[23])|3[3-58]|4[0-468]|5(?:5[78]|7[2-4]|[0468][2-9])|6(?:[0135-8]|4[2-5])|7(?:[0679]|8[2-7])|8(?:[024578]|3[25-9]|9[6-9])|9(?:11|3[2-4]))|4(?:2(?:2[2-9]|8[237-9])|3[689]|6[035-7]|7(?:[059][2-8]|[68])|80|9[3-5])|5(?:3[1-36-9]|4[4578]|5[013-8]|6[1-9]|7[2-8]|8[14-7]|9(?:[89][2-8]|[4-7]))|7(?:2[15]|3[5-9]|4[02-9]|6[135-8]|7[0-4689]|9(?:[017-9]|4[6-8]|5[2-478]|6[2-589]))|8(?:2(?:4[4-8]|9(?:[3578]|20|4[04-9]|6(?:5[25]|60)))|3(?:7(?:[2-5]|6[0-59])|[3-6][2-9]|8[2-5])|4[5-8]|5[2-9]|6(?:[37]|5(?:[467]|5[014-9])|6(?:[2-8]|9[02-69])|8[2-8]|9(?:[236-8]|9[23]))|7[579]|8[03-579]|9[2-8])|9(?:[23]0|4[02-46-9]|5[024-79]|6[4-9]|7[2-47-9]|8[02-7]|9(?:3(?:3[02-9]|4[0-24689])|4[2-69]|[5-7]))"]],
          ["(\\d{2})(\\d{3})(\\d{4})", "$1-$2-$3", ["1|2(?:2[37]|5[5-9]|64|78|8[39]|91)|4(?:2[2689]|64|7[347])|5(?:[2-589]|39)|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93)", "1|2(?:2[37]|5(?:[57]|[68]0|9[19])|64|78|8[39]|917)|4(?:2(?:[68]|20|9[178])|64|7[347])|5(?:[2-589]|39[67])|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93[34])", "1|2(?:2[37]|5(?:[57]|[68]0|9(?:17|99))|64|78|8[39]|917)|4(?:2(?:[68]|20|9[178])|64|7[347])|5(?:[2-589]|39[67])|60|8(?:[46-9]|3[279]|2[124589])|9(?:[235-8]|93(?:31|4))"]],
          ["(\\d{3})(\\d{2})(\\d{4})", "$1-$2-$3", ["2(?:9[14-79]|74|[34]7|[56]9)|82|993"]],
          ["(\\d)(\\d{4})(\\d{4})", "$1-$2-$3", ["3|4(?:2[09]|7[01])|6[1-9]"]],
          ["(\\d{2})(\\d{3})(\\d{4})", "$1-$2-$3", ["[2479][1-9]"]]
        ], "0", "0$1"],
        KE: ["254", "20\\d{6,7}|[4-9]\\d{6,9}", [
          ["(\\d{2})(\\d{5,7})", "$1 $2", ["[24-6]"]],
          ["(\\d{3})(\\d{6})", "$1 $2", ["7"]],
          ["(\\d{3})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[89]"]]
        ], "0", "0$1", "005|0"],
        KG: ["996", "[235-8]\\d{8,9}", [
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["[25-7]|31[25]"]],
          ["(\\d{4})(\\d{5})", "$1 $2", ["3(?:1[36]|[2-9])"]],
          ["(\\d{3})(\\d{3})(\\d)(\\d{3})", "$1 $2 $3 $4", ["8"]]
        ], "0", "0$1"],
        KH: ["855", "[1-9]\\d{7,9}", [
          ["(\\d{2})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["1\\d[1-9]|[2-9]"], "0$1"],
          ["(1[89]00)(\\d{3})(\\d{3})", "$1 $2 $3", ["1[89]0"]]
        ], "0"],
        KI: ["686", "[2458]\\d{4}|3\\d{4,7}|7\\d{7}", [], null, null, "0"],
        KM: ["269", "[379]\\d{6}", [
          ["(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3"]
        ]],
        KN: ["1", "[589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "869"],
        KP: ["850", "1\\d{9}|[28]\\d{7}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["1"]],
          ["(\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["2"]],
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["8"]]
        ], "0", "0$1"],
        KR: ["82", "007\\d{9,11}|[1-7]\\d{3,9}|8\\d{8}", [
          ["(\\d{2})(\\d{4})(\\d{4})", "$1-$2-$3", ["1(?:0|1[19]|[69]9|5[458])|[57]0", "1(?:0|1[19]|[69]9|5(?:44|59|8))|[57]0"]],
          ["(\\d{2})(\\d{3,4})(\\d{4})", "$1-$2-$3", ["1(?:[01]|5[1-4]|6[2-8]|[7-9])|[68]0|[3-6][1-9][1-9]", "1(?:[01]|5(?:[1-3]|4[56])|6[2-8]|[7-9])|[68]0|[3-6][1-9][1-9]"]],
          ["(\\d{3})(\\d)(\\d{4})", "$1-$2-$3", ["131", "1312"]],
          ["(\\d{3})(\\d{2})(\\d{4})", "$1-$2-$3", ["131", "131[13-9]"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1-$2-$3", ["13[2-9]"]],
          ["(\\d{2})(\\d{2})(\\d{3})(\\d{4})", "$1-$2-$3-$4", ["30"]],
          ["(\\d)(\\d{3,4})(\\d{4})", "$1-$2-$3", ["2[1-9]"]],
          ["(\\d)(\\d{3,4})", "$1-$2", ["21[0-46-9]"]],
          ["(\\d{2})(\\d{3,4})", "$1-$2", ["[3-6][1-9]1", "[3-6][1-9]1(?:[0-46-9])"]],
          ["(\\d{4})(\\d{4})", "$1-$2", ["1(?:5[246-9]|6[04678]|8[03579])", "1(?:5(?:22|44|66|77|88|99)|6(?:00|44|6[16]|70|88)|8(?:00|33|55|77|99))"], "$1"]
        ], "0", "0$1", "0(8[1-46-8]|85\\d{2})?"],
        KW: ["965", "[12569]\\d{6,7}", [
          ["(\\d{4})(\\d{3,4})", "$1 $2", ["[16]|2(?:[0-35-9]|4[0-35-9])|9[024-9]|52[25]"]],
          ["(\\d{3})(\\d{5})", "$1 $2", ["244|5(?:[015]|66)"]]
        ]],
        KY: ["1", "[3589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "345"],
        KZ: ["7", "(?:33\\d|7\\d{2}|80[09])\\d{7}", [
          ["([3489]\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1 $2-$3-$4", ["[34689]"]],
          ["(7\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["7"]]
        ], "8", null, null, null, null, null, ["33622\\d{5}|7(?:1(?:0(?:[23]\\d|4[023]|59|63)|1(?:[23]\\d|4[0-79]|59)|2(?:[23]\\d|59)|3(?:2\\d|3[1-79]|4[0-35-9]|59)|4(?:2\\d|3[013-79]|4[0-8]|5[1-79])|5(?:2\\d|3[1-8]|4[1-7]|59)|6(?:[234]\\d|5[19]|61)|72\\d|8(?:[27]\\d|3[1-46-9]|4[0-5]))|2(?:1(?:[23]\\d|4[46-9]|5[3469])|2(?:2\\d|3[0679]|46|5[12679])|3(?:[234]\\d|5[139])|4(?:2\\d|3[1235-9]|59)|5(?:[23]\\d|4[01246-8]|59|61)|6(?:2\\d|3[1-9]|4[0-4]|59)|7(?:[2379]\\d|40|5[279])|8(?:[23]\\d|4[0-3]|59)|9(?:2\\d|3[124578]|59)))\\d{5}", "7(?:0[012578]|47|6[02-4]|7[15-8]|85)\\d{7}", "800\\d{7}", "809\\d{7}", null, null, null, null, "751\\d{7}"]],
        LA: ["856", "[2-8]\\d{7,9}", [
          ["(20)(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["20"]],
          ["([2-8]\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["2[13]|3[14]|[4-8]"]],
          ["(30)(\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3 $4", ["30"]]
        ], "0", "0$1"],
        LB: ["961", "[13-9]\\d{6,7}", [
          ["(\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["[13-6]|7(?:[2-57]|62|8[0-7]|9[04-9])|8[02-9]|9"], "0$1"],
          ["([7-9]\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["[89][01]|7(?:[01]|6[013-9]|8[89]|9[1-3])"]]
        ], "0"],
        LC: ["1", "[5789]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "758"],
        LI: ["423", "6\\d{8}|[23789]\\d{6}", [
          ["(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3", ["[23789]"]],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["6[56]"]],
          ["(69)(7\\d{2})(\\d{4})", "$1 $2 $3", ["697"]]
        ], "0", null, "0|10(?:01|20|66)"],
        LK: ["94", "[1-9]\\d{8}", [
          ["(\\d{2})(\\d{1})(\\d{6})", "$1 $2 $3", ["[1-689]"]],
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["7"]]
        ], "0", "0$1"],
        LR: ["231", "2\\d{7,8}|[378]\\d{8}|4\\d{6}|5\\d{6,8}", [
          ["(2\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["2"]],
          ["([4-5])(\\d{3})(\\d{3})", "$1 $2 $3", ["[45]"]],
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["[23578]"]]
        ], "0", "0$1"],
        LS: ["266", "[2568]\\d{7}", [
          ["(\\d{4})(\\d{4})", "$1 $2"]
        ]],
        LT: ["370", "[3-9]\\d{7}", [
          ["([34]\\d)(\\d{6})", "$1 $2", ["37|4(?:1|5[45]|6[2-4])"]],
          ["([3-6]\\d{2})(\\d{5})", "$1 $2", ["3[148]|4(?:[24]|6[09])|528|6"]],
          ["([7-9]\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3", ["[7-9]"], "8 $1"],
          ["(5)(2\\d{2})(\\d{4})", "$1 $2 $3", ["52[0-79]"]]
        ], "8", "(8-$1)", "[08]", null, !0],
        LU: ["352", "[24-9]\\d{3,10}|3(?:[0-46-9]\\d{2,9}|5[013-9]\\d{1,8})", [
          ["(\\d{2})(\\d{3})", "$1 $2", ["[2-5]|7[1-9]|[89](?:[1-9]|0[2-9])"]],
          ["(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3", ["[2-5]|7[1-9]|[89](?:[1-9]|0[2-9])"]],
          ["(\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3", ["20"]],
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{1,2})", "$1 $2 $3 $4", ["2(?:[0367]|4[3-8])"]],
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3 $4", ["20"]],
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})(\\d{1,2})", "$1 $2 $3 $4 $5", ["2(?:[0367]|4[3-8])"]],
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{1,4})", "$1 $2 $3 $4", ["2(?:[12589]|4[12])|[3-5]|7[1-9]|8(?:[1-9]|0[2-9])|9(?:[1-9]|0[2-46-9])"]],
          ["(\\d{3})(\\d{2})(\\d{3})", "$1 $2 $3", ["70|80[01]|90[015]"]],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["6"]]
        ], null, null, "(15(?:0[06]|1[12]|35|4[04]|55|6[26]|77|88|99)\\d)"],
        LV: ["371", "[2689]\\d{7}", [
          ["([2689]\\d)(\\d{3})(\\d{3})", "$1 $2 $3"]
        ]],
        LY: ["218", "[25679]\\d{8}", [
          ["([25679]\\d)(\\d{7})", "$1-$2"]
        ], "0", "0$1"],
        MA: ["212", "[5-9]\\d{8}", [
          ["([5-7]\\d{2})(\\d{6})", "$1-$2", ["5(?:2[015-7]|3[0-4])|[67]"]],
          ["([58]\\d{3})(\\d{5})", "$1-$2", ["5(?:2[2-489]|3[5-9]|92)|892", "5(?:2(?:[2-48]|90)|3(?:[5-79]|80)|924)|892"]],
          ["(5\\d{4})(\\d{4})", "$1-$2", ["5(?:29|38)", "5(?:29|38)[89]"]],
          ["([5]\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["5(?:4[067]|5[03])"]],
          ["(8[09])(\\d{7})", "$1-$2", ["8(?:0|9[013-9])"]]
        ], "0", "0$1", null, null, null, null, ["5(?:2(?:(?:[015-7]\\d|2[02-9]|3[2-57]|4[2-8]|8[235-7])\\d|9(?:0\\d|[89]0))|3(?:(?:[0-4]\\d|[57][2-9]|6[2-8]|9[3-9])\\d|8(?:0\\d|[89]0))|(?:4[067]|5[03])\\d{2})\\d{4}", "(?:6(?:[0-79]\\d|8[0-247-9])|7(?:[07][07]|6[12]))\\d{6}", "80\\d{7}", "89\\d{7}", null, null, null, null, "5924[01]\\d{4}"]],
        MC: ["377", "[34689]\\d{7,8}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[39]"], "$1"],
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["4"]],
          ["(6)(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4 $5", ["6"]],
          ["(\\d{3})(\\d{3})(\\d{2})", "$1 $2 $3", ["8"], "$1"]
        ], "0", "0$1"],
        MD: ["373", "[235-9]\\d{7}", [
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["22|3"]],
          ["([25-7]\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3", ["2[13-9]|[5-7]"]],
          ["([89]\\d{2})(\\d{5})", "$1 $2", ["[89]"]]
        ], "0", "0$1"],
        ME: ["382", "[2-9]\\d{7,8}", [
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["[2-57-9]|6[036-9]", "[2-57-9]|6(?:[03689]|7(?:[0-8]|9[3-9]))"]],
          ["(67)(9)(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["679", "679[0-2]"]]
        ], "0", "0$1"],
        MF: ["590", "[56]\\d{8}", [
          ["([56]90)(\\d{2})(\\d{4})", "$1 $2-$3"]
        ], "0", null, null, null, null, null, ["590(?:[02][79]|13|5[0-268]|[78]7)\\d{4}", "690(?:0[0-7]|[1-9]\\d)\\d{4}"]],
        MG: ["261", "[23]\\d{8}", [
          ["([23]\\d)(\\d{2})(\\d{3})(\\d{2})", "$1 $2 $3 $4"]
        ], "0", "0$1"],
        MH: ["692", "[2-6]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1-$2"]
        ], "1"],
        MK: ["389", "[2-578]\\d{7}", [
          ["(2)(\\d{3})(\\d{4})", "$1 $2 $3", ["2"]],
          ["([347]\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["[347]"]],
          ["([58]\\d{2})(\\d)(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[58]"]]
        ], "0", "0$1"],
        ML: ["223", "[246-9]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[246-9]"]]
        ]],
        MM: ["95", "[1478]\\d{5,7}|[256]\\d{5,8}|9(?:[279]\\d{0,2}|[58]|[34]\\d{1,2}|6\\d?)\\d{6}", [
          ["(\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["1|2[245]"]],
          ["(2)(\\d{4})(\\d{4})", "$1 $2 $3", ["251"]],
          ["(\\d)(\\d{2})(\\d{3})", "$1 $2 $3", ["16|2"]],
          ["(\\d{2})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["67|81"]],
          ["(\\d{2})(\\d{2})(\\d{3,4})", "$1 $2 $3", ["[4-8]"]],
          ["(9)(\\d{3})(\\d{4,6})", "$1 $2 $3", ["9(?:2[0-4]|[35-9]|4[137-9])"]],
          ["(9)([34]\\d{4})(\\d{4})", "$1 $2 $3", ["9(?:3[0-36]|4[0-57-9])"]],
          ["(9)(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["92[56]"]],
          ["(9)(\\d{3})(\\d{3})(\\d{2})", "$1 $2 $3 $4", ["93"]]
        ], "0", "0$1"],
        MN: ["976", "[12]\\d{7,9}|[57-9]\\d{7}", [
          ["([12]\\d)(\\d{2})(\\d{4})", "$1 $2 $3", ["[12]1"]],
          ["([12]2\\d)(\\d{5,6})", "$1 $2", ["[12]2[1-3]"]],
          ["([12]\\d{3})(\\d{5})", "$1 $2", ["[12](?:27|[3-5])", "[12](?:27|[3-5]\\d)2"]],
          ["(\\d{4})(\\d{4})", "$1 $2", ["[57-9]"], "$1"],
          ["([12]\\d{4})(\\d{4,5})", "$1 $2", ["[12](?:27|[3-5])", "[12](?:27|[3-5]\\d)[4-9]"]]
        ], "0", "0$1"],
        MO: ["853", "[268]\\d{7}", [
          ["([268]\\d{3})(\\d{4})", "$1 $2"]
        ]],
        MP: ["1", "[5689]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "670"],
        MQ: ["596", "[56]\\d{8}", [
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ], "0", "0$1"],
        MR: ["222", "[2-48]\\d{7}", [
          ["([2-48]\\d)(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        MS: ["1", "[5689]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "664"],
        MT: ["356", "[2357-9]\\d{7}", [
          ["(\\d{4})(\\d{4})", "$1 $2"]
        ]],
        MU: ["230", "[2-9]\\d{6,7}", [
          ["([2-46-9]\\d{2})(\\d{4})", "$1 $2", ["[2-46-9]"]],
          ["(5\\d{3})(\\d{4})", "$1 $2", ["5"]]
        ]],
        MV: ["960", "[346-8]\\d{6,9}|9(?:00\\d{7}|\\d{6})", [
          ["(\\d{3})(\\d{4})", "$1-$2", ["[3467]|9(?:[1-9]|0[1-9])"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["[89]00"]]
        ]],
        MW: ["265", "(?:1(?:\\d{2})?|[2789]\\d{2})\\d{6}", [
          ["(\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["1"]],
          ["(2\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["2"]],
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[1789]"]]
        ], "0", "0$1"],
        MX: ["52", "[1-9]\\d{9,10}", [
          ["([358]\\d)(\\d{4})(\\d{4})", "$1 $2 $3", ["33|55|81"]],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["[2467]|3[0-2457-9]|5[089]|8[02-9]|9[0-35-9]"]],
          ["(1)([358]\\d)(\\d{4})(\\d{4})", "044 $2 $3 $4", ["1(?:33|55|81)"], "$1", null, "$1 $2 $3 $4"],
          ["(1)(\\d{3})(\\d{3})(\\d{4})", "044 $2 $3 $4", ["1(?:[2467]|3[0-2457-9]|5[089]|8[2-9]|9[1-35-9])"], "$1", null, "$1 $2 $3 $4"]
        ], "01", "01 $1", "0[12]|04[45](\\d{10})", "1$1", !0],
        MY: ["60", "[13-9]\\d{7,9}", [
          ["([4-79])(\\d{3})(\\d{4})", "$1-$2 $3", ["[4-79]"], "0$1"],
          ["(3)(\\d{4})(\\d{4})", "$1-$2 $3", ["3"], "0$1"],
          ["([18]\\d)(\\d{3})(\\d{3,4})", "$1-$2 $3", ["1[02-46-9][1-9]|8"], "0$1"],
          ["(1)([36-8]00)(\\d{2})(\\d{4})", "$1-$2-$3-$4", ["1[36-8]0"]],
          ["(11)(\\d{4})(\\d{4})", "$1-$2 $3", ["11"], "0$1"],
          ["(15[49])(\\d{3})(\\d{4})", "$1-$2 $3", ["15"], "0$1"]
        ], "0"],
        MZ: ["258", "[28]\\d{7,8}", [
          ["([28]\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["2|8[2-7]"]],
          ["(80\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["80"]]
        ]],
        NA: ["264", "[68]\\d{7,8}", [
          ["(8\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["8[1235]"]],
          ["(6\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["6"]],
          ["(88)(\\d{3})(\\d{3})", "$1 $2 $3", ["88"]],
          ["(870)(\\d{3})(\\d{3})", "$1 $2 $3", ["870"]]
        ], "0", "0$1"],
        NC: ["687", "[2-57-9]\\d{5}", [
          ["(\\d{2})(\\d{2})(\\d{2})", "$1.$2.$3", ["[2-46-9]|5[0-4]"]]
        ]],
        NE: ["227", "[0289]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[289]|09"]],
          ["(08)(\\d{3})(\\d{3})", "$1 $2 $3", ["08"]]
        ]],
        NF: ["672", "[13]\\d{5}", [
          ["(\\d{2})(\\d{4})", "$1 $2", ["1"]],
          ["(\\d)(\\d{5})", "$1 $2", ["3"]]
        ]],
        NG: ["234", "[1-6]\\d{5,8}|9\\d{5,9}|[78]\\d{5,13}", [
          ["(\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[12]|9(?:0[3-9]|[1-9])"]],
          ["(\\d{2})(\\d{3})(\\d{2,3})", "$1 $2 $3", ["[3-6]|7(?:[1-79]|0[1-9])|8[2-9]"]],
          ["(\\d{3})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["70|8[01]|90[2357-9]"]],
          ["([78]00)(\\d{4})(\\d{4,5})", "$1 $2 $3", ["[78]00"]],
          ["([78]00)(\\d{5})(\\d{5,6})", "$1 $2 $3", ["[78]00"]],
          ["(78)(\\d{2})(\\d{3})", "$1 $2 $3", ["78"]]
        ], "0", "0$1"],
        NI: ["505", "[12578]\\d{7}", [
          ["(\\d{4})(\\d{4})", "$1 $2"]
        ]],
        NL: ["31", "1\\d{4,8}|[2-7]\\d{8}|[89]\\d{6,9}", [
          ["([1-578]\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["1[035]|2[0346]|3[03568]|4[0356]|5[0358]|7|8[4578]"]],
          ["([1-5]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["1[16-8]|2[259]|3[124]|4[17-9]|5[124679]"]],
          ["(6)(\\d{8})", "$1 $2", ["6[0-57-9]"]],
          ["(66)(\\d{7})", "$1 $2", ["66"]],
          ["(14)(\\d{3,4})", "$1 $2", ["14"], "$1"],
          ["([89]0\\d)(\\d{4,7})", "$1 $2", ["80|9"]]
        ], "0", "0$1"],
        NO: ["47", "0\\d{4}|[2-9]\\d{7}", [
          ["([489]\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3", ["[489]"]],
          ["([235-7]\\d)(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[235-7]"]]
        ], null, null, null, null, null, null, ["(?:2[1-4]|3[1-3578]|5[1-35-7]|6[1-4679]|7[0-8])\\d{6}", "(?:4[015-8]|5[89]|87|9\\d)\\d{6}", "80[01]\\d{5}", "82[09]\\d{5}", "880\\d{5}", "81[23]\\d{5}", "0\\d{4}|81(?:0(?:0[7-9]|1\\d)|5\\d{2})\\d{3}", null, "85[0-5]\\d{5}"]],
        NP: ["977", "[1-8]\\d{7}|9(?:[1-69]\\d{6,8}|7[2-6]\\d{5,7}|8\\d{8})", [
          ["(1)(\\d{7})", "$1-$2", ["1[2-6]"]],
          ["(\\d{2})(\\d{6})", "$1-$2", ["1[01]|[2-8]|9(?:[1-69]|7[15-9])"]],
          ["(9\\d{2})(\\d{7})", "$1-$2", ["9(?:6[013]|7[245]|8)"], "$1"]
        ], "0", "0$1"],
        NR: ["674", "[458]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2"]
        ]],
        NU: ["683", "[1-5]\\d{3}"],
        NZ: ["64", "6[235-9]\\d{6}|[2-57-9]\\d{7,10}", [
          ["([34679])(\\d{3})(\\d{4})", "$1-$2 $3", ["[346]|7[2-57-9]|9[1-9]"]],
          ["(24099)(\\d{3})", "$1 $2", ["240", "2409", "24099"]],
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["21"]],
          ["(\\d{2})(\\d{3})(\\d{3,5})", "$1 $2 $3", ["2(?:1[1-9]|[69]|7[0-35-9])|70|86"]],
          ["(2\\d)(\\d{3,4})(\\d{4})", "$1 $2 $3", ["2[028]"]],
          ["(\\d{3})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["2(?:10|74)|5|[89]0"]]
        ], "0", "0$1"],
        OM: ["968", "(?:5|[279]\\d)\\d{6}|800\\d{5,6}", [
          ["(2\\d)(\\d{6})", "$1 $2", ["2"]],
          ["([79]\\d{3})(\\d{4})", "$1 $2", ["[79]"]],
          ["([58]00)(\\d{4,6})", "$1 $2", ["[58]"]]
        ]],
        PA: ["507", "[1-9]\\d{6,7}", [
          ["(\\d{3})(\\d{4})", "$1-$2", ["[1-57-9]"]],
          ["(\\d{4})(\\d{4})", "$1-$2", ["6"]]
        ]],
        PE: ["51", "[14-9]\\d{7,8}", [
          ["(1)(\\d{7})", "$1 $2", ["1"]],
          ["([4-8]\\d)(\\d{6})", "$1 $2", ["[4-7]|8[2-4]"]],
          ["(\\d{3})(\\d{5})", "$1 $2", ["80"]],
          ["(9\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["9"], "$1"]
        ], "0", "(0$1)"],
        PF: ["689", "4\\d{5,7}|8\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["4[09]|8[79]"]],
          ["(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3", ["44"]]
        ]],
        PG: ["675", "[1-9]\\d{6,7}", [
          ["(\\d{3})(\\d{4})", "$1 $2", ["[13-689]|27"]],
          ["(\\d{4})(\\d{4})", "$1 $2", ["20|7"]]
        ]],
        PH: ["63", "2\\d{5,7}|[3-9]\\d{7,9}|1800\\d{7,9}", [
          ["(2)(\\d{3})(\\d{4})", "$1 $2 $3", ["2"], "(0$1)"],
          ["(2)(\\d{5})", "$1 $2", ["2"], "(0$1)"],
          ["(\\d{4})(\\d{4,6})", "$1 $2", ["3(?:23|39|46)|4(?:2[3-6]|[35]9|4[26]|76)|5(?:22|44)|642|8(?:62|8[245])", "3(?:230|397|461)|4(?:2(?:35|[46]4|51)|396|4(?:22|63)|59[347]|76[15])|5(?:221|446)|642[23]|8(?:622|8(?:[24]2|5[13]))"], "(0$1)"],
          ["(\\d{5})(\\d{4})", "$1 $2", ["346|4(?:27|9[35])|883", "3469|4(?:279|9(?:30|56))|8834"], "(0$1)"],
          ["([3-8]\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["[3-8]"], "(0$1)"],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["81|9"], "0$1"],
          ["(1800)(\\d{3})(\\d{4})", "$1 $2 $3", ["1"]],
          ["(1800)(\\d{1,2})(\\d{3})(\\d{4})", "$1 $2 $3 $4", ["1"]]
        ], "0"],
        PK: ["92", "1\\d{8}|[2-8]\\d{5,11}|9(?:[013-9]\\d{4,9}|2\\d(?:111\\d{6}|\\d{3,7}))", [
          ["(\\d{2})(111)(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["(?:2[125]|4[0-246-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)1", "(?:2[125]|4[0-246-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)11", "(?:2[125]|4[0-246-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)111"]],
          ["(\\d{3})(111)(\\d{3})(\\d{3})", "$1 $2 $3 $4", ["2[349]|45|54|60|72|8[2-5]|9[2-9]", "(?:2[349]|45|54|60|72|8[2-5]|9[2-9])\\d1", "(?:2[349]|45|54|60|72|8[2-5]|9[2-9])\\d11", "(?:2[349]|45|54|60|72|8[2-5]|9[2-9])\\d111"]],
          ["(\\d{2})(\\d{7,8})", "$1 $2", ["(?:2[125]|4[0-246-9]|5[1-35-7]|6[1-8]|7[14]|8[16]|91)[2-9]"]],
          ["(\\d{3})(\\d{6,7})", "$1 $2", ["2[349]|45|54|60|72|8[2-5]|9[2-9]", "(?:2[349]|45|54|60|72|8[2-5]|9[2-9])\\d[2-9]"]],
          ["(3\\d{2})(\\d{7})", "$1 $2", ["3"], "0$1"],
          ["([15]\\d{3})(\\d{5,6})", "$1 $2", ["58[12]|1"]],
          ["(586\\d{2})(\\d{5})", "$1 $2", ["586"]],
          ["([89]00)(\\d{3})(\\d{2})", "$1 $2 $3", ["[89]00"], "0$1"]
        ], "0", "(0$1)"],
        PL: ["48", "[12]\\d{6,8}|[3-57-9]\\d{8}|6\\d{5,8}", [
          ["(\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[14]|2[0-57-9]|3[2-4]|5[24-689]|6[1-3578]|7[14-7]|8[1-79]|9[145]"]],
          ["(\\d{2})(\\d{1})(\\d{4})", "$1 $2 $3", ["[12]2"]],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["26|39|5[0137]|6[0469]|7[02389]|8[08]"]],
          ["(\\d{3})(\\d{2})(\\d{2,3})", "$1 $2 $3", ["64"]],
          ["(\\d{3})(\\d{3})", "$1 $2", ["64"]]
        ]],
        PM: ["508", "[45]\\d{5}", [
          ["([45]\\d)(\\d{2})(\\d{2})", "$1 $2 $3"]
        ], "0", "0$1"],
        PR: ["1", "[5789]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "787|939"],
        PS: ["970", "[24589]\\d{7,8}|1(?:[78]\\d{8}|[49]\\d{2,3})", [
          ["([2489])(2\\d{2})(\\d{4})", "$1 $2 $3", ["[2489]"]],
          ["(5[69]\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["5"]],
          ["(1[78]00)(\\d{3})(\\d{3})", "$1 $2 $3", ["1[78]"], "$1"]
        ], "0", "0$1"],
        PT: ["351", "[2-46-9]\\d{8}", [
          ["(2\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["2[12]"]],
          ["([2-46-9]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["2[3-9]|[346-9]"]]
        ]],
        PW: ["680", "[2-8]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2"]
        ]],
        PY: ["595", "5[0-5]\\d{4,7}|[2-46-9]\\d{5,8}", [
          ["(\\d{2})(\\d{5})", "$1 $2", ["(?:[26]1|3[289]|4[124678]|7[123]|8[1236])"], "(0$1)"],
          ["(\\d{2})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["(?:[26]1|3[289]|4[124678]|7[123]|8[1236])"], "(0$1)"],
          ["(\\d{3})(\\d{3,6})", "$1 $2", ["[2-9]0"], "0$1"],
          ["(\\d{3})(\\d{6})", "$1 $2", ["9[1-9]"], "0$1"],
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["8700"]],
          ["(\\d{3})(\\d{4,5})", "$1 $2", ["[2-8][1-9]"], "(0$1)"],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["[2-8][1-9]"], "0$1"]
        ], "0"],
        QA: ["974", "[2-8]\\d{6,7}", [
          ["([28]\\d{2})(\\d{4})", "$1 $2", ["[28]"]],
          ["([3-7]\\d{3})(\\d{4})", "$1 $2", ["[3-7]"]]
        ]],
        RE: ["262", "[268]\\d{8}", [
          ["([268]\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ], "0", "0$1", null, null, null, "262|6[49]|8"],
        RO: ["40", "2\\d{5,8}|[37-9]\\d{8}", [
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["[23]1"]],
          ["(21)(\\d{4})", "$1 $2", ["21"]],
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", ["[23][3-7]|[7-9]"]],
          ["(2\\d{2})(\\d{3})", "$1 $2", ["2[3-6]"]]
        ], "0", "0$1"],
        RS: ["381", "[126-9]\\d{4,11}|3(?:[0-79]\\d{3,10}|8[2-9]\\d{2,9})", [
          ["([23]\\d{2})(\\d{4,9})", "$1 $2", ["(?:2[389]|39)0"]],
          ["([1-3]\\d)(\\d{5,10})", "$1 $2", ["1|2(?:[0-24-7]|[389][1-9])|3(?:[0-8]|9[1-9])"]],
          ["(6\\d)(\\d{6,8})", "$1 $2", ["6"]],
          ["([89]\\d{2})(\\d{3,9})", "$1 $2", ["[89]"]],
          ["(7[26])(\\d{4,9})", "$1 $2", ["7[26]"]],
          ["(7[08]\\d)(\\d{4,9})", "$1 $2", ["7[08]"]]
        ], "0", "0$1"],
        RU: ["7", "[3489]\\d{9}", [
          ["([3489]\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1 $2-$3-$4", ["[34689]"]],
          ["(7\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["7"]]
        ], "8", "8 ($1)", null, null, !0, null, ["(?:3(?:0[12]|4[1-35-79]|5[1-3]|65|8[1-58]|9[0145])|4(?:01|1[1356]|2[13467]|7[1-5]|8[1-7]|9[1-689])|8(?:1[1-8]|2[01]|3[13-6]|4[0-8]|5[15]|6[1-35-79]|7[1-37-9]))\\d{7}", "9\\d{9}", "80[04]\\d{7}", "80[39]\\d{7}"]],
        RW: ["250", "[027-9]\\d{7,8}", [
          ["(2\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["2"], "$1"],
          ["([7-9]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["[7-9]"], "0$1"],
          ["(0\\d)(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["0"]]
        ], "0"],
        SA: ["966", "1\\d{7,8}|(?:[2-467]|92)\\d{7}|5\\d{8}|8\\d{9}", [
          ["([1-467])(\\d{3})(\\d{4})", "$1 $2 $3", ["[1-467]"]],
          ["(1\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["1[1-467]"]],
          ["(5\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["5"]],
          ["(92\\d{2})(\\d{5})", "$1 $2", ["92"], "$1"],
          ["(800)(\\d{3})(\\d{4})", "$1 $2 $3", ["80"], "$1"],
          ["(811)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["81"]]
        ], "0", "0$1"],
        SB: ["677", "[1-9]\\d{4,6}", [
          ["(\\d{2})(\\d{5})", "$1 $2", ["[7-9]"]]
        ]],
        SC: ["248", "[2468]\\d{5,6}", [
          ["(\\d{3})(\\d{3})", "$1 $2", ["8"]],
          ["(\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["[246]"]]
        ]],
        SD: ["249", "[19]\\d{8}", [
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3"]
        ], "0", "0$1"],
        SE: ["46", "[1-35-9]\\d{5,11}|4\\d{6,8}", [
          ["(8)(\\d{2,3})(\\d{2,3})(\\d{2})", "$1-$2 $3 $4", ["8"], null, null, "$1 $2 $3 $4"],
          ["([1-69]\\d)(\\d{2,3})(\\d{2})(\\d{2})", "$1-$2 $3 $4", ["1[013689]|2[0136]|3[1356]|4[0246]|54|6[03]|90"], null, null, "$1 $2 $3 $4"],
          ["([1-469]\\d)(\\d{3})(\\d{2})", "$1-$2 $3", ["1[136]|2[136]|3[356]|4[0246]|6[03]|90"], null, null, "$1 $2 $3"],
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1-$2 $3 $4", ["1[2457]|2(?:[247-9]|5[0138])|3[0247-9]|4[1357-9]|5[0-35-9]|6(?:[124-689]|7[0-2])|9(?:[125-8]|3[0-5]|4[0-3])"], null, null, "$1 $2 $3 $4"],
          ["(\\d{3})(\\d{2,3})(\\d{2})", "$1-$2 $3", ["1[2457]|2(?:[247-9]|5[0138])|3[0247-9]|4[1357-9]|5[0-35-9]|6(?:[124-689]|7[0-2])|9(?:[125-8]|3[0-5]|4[0-3])"], null, null, "$1 $2 $3"],
          ["(7\\d)(\\d{3})(\\d{2})(\\d{2})", "$1-$2 $3 $4", ["7"], null, null, "$1 $2 $3 $4"],
          ["(77)(\\d{2})(\\d{2})", "$1-$2$3", ["7"], null, null, "$1 $2 $3"],
          ["(20)(\\d{2,3})(\\d{2})", "$1-$2 $3", ["20"], null, null, "$1 $2 $3"],
          ["(9[034]\\d)(\\d{2})(\\d{2})(\\d{3})", "$1-$2 $3 $4", ["9[034]"], null, null, "$1 $2 $3 $4"],
          ["(9[034]\\d)(\\d{4})", "$1-$2", ["9[034]"], null, null, "$1 $2"],
          ["(\\d{3})(\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1-$2 $3 $4 $5", ["25[245]|67[3-6]"], null, null, "$1 $2 $3 $4 $5"]
        ], "0", "0$1"],
        SG: ["65", "[36]\\d{7}|[17-9]\\d{7,10}", [
          ["([3689]\\d{3})(\\d{4})", "$1 $2", ["[369]|8[1-9]"]],
          ["(1[89]00)(\\d{3})(\\d{4})", "$1 $2 $3", ["1[89]"]],
          ["(7000)(\\d{4})(\\d{3})", "$1 $2 $3", ["70"]],
          ["(800)(\\d{3})(\\d{4})", "$1 $2 $3", ["80"]]
        ]],
        SH: ["290", "[256]\\d{4}", [], null, null, null, null, null, null, ["2(?:[0-57-9]\\d|6[4-9])\\d{2}", "[56]\\d{4}", null, null, null, null, null, null, "262\\d{2}"]],
        SI: ["386", "[1-7]\\d{6,7}|[89]\\d{4,7}", [
          ["(\\d)(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[12]|3[24-8]|4[24-8]|5[2-8]|7[3-8]"], "(0$1)"],
          ["([3-7]\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["[37][01]|4[0139]|51|6"]],
          ["([89][09])(\\d{3,6})", "$1 $2", ["[89][09]"]],
          ["([58]\\d{2})(\\d{5})", "$1 $2", ["59|8[1-3]"]]
        ], "0", "0$1"],
        SJ: ["47", "0\\d{4}|[4789]\\d{7}", [
          ["([489]\\d{2})(\\d{2})(\\d{3})", "$1 $2 $3", ["[489]"]],
          ["([235-7]\\d)(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[235-7]"]]
        ], null, null, null, null, null, null, ["79\\d{6}", "(?:4[015-8]|5[89]|9\\d)\\d{6}", "80[01]\\d{5}", "82[09]\\d{5}", "880\\d{5}", "81[23]\\d{5}", "0\\d{4}|81(?:0(?:0[7-9]|1\\d)|5\\d{2})\\d{3}", null, "85[0-5]\\d{5}"]],
        SK: ["421", "(?:[2-68]\\d{5,8}|9\\d{6,8})", [
          ["(2)(16)(\\d{3,4})", "$1 $2 $3", ["216"]],
          ["([3-5]\\d)(16)(\\d{2,3})", "$1 $2 $3", ["[3-5]"]],
          ["(2)(\\d{3})(\\d{3})(\\d{2})", "$1/$2 $3 $4", ["2"]],
          ["([3-5]\\d)(\\d{3})(\\d{2})(\\d{2})", "$1/$2 $3 $4", ["[3-5]"]],
          ["([689]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["[689]"]],
          ["(9090)(\\d{3})", "$1 $2", ["9090"]]
        ], "0", "0$1"],
        SL: ["232", "[2-9]\\d{7}", [
          ["(\\d{2})(\\d{6})", "$1 $2"]
        ], "0", "(0$1)"],
        SM: ["378", "[05-7]\\d{7,9}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[5-7]"]],
          ["(0549)(\\d{6})", "$1 $2", ["0"], null, null, "($1) $2"],
          ["(\\d{6})", "0549 $1", ["[89]"], null, null, "(0549) $1"]
        ], null, null, "(?:0549)?([89]\\d{5})", "0549$1"],
        SN: ["221", "[3789]\\d{8}", [
          ["(\\d{2})(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[379]"]],
          ["(\\d{3})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["8"]]
        ]],
        SO: ["252", "[1-79]\\d{6,8}", [
          ["(\\d)(\\d{6})", "$1 $2", ["2[0-79]|[13-5]"]],
          ["(\\d)(\\d{7})", "$1 $2", ["24|[67]"]],
          ["(\\d{2})(\\d{5,7})", "$1 $2", ["15|28|6[1-35-9]|799|9[2-9]"]],
          ["(90\\d)(\\d{3})(\\d{3})", "$1 $2 $3", ["90"]]
        ], "0"],
        SR: ["597", "[2-8]\\d{5,6}", [
          ["(\\d{3})(\\d{3})", "$1-$2", ["[2-4]|5[2-58]"]],
          ["(\\d{2})(\\d{2})(\\d{2})", "$1-$2-$3", ["56"]],
          ["(\\d{3})(\\d{4})", "$1-$2", ["59|[6-8]"]]
        ]],
        SS: ["211", "[19]\\d{8}", [
          ["(\\d{3})(\\d{3})(\\d{3})", "$1 $2 $3", null, "0$1"]
        ], "0"],
        ST: ["239", "[29]\\d{6}", [
          ["(\\d{3})(\\d{4})", "$1 $2"]
        ]],
        SV: ["503", "[267]\\d{7}|[89]\\d{6}(?:\\d{4})?", [
          ["(\\d{4})(\\d{4})", "$1 $2", ["[267]"]],
          ["(\\d{3})(\\d{4})", "$1 $2", ["[89]"]],
          ["(\\d{3})(\\d{4})(\\d{4})", "$1 $2 $3", ["[89]"]]
        ]],
        SX: ["1", "[5789]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "721"],
        SY: ["963", "[1-59]\\d{7,8}", [
          ["(\\d{2})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[1-5]"]],
          ["(9\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["9"]]
        ], "0", "0$1", null, null, !0],
        SZ: ["268", "[027]\\d{7}", [
          ["(\\d{4})(\\d{4})", "$1 $2", ["[027]"]]
        ]],
        TA: ["290", "8\\d{3}", [], null, null, null, null, null, null, ["8\\d{3}"]],
        TC: ["1", "[5689]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "649"],
        TD: ["235", "[2679]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        TG: ["228", "[29]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ]],
        TH: ["66", "[2-9]\\d{7,8}|1\\d{3}(?:\\d{5,6})?", [
          ["(2)(\\d{3})(\\d{4})", "$1 $2 $3", ["2"]],
          ["([13-9]\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["14|[3-9]"]],
          ["(1[89]00)(\\d{3})(\\d{3})", "$1 $2 $3", ["1"], "$1"]
        ], "0", "0$1"],
        TJ: ["992", "[3-589]\\d{8}", [
          ["([349]\\d{2})(\\d{2})(\\d{4})", "$1 $2 $3", ["[34]7|91[78]"]],
          ["([4589]\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["4[148]|[58]|9(?:1[59]|[0235-9])"]],
          ["(331700)(\\d)(\\d{2})", "$1 $2 $3", ["331", "3317", "33170", "331700"]],
          ["(\\d{4})(\\d)(\\d{4})", "$1 $2 $3", ["3[1-5]", "3(?:[1245]|3(?:[02-9]|1[0-589]))"]]
        ], "8", "(8) $1", null, null, !0],
        TK: ["690", "[2-47]\\d{3,6}"],
        TL: ["670", "[2-489]\\d{6}|7\\d{6,7}", [
          ["(\\d{3})(\\d{4})", "$1 $2", ["[2-489]"]],
          ["(\\d{4})(\\d{4})", "$1 $2", ["7"]]
        ]],
        TM: ["993", "[1-6]\\d{7}", [
          ["(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2-$3-$4", ["12"]],
          ["(\\d{2})(\\d{6})", "$1 $2", ["6"], "8 $1"],
          ["(\\d{3})(\\d)(\\d{2})(\\d{2})", "$1 $2-$3-$4", ["13|[2-5]"]]
        ], "8", "(8 $1)"],
        TN: ["216", "[2-57-9]\\d{7}", [
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3"]
        ]],
        TO: ["676", "[02-8]\\d{4,6}", [
          ["(\\d{2})(\\d{3})", "$1-$2", ["[1-6]|7[0-4]|8[05]"]],
          ["(\\d{3})(\\d{4})", "$1 $2", ["7[5-9]|8[47-9]"]],
          ["(\\d{4})(\\d{3})", "$1 $2", ["0"]]
        ]],
        TR: ["90", "[2-589]\\d{9}|444\\d{4}", [
          ["(\\d{3})(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["[23]|4(?:[0-35-9]|4[0-35-9])"], "(0$1)", "true"],
          ["(\\d{3})(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["5[02-69]"], "0$1", "true"],
          ["(\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["51|[89]"], "0$1", "true"],
          ["(444)(\\d{1})(\\d{3})", "$1 $2 $3", ["444"]]
        ], "0"],
        TT: ["1", "[589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "868"],
        TV: ["688", "[279]\\d{4,6}"],
        TW: ["886", "2\\d{6,8}|[3-689]\\d{7,8}|7\\d{7,9}", [
          ["(20)(\\d)(\\d{4})", "$1 $2 $3", ["202"]],
          ["(20)(\\d{3})(\\d{4})", "$1 $2 $3", ["20[013-9]"]],
          ["([2-8])(\\d{3,4})(\\d{4})", "$1 $2 $3", ["2[23-8]|[3-6]|[78][1-9]"]],
          ["([89]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["80|9"]],
          ["(70)(\\d{4})(\\d{4})", "$1 $2 $3", ["70"]]
        ], "0", "0$1"],
        TZ: ["255", "\\d{9}", [
          ["([24]\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["[24]"]],
          ["([67]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["[67]"]],
          ["([89]\\d{2})(\\d{2})(\\d{4})", "$1 $2 $3", ["[89]"]]
        ], "0", "0$1"],
        UA: ["380", "[3-9]\\d{8}", [
          ["([3-9]\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["[38]9|4(?:[45][0-5]|87)|5(?:0|6[37]|7[37])|6[36-8]|73|9[1-9]", "[38]9|4(?:[45][0-5]|87)|5(?:0|6(?:3[14-7]|7)|7[37])|6[36-8]|73|9[1-9]"]],
          ["([3-689]\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["3[1-8]2|4[13678]2|5(?:[12457]2|6[24])|6(?:[49]2|[12][29]|5[24])|8[0-8]|90", "3(?:[1-46-8]2[013-9]|52)|4(?:[1378]2|62[013-9])|5(?:[12457]2|6[24])|6(?:[49]2|[12][29]|5[24])|8[0-8]|90"]],
          ["([3-6]\\d{3})(\\d{5})", "$1 $2", ["3(?:5[013-9]|[1-46-8])|4(?:[137][013-9]|6|[45][6-9]|8[4-6])|5(?:[1245][013-9]|6[0135-9]|3|7[4-6])|6(?:[49][013-9]|5[0135-9]|[12][13-8])", "3(?:5[013-9]|[1-46-8](?:22|[013-9]))|4(?:[137][013-9]|6(?:[013-9]|22)|[45][6-9]|8[4-6])|5(?:[1245][013-9]|6(?:3[02389]|[015689])|3|7[4-6])|6(?:[49][013-9]|5[0135-9]|[12][13-8])"]]
        ], "0", "0$1"],
        UG: ["256", "\\d{9}", [
          ["(\\d{3})(\\d{6})", "$1 $2", ["[7-9]|20(?:[013-8]|2[5-9])|4(?:6[45]|[7-9])"]],
          ["(\\d{2})(\\d{7})", "$1 $2", ["3|4(?:[1-5]|6[0-36-9])"]],
          ["(2024)(\\d{5})", "$1 $2", ["2024"]]
        ], "0", "0$1"],
        US: ["1", "[2-9]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, !0, null, ["(?:2(?:0[1-35-9]|1[02-9]|2[04589]|3[149]|4[08]|5[1-46]|6[0279]|7[026]|8[13])|3(?:0[1-57-9]|1[02-9]|2[0135]|3[014679]|4[67]|5[12]|6[014]|8[056])|4(?:0[124-9]|1[02-579]|2[3-5]|3[0245]|4[0235]|58|6[39]|7[0589]|8[04])|5(?:0[1-57-9]|1[0235-8]|20|3[0149]|4[01]|5[19]|6[1-37]|7[013-5]|8[056])|6(?:0[1-35-9]|1[024-9]|2[03689]|3[016]|4[16]|5[017]|6[0-279]|78|8[12])|7(?:0[1-46-8]|1[02-9]|2[0457]|3[1247]|4[037]|5[47]|6[02359]|7[02-59]|8[156])|8(?:0[1-68]|1[02-8]|28|3[0-25]|4[3578]|5[046-9]|6[02-5]|7[028])|9(?:0[1346-9]|1[02-9]|2[0589]|3[014678]|4[0179]|5[12469]|7[0-3589]|8[0459]))[2-9]\\d{6}", null, "8(?:00|44|55|66|77|88)[2-9]\\d{6}", "900[2-9]\\d{6}", "5(?:00|22|33|44|66|77|88)[2-9]\\d{6}"]],
        UY: ["598", "[2489]\\d{6,7}", [
          ["(\\d{4})(\\d{4})", "$1 $2", ["[24]"]],
          ["(\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["9[1-9]"], "0$1"],
          ["(\\d{3})(\\d{4})", "$1 $2", ["[89]0"], "0$1"]
        ], "0"],
        UZ: ["998", "[679]\\d{8}", [
          ["([679]\\d)(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ], "8", "8 $1"],
        VA: ["39", "(?:0(?:878\\d{5}|6698\\d{5})|[1589]\\d{5,10}|3(?:[12457-9]\\d{8}|[36]\\d{7,9}))", [
          ["(\\d{2})(\\d{3,4})(\\d{4})", "$1 $2 $3", ["0[26]|55"]],
          ["(0[26])(\\d{4})(\\d{5})", "$1 $2 $3", ["0[26]"]],
          ["(0[26])(\\d{4,6})", "$1 $2", ["0[26]"]],
          ["(0\\d{2})(\\d{3,4})(\\d{4})", "$1 $2 $3", ["0[13-57-9][0159]"]],
          ["(\\d{3})(\\d{3,6})", "$1 $2", ["0[13-57-9][0159]|8(?:03|4[17]|9[245])", "0[13-57-9][0159]|8(?:03|4[17]|9(?:2|[45][0-4]))"]],
          ["(0\\d{3})(\\d{3})(\\d{4})", "$1 $2 $3", ["0[13-57-9][2-46-8]"]],
          ["(0\\d{3})(\\d{2,6})", "$1 $2", ["0[13-57-9][2-46-8]"]],
          ["(\\d{3})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[13]|8(?:00|4[08]|9[59])", "[13]|8(?:00|4[08]|9(?:5[5-9]|9))"]],
          ["(\\d{4})(\\d{4})", "$1 $2", ["894", "894[5-9]"]],
          ["(\\d{3})(\\d{4})(\\d{4})", "$1 $2 $3", ["3"]]
        ], null, null, null, null, null, null, ["06698\\d{5}", "3(?:[12457-9]\\d{8}|6\\d{7,8}|3\\d{7,9})", "80(?:0\\d{6}|3\\d{3})", "0878\\d{5}|1(?:44|6[346])\\d{6}|89(?:2\\d{3}|4(?:[0-4]\\d{2}|[5-9]\\d{4})|5(?:[0-4]\\d{2}|[5-9]\\d{6})|9\\d{6})", "1(?:78\\d|99)\\d{6}", null, null, null, "55\\d{8}"]],
        VC: ["1", "[5789]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "784"],
        VE: ["58", "[24589]\\d{9}", [
          ["(\\d{3})(\\d{7})", "$1-$2"]
        ], "0", "0$1"],
        VG: ["1", "[2589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "284"],
        VI: ["1", "[3589]\\d{9}", [
          ["(\\d{3})(\\d{3})(\\d{4})", "($1) $2-$3", null, null, null, "$1-$2-$3"]
        ], "1", null, null, null, null, "340"],
        VN: ["84", "[17]\\d{6,9}|[2-69]\\d{7,9}|8\\d{6,8}", [
          ["([17]99)(\\d{4})", "$1 $2", ["[17]99"]],
          ["([48])(\\d{4})(\\d{4})", "$1 $2 $3", ["4|8(?:[1-57]|[689][0-79])"]],
          ["([235-7]\\d)(\\d{4})(\\d{3})", "$1 $2 $3", ["2[025-79]|3[0136-9]|5[2-9]|6[0-46-8]|7[02-79]"]],
          ["(80)(\\d{5})", "$1 $2", ["80"]],
          ["(69\\d)(\\d{4,5})", "$1 $2", ["69"]],
          ["([235-7]\\d{2})(\\d{4})(\\d{3})", "$1 $2 $3", ["2[1348]|3[25]|5[01]|65|7[18]"]],
          ["([89]\\d)(\\d{3})(\\d{2})(\\d{2})", "$1 $2 $3 $4", ["8[689]8|9"]],
          ["(1[2689]\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["1(?:[26]|8[68]|99)"]],
          ["(1[89]00)(\\d{4,6})", "$1 $2", ["1[89]0"], "$1"]
        ], "0", "0$1", null, null, !0],
        VU: ["678", "[2-57-9]\\d{4,6}", [
          ["(\\d{3})(\\d{4})", "$1 $2", ["[579]"]]
        ]],
        WF: ["681", "[4-8]\\d{5}", [
          ["(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3"]
        ]],
        WS: ["685", "[2-8]\\d{4,6}", [
          ["(8\\d{2})(\\d{3,4})", "$1 $2", ["8"]],
          ["(7\\d)(\\d{5})", "$1 $2", ["7"]],
          ["(\\d{5})", "$1", ["[2-6]"]]
        ]],
        YE: ["967", "[1-7]\\d{6,8}", [
          ["([1-7])(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[1-6]|7[24-68]"]],
          ["(7\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["7[0137]"]]
        ], "0", "0$1"],
        YT: ["262", "[268]\\d{8}", [
          ["([268]\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1 $2 $3 $4"]
        ], "0", "0$1", null, null, null, "269|63"],
        ZA: ["27", "[1-79]\\d{8}|8(?:[067]\\d{7}|[1-4]\\d{3,7})", [
          ["(860)(\\d{3})(\\d{3})", "$1 $2 $3", ["860"]],
          ["(\\d{2})(\\d{3})(\\d{4})", "$1 $2 $3", ["[1-79]|8(?:[0-47]|6[1-9])"]],
          ["(\\d{2})(\\d{3,4})", "$1 $2", ["8[1-4]"]],
          ["(\\d{2})(\\d{3})(\\d{2,3})", "$1 $2 $3", ["8[1-4]"]]
        ], "0", "0$1"],
        ZM: ["260", "[289]\\d{8}", [
          ["([29]\\d)(\\d{7})", "$1 $2", ["[29]"]],
          ["(800)(\\d{3})(\\d{3})", "$1 $2 $3", ["8"]]
        ], "0", "0$1"],
        ZW: ["263", "2(?:[012457-9]\\d{3,8}|6(?:[14]\\d{7}|\\d{4}))|[13-79]\\d{4,9}|8[06]\\d{8}", [
          ["([49])(\\d{3})(\\d{2,4})", "$1 $2 $3", ["4|9[2-9]"]],
          ["(7\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["7"]],
          ["(86\\d{2})(\\d{3})(\\d{3})", "$1 $2 $3", ["86[24]"]],
          ["([2356]\\d{2})(\\d{3,5})", "$1 $2", ["2(?:0[45]|2[278]|[49]8|[78])|3(?:08|17|3[78]|7[1569]|8[37]|98)|5[15][78]|6(?:[29]8|[38]7|6[78]|75|[89]8)"]],
          ["(\\d{3})(\\d{3})(\\d{3,4})", "$1 $2 $3", ["2(?:1[39]|2[0157]|6[14]|7[35]|84)|329"]],
          ["([1-356]\\d)(\\d{3,5})", "$1 $2", ["1[3-9]|2[0569]|3[0-69]|5[05689]|6[0-46-9]"]],
          ["([235]\\d)(\\d{3})(\\d{3,4})", "$1 $2 $3", ["[23]9|54"]],
          ["([25]\\d{3})(\\d{3,5})", "$1 $2", ["(?:25|54)8", "258[23]|5483"]],
          ["(8\\d{3})(\\d{6})", "$1 $2", ["86"]],
          ["(80\\d)(\\d{3})(\\d{4})", "$1 $2 $3", ["80"]]
        ], "0", "0$1"],
        "001": ["979", "\\d{9}", [
          ["(\\d)(\\d{4})(\\d{4})", "$1 $2 $3"]
        ]]
      }
    }
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d && d.__esModule ? d : {
        default: d
      }
    }
    Object.defineProperty($, "__esModule", {
      value: !0
    });
    var e = t(6);
    Object.defineProperty($, "parse", {
      enumerable: !0,
      get: function () {
        return n(e).default
      }
    });
    var r = t(12);
    Object.defineProperty($, "format", {
      enumerable: !0,
      get: function () {
        return n(r).default
      }
    });
    var l = t(35);
    Object.defineProperty($, "is_valid_number", {
      enumerable: !0,
      get: function () {
        return n(l).default
      }
    }), Object.defineProperty($, "isValidNumber", {
      enumerable: !0,
      get: function () {
        return n(l).default
      }
    });
    var u = t(34);
    Object.defineProperty($, "as_you_type", {
      enumerable: !0,
      get: function () {
        return n(u).default
      }
    }), Object.defineProperty($, "asYouType", {
      enumerable: !0,
      get: function () {
        return n(u).default
      }
    })
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d && d.__esModule ? d : {
        default: d
      }
    }

    function e(d, $) {
      for (var t = d.slice(0, $), n = r("(", t), e = r(")", t), l = n - e; l > 0 && $ < d.length;) ")" === d[$] && l--, $++;
      return d.slice(0, $)
    }

    function r(d, $) {
      var t = 0,
        n = !0,
        e = !1,
        r = void 0;
      try {
        for (var l, u = (0, i.default)($); !(n = (l = u.next()).done); n = !0) {
          var o = l.value;
          o === d && t++
        }
      } catch (d) {
        e = !0, r = d
      } finally {
        try {
          !n && u.return && u.return()
        } finally {
          if (e) throw r
        }
      }
      return t
    }

    function l(d, $) {
      if ($ < 1) return "";
      for (var t = ""; $ > 1;) 1 & $ && (t += d), $ >>= 1, d += d;
      return t + d
    }
    Object.defineProperty($, "__esModule", {
      value: !0
    }), $.default = function (d) {
      var $ = function () {
        function $(t) {
          (0, a.default)(this, $), t && d.countries[t] && (this.default_country = t), this.reset()
        }
        return (0, f.default)($, [{
          key: "input",
          value: function (d) {
            var $ = (0, s.extract_formatted_phone_number)(d);
            return $ || d && d.indexOf("+") >= 0 && ($ = "+"), (0, h.matches_entirely)($, I) ? this.process_input((0, s.parse_phone_number)($)) : this.current_output
          }
        }, {
          key: "process_input",
          value: function (d) {
            if ("+" === d[0] && (this.parsed_input || (this.parsed_input += "+", this.reset_countriness()), d = d.slice(1)), this.parsed_input += d, this.valid = !1, this.national_number += d, this.is_international())
              if (this.country_phone_code) this.country || this.determine_the_country();
              else {
                if (!this.extract_country_phone_code()) return this.parsed_input;
                this.initialize_phone_number_formats_for_this_country_phone_code(), this.reset_format(), this.determine_the_country()
              }
            else {
              var $ = this.national_prefix;
              this.national_number = this.national_prefix + this.national_number, this.extract_national_prefix(), this.national_prefix !== $ && (this.matching_formats = this.available_formats, this.reset_format())
            }
            var t = this.format_national_phone_number(d);
            return t ? this.full_phone_number(t) : this.parsed_input
          }
        }, {
          key: "format_national_phone_number",
          value: function (d) {
            var $ = void 0;
            this.chosen_format && ($ = this.format_next_national_number_digits(d));
            var t = this.attempt_to_format_complete_phone_number();
            return t ? (this.country && (this.valid = !0), t) : (this.match_formats_by_leading_digits(), this.choose_another_format() ? this.reformat_national_number() : $)
          }
        }, {
          key: "reset",
          value: function () {
            return this.parsed_input = "", this.current_output = "", this.national_prefix = "", this.national_number = "", this.reset_countriness(), this.reset_format(), this.valid = !1, this
          }
        }, {
          key: "reset_country",
          value: function () {
            this.default_country && !this.is_international() ? this.country = this.default_country : this.country = void 0
          }
        }, {
          key: "reset_countriness",
          value: function () {
            this.reset_country(), this.default_country && !this.is_international() ? (this.country_metadata = d.countries[this.default_country], this.country_phone_code = this.country_metadata.phone_code, this.initialize_phone_number_formats_for_this_country_phone_code()) : (this.country_metadata = void 0, this.country_phone_code = void 0, this.available_formats = [], this.matching_formats = this.available_formats)
          }
        }, {
          key: "reset_format",
          value: function () {
            this.chosen_format = void 0, this.template = void 0, this.partially_populated_template = void 0, this.last_match_position = -1
          }
        }, {
          key: "reformat_national_number",
          value: function () {
            return this.format_next_national_number_digits(this.national_number)
          }
        }, {
          key: "initialize_phone_number_formats_for_this_country_phone_code",
          value: function () {
            this.available_formats = (0, c.get_formats)(this.country_metadata).filter(function (d) {
              return E.test((0, c.get_format_international_format)(d))
            }).sort(function (d, $) {
              return 0 === (0, c.get_format_leading_digits_patterns)(d).length && (0, c.get_format_leading_digits_patterns)($).length > 0 ? -1 : (0, c.get_format_leading_digits_patterns)(d).length > 0 && 0 === (0, c.get_format_leading_digits_patterns)($).length ? 1 : 0
            }), this.matching_formats = this.available_formats
          }
        }, {
          key: "match_formats_by_leading_digits",
          value: function () {
            var d = this.national_number,
              $ = d.length - T;
            $ < 0 && ($ = 0), this.matching_formats = this.get_relevant_phone_number_formats().filter(function (t) {
              var n = (0, c.get_format_leading_digits_patterns)(t).length;
              if (0 === n) return !0;
              var e = Math.min($, n - 1),
                r = (0, c.get_format_leading_digits_patterns)(t)[e];
              return new RegExp("^" + r).test(d)
            })
          }
        }, {
          key: "get_relevant_phone_number_formats",
          value: function () {
            var d = this.national_number;
            return d.length <= T ? this.available_formats : this.matching_formats
          }
        }, {
          key: "attempt_to_format_complete_phone_number",
          value: function () {
            var d = !0,
              $ = !1,
              t = void 0;
            try {
              for (var n, e = (0, i.default)(this.get_relevant_phone_number_formats()); !(d = (n = e.next()).done); d = !0) {
                var r = n.value,
                  l = new RegExp("^(?:" + (0, c.get_format_pattern)(r) + ")$");
                if (l.test(this.national_number)) {
                  if (this.validate_format(r)) {
                    this.reset_format(), this.chosen_format = r;
                    var u = (0, p.format_national_number_using_format)(this.national_number, r, this.is_international(), this.national_prefix.length > 0, this.country_metadata);
                    if (this.create_formatting_template(r)) this.reformat_national_number();
                    else {
                      var o = this.full_phone_number(u);
                      this.template = o.replace(/[\d\+]/g, b), this.partially_populated_template = o
                    }
                    return u
                  }
                } else if (this.national_prefix && (0, c.get_format_national_prefix_is_optional_when_formatting)(r, this.country_metadata)) {
                  if (!l.test(this.national_prefix + this.national_number)) continue;
                  this.national_number = this.national_prefix + this.national_number, this.national_prefix = ""
                }
              }
            } catch (d) {
              $ = !0, t = d
            } finally {
              try {
                !d && e.return && e.return()
              } finally {
                if ($) throw t
              }
            }
          }
        }, {
          key: "full_phone_number",
          value: function (d) {
            return this.is_international() ? "+" + this.country_phone_code + " " + d : d
          }
        }, {
          key: "extract_country_phone_code",
          value: function () {
            if (this.national_number) {
              var $ = (0, s.parse_phone_number_and_country_phone_code)(this.parsed_input, d),
                t = $.country_phone_code,
                n = $.number;
              if (t) return this.country_phone_code = t, this.national_number = n, this.country_metadata = (0, c.get_metadata_by_country_phone_code)(t, d)
            }
          }
        }, {
          key: "extract_national_prefix",
          value: function () {
            if (this.national_prefix = "", this.country_metadata) {
              var d = (0, c.get_national_prefix_for_parsing)(this.country_metadata);
              if (d) {
                var $ = this.national_number.match(new RegExp("^(?:" + d + ")"));
                if ($ && $[0]) {
                  var t = $[0].length;
                  return this.national_prefix = this.national_number.slice(0, t), this.national_number = this.national_number.slice(t), this.national_prefix
                }
              }
            }
          }
        }, {
          key: "choose_another_format",
          value: function () {
            var d = !0,
              $ = !1,
              t = void 0;
            try {
              for (var n, e = (0, i.default)(this.get_relevant_phone_number_formats()); !(d = (n = e.next()).done); d = !0) {
                var r = n.value;
                if (this.chosen_format === r) return;
                if (this.validate_format(r) && this.create_formatting_template(r)) return this.chosen_format = r, this.last_match_position = -1, !0
              }
            } catch (d) {
              $ = !0, t = d
            } finally {
              try {
                !d && e.return && e.return()
              } finally {
                if ($) throw t
              }
            }
            this.reset_country(), this.reset_format()
          }
        }, {
          key: "validate_format",
          value: function (d) {
            if (this.is_international() || this.national_prefix || !(0, c.get_format_national_prefix_is_mandatory_when_formatting)(d, this.country_metadata)) return !0
          }
        }, {
          key: "create_formatting_template",
          value: function (d) {
            if (!((0, c.get_format_pattern)(d).indexOf("|") >= 0)) {
              var $ = ((0, c.get_format_national_prefix_formatting_rule)(d, this.country_metadata), (0, c.get_format_pattern)(d).replace(S, "\\d").replace(O, "\\d")),
                t = g.match($)[0];
              if (!(this.national_number.length > t.length)) {
                var n = this.get_format_format(d);
                if (this.national_prefix) {
                  var e = (0, c.get_format_national_prefix_formatting_rule)(d, this.country_metadata);
                  e && (n = n.replace(p.FIRST_GROUP_PATTERN, e))
                }
                var r = t.replace(new RegExp($, "g"), n).replace(y, b);
                return this.partially_populated_template = r, r = this.is_international() ? b + l(b, this.country_phone_code.length) + " " + r : r.replace(/\d/g, b), this.template = r
              }
            }
          }
        }, {
          key: "format_next_national_number_digits",
          value: function (d) {
            var $ = !0,
              t = !1,
              n = void 0;
            try {
              for (var r, l = (0, i.default)(d); !($ = (r = l.next()).done); $ = !0) {
                var u = r.value;
                if (this.partially_populated_template.slice(this.last_match_position + 1).search(x) === -1) return this.chosen_format = void 0, this.template = void 0, void(this.partially_populated_template = void 0);
                this.last_match_position = this.partially_populated_template.search(x), this.partially_populated_template = this.partially_populated_template.replace(x, u)
              }
            } catch (d) {
              t = !0, n = d
            } finally {
              try {
                !$ && l.return && l.return()
              } finally {
                if (t) throw n
              }
            }
            return e(this.partially_populated_template, this.last_match_position + 1).replace(M, " ")
          }
        }, {
          key: "is_international",
          value: function () {
            return this.parsed_input && "+" === this.parsed_input[0]
          }
        }, {
          key: "get_format_format",
          value: function (d) {
            return this.is_international() ? (0, p.local_to_international_style)((0, c.get_format_international_format)(d)) : (0, c.get_format_format)(d)
          }
        }, {
          key: "determine_the_country",
          value: function () {
            this.country = (0, s.find_country_code)(this.country_phone_code, this.national_number, d)
          }
        }]), $
      }();
      return $.DIGIT_PLACEHOLDER = b, $
    }, $.close_dangling_braces = e, $.count_occurences = r, $.repeat = l;
    var u = t(13),
      i = n(u),
      o = t(38),
      a = n(o),
      _ = t(39),
      f = n(_),
      c = t(5),
      s = t(6),
      p = t(12),
      h = t(11),
      m = "9",
      y = new RegExp(m, "g"),
      v = 15,
      g = l(m, v),
      b = "x",
      x = new RegExp(b),
      M = new RegExp(b, "g"),
      S = /\[([^\[\]])*\]/g,
      O = /\d(?=[^,}][^,}])/g,
      E = new RegExp("^[" + s.VALID_PUNCTUATION + "]*(\\$\\d[" + s.VALID_PUNCTUATION + "]*)+$"),
      T = 3,
      A = "[" + s.PLUS_CHARS + "]{0,1}[" + s.VALID_PUNCTUATION + s.VALID_DIGITS + "]*",
      I = new RegExp("^" + A + "$", "i")
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d && d.__esModule ? d : {
        default: d
      }
    }

    function e(d, $) {
      if (!d) return !1;
      if ("string" == typeof d && (d = l.default.call(this, d, $)), !d.country) return !1;
      var t = this.metadata.countries[d.country];
      return !((0, u.get_types)(t) && !(0, r.get_number_type)(d.phone, t))
    }
    Object.defineProperty($, "__esModule", {
      value: !0
    }), $.default = e;
    var r = t(6),
      l = n(r),
      u = t(5)
  }, function (d, $, t) {
    d.exports = {
      default: t(42),
      __esModule: !0
    }
  }, function (d, $, t) {
    d.exports = {
      default: t(43),
      __esModule: !0
    }
  }, function (d, $, t) {
    "use strict";
    $.__esModule = !0, $.default = function (d, $) {
      if (!(d instanceof $)) throw new TypeError("Cannot call a class as a function")
    }
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d && d.__esModule ? d : {
        default: d
      }
    }
    $.__esModule = !0;
    var e = t(37),
      r = n(e);
    $.default = function () {
      function d(d, $) {
        for (var t = 0; t < $.length; t++) {
          var n = $[t];
          n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), (0, r.default)(d, n.key, n)
        }
      }
      return function ($, t, n) {
        return t && d($.prototype, t), n && d($, n), $
      }
    }()
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d && d.__esModule ? d : {
        default: d
      }
    }
    $.__esModule = !0;
    var e = t(36),
      r = n(e);
    $.default = r.default || function (d) {
      for (var $ = 1; $ < arguments.length; $++) {
        var t = arguments[$];
        for (var n in t) Object.prototype.hasOwnProperty.call(t, n) && (d[n] = t[n])
      }
      return d
    }
  }, function (d, $, t) {
    t(72), t(71), d.exports = t(67)
  }, function (d, $, t) {
    t(69), d.exports = t(2).Object.assign
  }, function (d, $, t) {
    t(70);
    var n = t(2).Object;
    d.exports = function (d, $, t) {
      return n.defineProperty(d, $, t)
    }
  }, function (d, $) {
    d.exports = function (d) {
      if ("function" != typeof d) throw TypeError(d + " is not a function!");
      return d
    }
  }, function (d, $) {
    d.exports = function () {}
  }, function (d, $, t) {
    var n = t(20),
      e = t(64),
      r = t(63);
    d.exports = function (d) {
      return function ($, t, l) {
        var u, i = n($),
          o = e(i.length),
          a = r(l, o);
        if (d && t != t) {
          for (; o > a;)
            if (u = i[a++], u != u) return !0
        } else
          for (; o > a; a++)
            if ((d || a in i) && i[a] === t) return d || a || 0;
        return !d && -1
      }
    }
  }, function (d, $, t) {
    var n = t(21),
      e = t(1)("toStringTag"),
      r = "Arguments" == n(function () {
        return arguments
      }()),
      l = function (d, $) {
        try {
          return d[$]
        } catch (d) {}
      };
    d.exports = function (d) {
      var $, t, u;
      return void 0 === d ? "Undefined" : null === d ? "Null" : "string" == typeof (t = l($ = Object(d), e)) ? t : r ? n($) : "Object" == (u = n($)) && "function" == typeof $.callee ? "Arguments" : u
    }
  }, function (d, $, t) {
    var n = t(44);
    d.exports = function (d, $, t) {
      if (n(d), void 0 === $) return d;
      switch (t) {
        case 1:
          return function (t) {
            return d.call($, t)
          };
        case 2:
          return function (t, n) {
            return d.call($, t, n)
          };
        case 3:
          return function (t, n, e) {
            return d.call($, t, n, e)
          }
      }
      return function () {
        return d.apply($, arguments)
      }
    }
  }, function (d, $, t) {
    d.exports = t(0).document && document.documentElement
  }, function (d, $, t) {
    d.exports = !t(3) && !t(16)(function () {
      return 7 != Object.defineProperty(t(22)("div"), "a", {
        get: function () {
          return 7
        }
      }).a
    })
  }, function (d, $, t) {
    "use strict";
    var n = t(55),
      e = t(27),
      r = t(28),
      l = {};
    t(4)(l, t(1)("iterator"), function () {
      return this
    }), d.exports = function (d, $, t) {
      d.prototype = n(l, {
        next: e(1, t)
      }), r(d, $ + " Iterator")
    }
  }, function (d, $) {
    d.exports = function (d, $) {
      return {
        value: $,
        done: !!d
      }
    }
  }, function (d, $) {
    d.exports = !0
  }, function (d, $, t) {
    "use strict";
    var n = t(26),
      e = t(57),
      r = t(60),
      l = t(30),
      u = t(24),
      i = Object.assign;
    d.exports = !i || t(16)(function () {
      var d = {},
        $ = {},
        t = Symbol(),
        n = "abcdefghijklmnopqrst";
      return d[t] = 7, n.split("").forEach(function (d) {
        $[d] = d
      }), 7 != i({}, d)[t] || Object.keys(i({}, $)).join("") != n
    }) ? function (d, $) {
      for (var t = l(d), i = arguments.length, o = 1, a = e.f, _ = r.f; i > o;)
        for (var f, c = u(arguments[o++]), s = a ? n(c).concat(a(c)) : n(c), p = s.length, h = 0; p > h;) _.call(c, f = s[h++]) && (t[f] = c[f]);
      return t
    } : i
  }, function (d, $, t) {
    var n = t(7),
      e = t(56),
      r = t(23),
      l = t(18)("IE_PROTO"),
      u = function () {},
      i = "prototype",
      o = function () {
        var d, $ = t(22)("iframe"),
          n = r.length,
          e = "<",
          l = ">";
        for ($.style.display = "none", t(49).appendChild($), $.src = "javascript:", d = $.contentWindow.document, d.open(), d.write(e + "script" + l + "document.F=Object" + e + "/script" + l), d.close(), o = d.F; n--;) delete o[i][r[n]];
        return o()
      };
    d.exports = Object.create || function (d, $) {
      var t;
      return null !== d ? (u[i] = n(d), t = new u, u[i] = null, t[l] = d) : t = o(), void 0 === $ ? t : e(t, $)
    }
  }, function (d, $, t) {
    var n = t(10),
      e = t(7),
      r = t(26);
    d.exports = t(3) ? Object.defineProperties : function (d, $) {
      e(d);
      for (var t, l = r($), u = l.length, i = 0; u > i;) n.f(d, t = l[i++], $[t]);
      return d
    }
  }, function (d, $) {
    $.f = Object.getOwnPropertySymbols
  }, function (d, $, t) {
    var n = t(8),
      e = t(30),
      r = t(18)("IE_PROTO"),
      l = Object.prototype;
    d.exports = Object.getPrototypeOf || function (d) {
      return d = e(d), n(d, r) ? d[r] : "function" == typeof d.constructor && d instanceof d.constructor ? d.constructor.prototype : d instanceof Object ? l : null
    }
  }, function (d, $, t) {
    var n = t(8),
      e = t(20),
      r = t(46)(!1),
      l = t(18)("IE_PROTO");
    d.exports = function (d, $) {
      var t, u = e(d),
        i = 0,
        o = [];
      for (t in u) t != l && n(u, t) && o.push(t);
      for (; $.length > i;) n(u, t = $[i++]) && (~r(o, t) || o.push(t));
      return o
    }
  }, function (d, $) {
    $.f = {}.propertyIsEnumerable
  }, function (d, $, t) {
    d.exports = t(4)
  }, function (d, $, t) {
    var n = t(19),
      e = t(14);
    d.exports = function (d) {
      return function ($, t) {
        var r, l, u = String(e($)),
          i = n(t),
          o = u.length;
        return i < 0 || i >= o ? d ? "" : void 0 : (r = u.charCodeAt(i), r < 55296 || r > 56319 || i + 1 === o || (l = u.charCodeAt(i + 1)) < 56320 || l > 57343 ? d ? u.charAt(i) : r : d ? u.slice(i, i + 2) : (r - 55296 << 10) + (l - 56320) + 65536)
      }
    }
  }, function (d, $, t) {
    var n = t(19),
      e = Math.max,
      r = Math.min;
    d.exports = function (d, $) {
      return d = n(d), d < 0 ? e(d + $, 0) : r(d, $)
    }
  }, function (d, $, t) {
    var n = t(19),
      e = Math.min;
    d.exports = function (d) {
      return d > 0 ? e(n(d), 9007199254740991) : 0
    }
  }, function (d, $, t) {
    var n = t(17);
    d.exports = function (d, $) {
      if (!n(d)) return d;
      var t, e;
      if ($ && "function" == typeof (t = d.toString) && !n(e = t.call(d))) return e;
      if ("function" == typeof (t = d.valueOf) && !n(e = t.call(d))) return e;
      if (!$ && "function" == typeof (t = d.toString) && !n(e = t.call(d))) return e;
      throw TypeError("Can't convert object to primitive value")
    }
  }, function (d, $, t) {
    var n = t(47),
      e = t(1)("iterator"),
      r = t(9);
    d.exports = t(2).getIteratorMethod = function (d) {
      if (void 0 != d) return d[e] || d["@@iterator"] || r[n(d)]
    }
  }, function (d, $, t) {
    var n = t(7),
      e = t(66);
    d.exports = t(2).getIterator = function (d) {
      var $ = e(d);
      if ("function" != typeof $) throw TypeError(d + " is not iterable!");
      return n($.call(d))
    }
  }, function (d, $, t) {
    "use strict";
    var n = t(45),
      e = t(52),
      r = t(9),
      l = t(20);
    d.exports = t(25)(Array, "Array", function (d, $) {
      this._t = l(d), this._i = 0, this._k = $
    }, function () {
      var d = this._t,
        $ = this._k,
        t = this._i++;
      return !d || t >= d.length ? (this._t = void 0, e(1)) : "keys" == $ ? e(0, t) : "values" == $ ? e(0, d[t]) : e(0, [t, d[t]])
    }, "values"), r.Arguments = r.Array, n("keys"), n("values"), n("entries")
  }, function (d, $, t) {
    var n = t(15);
    n(n.S + n.F, "Object", {
      assign: t(54)
    })
  }, function (d, $, t) {
    var n = t(15);
    n(n.S + n.F * !t(3), "Object", {
      defineProperty: t(10).f
    })
  }, function (d, $, t) {
    "use strict";
    var n = t(62)(!0);
    t(25)(String, "String", function (d) {
      this._t = String(d), this._i = 0
    }, function () {
      var d, $ = this._t,
        t = this._i;
      return t >= $.length ? {
        value: void 0,
        done: !0
      } : (d = n($, t), this._i += d.length, {
        value: d,
        done: !1
      })
    })
  }, function (d, $, t) {
    t(68);
    for (var n = t(0), e = t(4), r = t(9), l = t(1)("toStringTag"), u = ["NodeList", "DOMTokenList", "MediaList", "StyleSheetList", "CSSRuleList"], i = 0; i < 5; i++) {
      var o = u[i],
        a = n[o],
        _ = a && a.prototype;
      _ && !_[l] && e(_, l, o), r[o] = r.Array
    }
  }, function (d, $, t) {
    "use strict";

    function n(d) {
      return d && d.__esModule ? d : {
        default: d
      }
    }
    Object.defineProperty($, "__esModule", {
      value: !0
    }), $.asYouType = $.as_you_type = $.isValidNumber = $.is_valid_number = $.format = $.parse = void 0;
    var e = t(32),
      r = n(e),
      l = t(33),
      u = {
        metadata: r.default
      },
      i = ($.parse = l.parse.bind(u), $.format = l.format.bind(u), $.is_valid_number = l.is_valid_number.bind(u)),
      o = ($.isValidNumber = i, $.as_you_type = (0, l.as_you_type)(r.default));
    $.asYouType = o
  }])
});
//# sourceMappingURL=libphonenumber-js.min.js.map
/*!
 * Parsley.js
 * Version 2.8.1 - built Sat, Feb 3rd 2018, 2:27 pm
 * http://parsleyjs.org
 * Guillaume Potier - <guillaume@wisembly.com>
 * Marc-Andre Lafortune - <petroselinum@marc-andre.ca>
 * MIT Licensed
 */
function _toConsumableArray(e) {
  if (Array.isArray(e)) {
    for (var t = 0, i = Array(e.length); t < e.length; t++) i[t] = e[t];
    return i
  }
  return Array.from(e)
}
var _slice = Array.prototype.slice,
  _slicedToArray = function () {
    function e(e, t) {
      var i = [],
        n = !0,
        r = !1,
        s = void 0;
      try {
        for (var a, o = e[Symbol.iterator](); !(n = (a = o.next()).done) && (i.push(a.value), !t || i.length !== t); n = !0);
      } catch (l) {
        r = !0, s = l
      } finally {
        try {
          !n && o["return"] && o["return"]()
        } finally {
          if (r) throw s
        }
      }
      return i
    }
    return function (t, i) {
      if (Array.isArray(t)) return t;
      if (Symbol.iterator in Object(t)) return e(t, i);
      throw new TypeError("Invalid attempt to destructure non-iterable instance")
    }
  }(),
  _extends = Object.assign || function (e) {
    for (var t = 1; t < arguments.length; t++) {
      var i = arguments[t];
      for (var n in i) Object.prototype.hasOwnProperty.call(i, n) && (e[n] = i[n])
    }
    return e
  };
! function (e, t) {
  "object" == typeof exports && "undefined" != typeof module ? module.exports = t(require("jquery")) : "function" == typeof define && define.amd ? define(["jquery"], t) : e.parsley = t(e.jQuery)
}(this, function (e) {
  "use strict";

  function t(e, t) {
    return e.parsleyAdaptedCallback || (e.parsleyAdaptedCallback = function () {
      var i = Array.prototype.slice.call(arguments, 0);
      i.unshift(this), e.apply(t || M, i)
    }), e.parsleyAdaptedCallback
  }

  function i(e) {
    return 0 === e.lastIndexOf(D, 0) ? e.substr(D.length) : e
  }
  /**
   * inputevent - Alleviate browser bugs for input events
   * https://github.com/marcandre/inputevent
   * @version v0.0.3 - (built Thu, Apr 14th 2016, 5:58 pm)
   * @author Marc-Andre Lafortune <github@marc-andre.ca>
   * @license MIT
   */
  function n() {
    var t = this,
      i = window || global;
    _extends(this, {
      isNativeEvent: function (e) {
        return e.originalEvent && e.originalEvent.isTrusted !== !1
      },
      fakeInputEvent: function (i) {
        t.isNativeEvent(i) && e(i.target).trigger("input")
      },
      misbehaves: function (i) {
        t.isNativeEvent(i) && (t.behavesOk(i), e(document).on("change.inputevent", i.data.selector, t.fakeInputEvent), t.fakeInputEvent(i))
      },
      behavesOk: function (i) {
        t.isNativeEvent(i) && e(document).off("input.inputevent", i.data.selector, t.behavesOk).off("change.inputevent", i.data.selector, t.misbehaves)
      },
      install: function () {
        if (!i.inputEventPatched) {
          i.inputEventPatched = "0.0.3";
          for (var n = ["select", 'input[type="checkbox"]', 'input[type="radio"]', 'input[type="file"]'], r = 0; r < n.length; r++) {
            var s = n[r];
            e(document).on("input.inputevent", s, {
              selector: s
            }, t.behavesOk).on("change.inputevent", s, {
              selector: s
            }, t.misbehaves)
          }
        }
      },
      uninstall: function () {
        delete i.inputEventPatched, e(document).off(".inputevent")
      }
    })
  }
  var r = 1,
    s = {},
    a = {
      attr: function (e, t, i) {
        var n, r, s, a = new RegExp("^" + t, "i");
        if ("undefined" == typeof i) i = {};
        else
          for (n in i) i.hasOwnProperty(n) && delete i[n];
        if (!e) return i;
        for (s = e.attributes, n = s.length; n--;) r = s[n], r && r.specified && a.test(r.name) && (i[this.camelize(r.name.slice(t.length))] = this.deserializeValue(r.value));
        return i
      },
      checkAttr: function (e, t, i) {
        return e.hasAttribute(t + i)
      },
      setAttr: function (e, t, i, n) {
        e.setAttribute(this.dasherize(t + i), String(n))
      },
      getType: function (e) {
        return e.getAttribute("type") || "text"
      },
      generateID: function () {
        return "" + r++
      },
      deserializeValue: function (e) {
        var t;
        try {
          return e ? "true" == e || "false" != e && ("null" == e ? null : isNaN(t = Number(e)) ? /^[\[\{]/.test(e) ? JSON.parse(e) : e : t) : e
        } catch (i) {
          return e
        }
      },
      camelize: function (e) {
        return e.replace(/-+(.)?/g, function (e, t) {
          return t ? t.toUpperCase() : ""
        })
      },
      dasherize: function (e) {
        return e.replace(/::/g, "/").replace(/([A-Z]+)([A-Z][a-z])/g, "$1_$2").replace(/([a-z\d])([A-Z])/g, "$1_$2").replace(/_/g, "-").toLowerCase()
      },
      warn: function () {
        var e;
        window.console && "function" == typeof window.console.warn && (e = window.console).warn.apply(e, arguments)
      },
      warnOnce: function (e) {
        s[e] || (s[e] = !0, this.warn.apply(this, arguments))
      },
      _resetWarnings: function () {
        s = {}
      },
      trimString: function (e) {
        return e.replace(/^\s+|\s+$/g, "")
      },
      parse: {
        date: function S(e) {
          var t = e.match(/^(\d{4,})-(\d\d)-(\d\d)$/);
          if (!t) return null;
          var i = t.map(function (e) {
              return parseInt(e, 10)
            }),
            n = _slicedToArray(i, 4),
            r = (n[0], n[1]),
            s = n[2],
            a = n[3],
            S = new Date(r, s - 1, a);
          return S.getFullYear() !== r || S.getMonth() + 1 !== s || S.getDate() !== a ? null : S
        },
        string: function (e) {
          return e
        },
        integer: function (e) {
          return isNaN(e) ? null : parseInt(e, 10)
        },
        number: function (e) {
          if (isNaN(e)) throw null;
          return parseFloat(e)
        },
        "boolean": function (e) {
          return !/^\s*false\s*$/i.test(e)
        },
        object: function (e) {
          return a.deserializeValue(e)
        },
        regexp: function (e) {
          var t = "";
          return /^\/.*\/(?:[gimy]*)$/.test(e) ? (t = e.replace(/.*\/([gimy]*)$/, "$1"), e = e.replace(new RegExp("^/(.*?)/" + t + "$"), "$1")) : e = "^" + e + "$", new RegExp(e, t)
        }
      },
      parseRequirement: function (e, t) {
        var i = this.parse[e || "string"];
        if (!i) throw 'Unknown requirement specification: "' + e + '"';
        var n = i(t);
        if (null === n) throw "Requirement is not a " + e + ': "' + t + '"';
        return n
      },
      namespaceEvents: function (t, i) {
        return t = this.trimString(t || "").split(/\s+/), t[0] ? e.map(t, function (e) {
          return e + "." + i
        }).join(" ") : ""
      },
      difference: function (t, i) {
        var n = [];
        return e.each(t, function (e, t) {
          i.indexOf(t) == -1 && n.push(t)
        }), n
      },
      all: function (t) {
        return e.when.apply(e, _toConsumableArray(t).concat([42, 42]))
      },
      objectCreate: Object.create || function () {
        var e = function () {};
        return function (t) {
          if (arguments.length > 1) throw Error("Second argument not supported");
          if ("object" != typeof t) throw TypeError("Argument must be an object");
          e.prototype = t;
          var i = new e;
          return e.prototype = null, i
        }
      }(),
      _SubmitSelector: 'input[type="submit"], button:submit'
    },
    o = {
      namespace: "data-parsley-",
      inputs: "input, textarea, select",
      excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden]",
      priorityEnabled: !0,
      multiple: null,
      group: null,
      uiEnabled: !0,
      validationThreshold: 3,
      focus: "first",
      trigger: !1,
      triggerAfterFailure: "input",
      errorClass: "parsley-error",
      successClass: "parsley-success",
      classHandler: function (e) {},
      errorsContainer: function (e) {},
      errorsWrapper: '<ul class="parsley-errors-list"></ul>',
      errorTemplate: "<li></li>"
    },
    l = function () {
      this.__id__ = a.generateID()
    };
  l.prototype = {
    asyncSupport: !0,
    _pipeAccordingToValidationResult: function () {
      var t = this,
        i = function () {
          var i = e.Deferred();
          return !0 !== t.validationResult && i.reject(), i.resolve().promise()
        };
      return [i, i]
    },
    actualizeOptions: function () {
      return a.attr(this.element, this.options.namespace, this.domOptions), this.parent && this.parent.actualizeOptions && this.parent.actualizeOptions(), this
    },
    _resetOptions: function (e) {
      this.domOptions = a.objectCreate(this.parent.options), this.options = a.objectCreate(this.domOptions);
      for (var t in e) e.hasOwnProperty(t) && (this.options[t] = e[t]);
      this.actualizeOptions()
    },
    _listeners: null,
    on: function (e, t) {
      this._listeners = this._listeners || {};
      var i = this._listeners[e] = this._listeners[e] || [];
      return i.push(t), this
    },
    subscribe: function (t, i) {
      e.listenTo(this, t.toLowerCase(), i)
    },
    off: function (e, t) {
      var i = this._listeners && this._listeners[e];
      if (i)
        if (t)
          for (var n = i.length; n--;) i[n] === t && i.splice(n, 1);
        else delete this._listeners[e];
      return this
    },
    unsubscribe: function (t, i) {
      e.unsubscribeTo(this, t.toLowerCase())
    },
    trigger: function (e, t, i) {
      t = t || this;
      var n, r = this._listeners && this._listeners[e];
      if (r)
        for (var s = r.length; s--;)
          if (n = r[s].call(t, t, i), n === !1) return n;
      return !this.parent || this.parent.trigger(e, t, i)
    },
    asyncIsValid: function (e, t) {
      return a.warnOnce("asyncIsValid is deprecated; please use whenValid instead"), this.whenValid({
        group: e,
        force: t
      })
    },
    _findRelated: function () {
      return this.options.multiple ? e(this.parent.element.querySelectorAll("[" + this.options.namespace + 'multiple="' + this.options.multiple + '"]')) : this.$element
    }
  };
  var u = function (e, t) {
      var i = e.match(/^\s*\[(.*)\]\s*$/);
      if (!i) throw 'Requirement is not an array: "' + e + '"';
      var n = i[1].split(",").map(a.trimString);
      if (n.length !== t) throw "Requirement has " + n.length + " values when " + t + " are needed";
      return n
    },
    d = function (e, t, i) {
      var n = null,
        r = {};
      for (var s in e)
        if (s) {
          var o = i(s);
          "string" == typeof o && (o = a.parseRequirement(e[s], o)), r[s] = o
        } else n = a.parseRequirement(e[s], t);
      return [n, r]
    },
    h = function (t) {
      e.extend(!0, this, t)
    };
  h.prototype = {
    validate: function (e, t) {
      if (this.fn) return arguments.length > 3 && (t = [].slice.call(arguments, 1, -1)), this.fn(e, t);
      if (Array.isArray(e)) {
        if (!this.validateMultiple) throw "Validator `" + this.name + "` does not handle multiple values";
        return this.validateMultiple.apply(this, arguments)
      }
      var i = arguments[arguments.length - 1];
      if (this.validateDate && i._isDateInput()) return arguments[0] = a.parse.date(arguments[0]), null !== arguments[0] && this.validateDate.apply(this, arguments);
      if (this.validateNumber) return !isNaN(e) && (arguments[0] = parseFloat(arguments[0]), this.validateNumber.apply(this, arguments));
      if (this.validateString) return this.validateString.apply(this, arguments);
      throw "Validator `" + this.name + "` only handles multiple values"
    },
    parseRequirements: function (t, i) {
      if ("string" != typeof t) return Array.isArray(t) ? t : [t];
      var n = this.requirementType;
      if (Array.isArray(n)) {
        for (var r = u(t, n.length), s = 0; s < r.length; s++) r[s] = a.parseRequirement(n[s], r[s]);
        return r
      }
      return e.isPlainObject(n) ? d(n, t, i) : [a.parseRequirement(n, t)]
    },
    requirementType: "string",
    priority: 2
  };
  var p = function (e, t) {
      this.__class__ = "ValidatorRegistry", this.locale = "en", this.init(e || {}, t || {})
    },
    c = {
      email: /^((([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/,
      number: /^-?(\d*\.)?\d+(e[-+]?\d+)?$/i,
      integer: /^-?\d+$/,
      digits: /^\d+$/,
      alphanum: /^\w+$/i,
      date: {
        test: function (e) {
          return null !== a.parse.date(e)
        }
      },
      url: new RegExp("^(?:(?:https?|ftp)://)?(?:\\S+(?::\\S*)?@)?(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-zA-Z\\u00a1-\\uffff0-9]-*)*[a-zA-Z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-zA-Z\\u00a1-\\uffff0-9]-*)*[a-zA-Z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-zA-Z\\u00a1-\\uffff]{2,})))(?::\\d{2,5})?(?:/\\S*)?$")
    };
  c.range = c.number;
  var f = function (e) {
      var t = ("" + e).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
      return t ? Math.max(0, (t[1] ? t[1].length : 0) - (t[2] ? +t[2] : 0)) : 0
    },
    m = function (e, t) {
      return t.map(a.parse[e])
    },
    g = function (e, t) {
      return function (i) {
        for (var n = arguments.length, r = Array(n > 1 ? n - 1 : 0), s = 1; s < n; s++) r[s - 1] = arguments[s];
        return r.pop(), t.apply(void 0, [i].concat(_toConsumableArray(m(e, r))))
      }
    },
    v = function (e) {
      return {
        validateDate: g("date", e),
        validateNumber: g("number", e),
        requirementType: e.length <= 2 ? "string" : ["string", "string"],
        priority: 30
      }
    };
  p.prototype = {
    init: function (e, t) {
      this.catalog = t, this.validators = _extends({}, this.validators);
      for (var i in e) this.addValidator(i, e[i].fn, e[i].priority);
      window.Parsley.trigger("parsley:validator:init")
    },
    setLocale: function (e) {
      if ("undefined" == typeof this.catalog[e]) throw new Error(e + " is not available in the catalog");
      return this.locale = e, this
    },
    addCatalog: function (e, t, i) {
      return "object" == typeof t && (this.catalog[e] = t), !0 === i ? this.setLocale(e) : this
    },
    addMessage: function (e, t, i) {
      return "undefined" == typeof this.catalog[e] && (this.catalog[e] = {}), this.catalog[e][t] = i, this
    },
    addMessages: function (e, t) {
      for (var i in t) this.addMessage(e, i, t[i]);
      return this
    },
    addValidator: function (e, t, i) {
      if (this.validators[e]) a.warn('Validator "' + e + '" is already defined.');
      else if (o.hasOwnProperty(e)) return void a.warn('"' + e + '" is a restricted keyword and is not a valid validator name.');
      return this._setValidator.apply(this, arguments)
    },
    hasValidator: function (e) {
      return !!this.validators[e]
    },
    updateValidator: function (e, t, i) {
      return this.validators[e] ? this._setValidator.apply(this, arguments) : (a.warn('Validator "' + e + '" is not already defined.'), this.addValidator.apply(this, arguments))
    },
    removeValidator: function (e) {
      return this.validators[e] || a.warn('Validator "' + e + '" is not defined.'), delete this.validators[e], this
    },
    _setValidator: function (e, t, i) {
      "object" != typeof t && (t = {
        fn: t,
        priority: i
      }), t.validate || (t = new h(t)), this.validators[e] = t;
      for (var n in t.messages || {}) this.addMessage(n, e, t.messages[n]);
      return this
    },
    getErrorMessage: function (e) {
      var t;
      if ("type" === e.name) {
        var i = this.catalog[this.locale][e.name] || {};
        t = i[e.requirements]
      } else t = this.formatMessage(this.catalog[this.locale][e.name], e.requirements);
      return t || this.catalog[this.locale].defaultMessage || this.catalog.en.defaultMessage
    },
    formatMessage: function (e, t) {
      if ("object" == typeof t) {
        for (var i in t) e = this.formatMessage(e, t[i]);
        return e
      }
      return "string" == typeof e ? e.replace(/%s/i, t) : ""
    },
    validators: {
      notblank: {
        validateString: function (e) {
          return /\S/.test(e)
        },
        priority: 2
      },
      required: {
        validateMultiple: function (e) {
          return e.length > 0
        },
        validateString: function (e) {
          return /\S/.test(e)
        },
        priority: 512
      },
      type: {
        validateString: function (e, t) {
          var i = arguments.length <= 2 || void 0 === arguments[2] ? {} : arguments[2],
            n = i.step,
            r = void 0 === n ? "any" : n,
            s = i.base,
            a = void 0 === s ? 0 : s,
            o = c[t];
          if (!o) throw new Error("validator type `" + t + "` is not supported");
          if (!o.test(e)) return !1;
          if ("number" === t && !/^any$/i.test(r || "")) {
            var l = Number(e),
              u = Math.max(f(r), f(a));
            if (f(l) > u) return !1;
            var d = function (e) {
              return Math.round(e * Math.pow(10, u))
            };
            if ((d(l) - d(a)) % d(r) != 0) return !1
          }
          return !0
        },
        requirementType: {
          "": "string",
          step: "string",
          base: "number"
        },
        priority: 256
      },
      pattern: {
        validateString: function (e, t) {
          return t.test(e)
        },
        requirementType: "regexp",
        priority: 64
      },
      minlength: {
        validateString: function (e, t) {
          return e.length >= t
        },
        requirementType: "integer",
        priority: 30
      },
      maxlength: {
        validateString: function (e, t) {
          return e.length <= t
        },
        requirementType: "integer",
        priority: 30
      },
      length: {
        validateString: function (e, t, i) {
          return e.length >= t && e.length <= i
        },
        requirementType: ["integer", "integer"],
        priority: 30
      },
      mincheck: {
        validateMultiple: function (e, t) {
          return e.length >= t
        },
        requirementType: "integer",
        priority: 30
      },
      maxcheck: {
        validateMultiple: function (e, t) {
          return e.length <= t
        },
        requirementType: "integer",
        priority: 30
      },
      check: {
        validateMultiple: function (e, t, i) {
          return e.length >= t && e.length <= i
        },
        requirementType: ["integer", "integer"],
        priority: 30
      },
      min: v(function (e, t) {
        return e >= t
      }),
      max: v(function (e, t) {
        return e <= t
      }),
      range: v(function (e, t, i) {
        return e >= t && e <= i
      }),
      equalto: {
        validateString: function (t, i) {
          var n = e(i);
          return n.length ? t === n.val() : t === i
        },
        priority: 256
      }
    }
  };
  var y = {},
    _ = function k(e, t, i) {
      for (var n = [], r = [], s = 0; s < e.length; s++) {
        for (var a = !1, o = 0; o < t.length; o++)
          if (e[s].assert.name === t[o].assert.name) {
            a = !0;
            break
          } a ? r.push(e[s]) : n.push(e[s])
      }
      return {
        kept: r,
        added: n,
        removed: i ? [] : k(t, e, !0).added
      }
    };
  y.Form = {
    _actualizeTriggers: function () {
      var e = this;
      this.$element.on("submit.Parsley", function (t) {
        e.onSubmitValidate(t)
      }), this.$element.on("click.Parsley", a._SubmitSelector, function (t) {
        e.onSubmitButton(t)
      }), !1 !== this.options.uiEnabled && this.element.setAttribute("novalidate", "")
    },
    focus: function () {
      if (this._focusedField = null, !0 === this.validationResult || "none" === this.options.focus) return null;
      for (var e = 0; e < this.fields.length; e++) {
        var t = this.fields[e];
        if (!0 !== t.validationResult && t.validationResult.length > 0 && "undefined" == typeof t.options.noFocus && (this._focusedField = t.$element, "first" === this.options.focus)) break
      }
      return null === this._focusedField ? null : this._focusedField.focus()
    },
    _destroyUI: function () {
      this.$element.off(".Parsley")
    }
  }, y.Field = {
    _reflowUI: function () {
      if (this._buildUI(), this._ui) {
        var e = _(this.validationResult, this._ui.lastValidationResult);
        this._ui.lastValidationResult = this.validationResult, this._manageStatusClass(), this._manageErrorsMessages(e), this._actualizeTriggers(), !e.kept.length && !e.added.length || this._failedOnce || (this._failedOnce = !0, this._actualizeTriggers())
      }
    },
    getErrorsMessages: function () {
      if (!0 === this.validationResult) return [];
      for (var e = [], t = 0; t < this.validationResult.length; t++) e.push(this.validationResult[t].errorMessage || this._getErrorMessage(this.validationResult[t].assert));
      return e
    },
    addError: function (e) {
      var t = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1],
        i = t.message,
        n = t.assert,
        r = t.updateClass,
        s = void 0 === r || r;
      this._buildUI(), this._addError(e, {
        message: i,
        assert: n
      }), s && this._errorClass()
    },
    updateError: function (e) {
      var t = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1],
        i = t.message,
        n = t.assert,
        r = t.updateClass,
        s = void 0 === r || r;
      this._buildUI(), this._updateError(e, {
        message: i,
        assert: n
      }), s && this._errorClass()
    },
    removeError: function (e) {
      var t = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1],
        i = t.updateClass,
        n = void 0 === i || i;
      this._buildUI(), this._removeError(e), n && this._manageStatusClass()
    },
    _manageStatusClass: function () {
      this.hasConstraints() && this.needsValidation() && !0 === this.validationResult ? this._successClass() : this.validationResult.length > 0 ? this._errorClass() : this._resetClass()
    },
    _manageErrorsMessages: function (t) {
      if ("undefined" == typeof this.options.errorsMessagesDisabled) {
        if ("undefined" != typeof this.options.errorMessage) return t.added.length || t.kept.length ? (this._insertErrorWrapper(), 0 === this._ui.$errorsWrapper.find(".parsley-custom-error-message").length && this._ui.$errorsWrapper.append(e(this.options.errorTemplate).addClass("parsley-custom-error-message")), this._ui.$errorsWrapper.addClass("filled").find(".parsley-custom-error-message").html(this.options.errorMessage)) : this._ui.$errorsWrapper.removeClass("filled").find(".parsley-custom-error-message").remove();
        for (var i = 0; i < t.removed.length; i++) this._removeError(t.removed[i].assert.name);
        for (i = 0; i < t.added.length; i++) this._addError(t.added[i].assert.name, {
          message: t.added[i].errorMessage,
          assert: t.added[i].assert
        });
        for (i = 0; i < t.kept.length; i++) this._updateError(t.kept[i].assert.name, {
          message: t.kept[i].errorMessage,
          assert: t.kept[i].assert
        })
      }
    },
    _addError: function (t, i) {
      var n = i.message,
        r = i.assert;
      this._insertErrorWrapper(), this._ui.$errorClassHandler.attr("aria-describedby", this._ui.errorsWrapperId), this._ui.$errorsWrapper.addClass("filled").append(e(this.options.errorTemplate).addClass("parsley-" + t).html(n || this._getErrorMessage(r)))
    },
    _updateError: function (e, t) {
      var i = t.message,
        n = t.assert;
      this._ui.$errorsWrapper.addClass("filled").find(".parsley-" + e).html(i || this._getErrorMessage(n))
    },
    _removeError: function (e) {
      this._ui.$errorClassHandler.removeAttr("aria-describedby"), this._ui.$errorsWrapper.removeClass("filled").find(".parsley-" + e).remove()
    },
    _getErrorMessage: function (e) {
      var t = e.name + "Message";
      return "undefined" != typeof this.options[t] ? window.Parsley.formatMessage(this.options[t], e.requirements) : window.Parsley.getErrorMessage(e)
    },
    _buildUI: function () {
      if (!this._ui && !1 !== this.options.uiEnabled) {
        var t = {};
        this.element.setAttribute(this.options.namespace + "id", this.__id__), t.$errorClassHandler = this._manageClassHandler(), t.errorsWrapperId = "parsley-id-" + (this.options.multiple ? "multiple-" + this.options.multiple : this.__id__), t.$errorsWrapper = e(this.options.errorsWrapper).attr("id", t.errorsWrapperId), t.lastValidationResult = [], t.validationInformationVisible = !1, this._ui = t
      }
    },
    _manageClassHandler: function () {
      if ("string" == typeof this.options.classHandler && e(this.options.classHandler).length) return e(this.options.classHandler);
      var t = this.options.classHandler;
      if ("string" == typeof this.options.classHandler && "function" == typeof window[this.options.classHandler] && (t = window[this.options.classHandler]), "function" == typeof t) {
        var i = t.call(this, this);
        if ("undefined" != typeof i && i.length) return i
      } else {
        if ("object" == typeof t && t instanceof jQuery && t.length) return t;
        t && a.warn("The class handler `" + t + "` does not exist in DOM nor as a global JS function")
      }
      return this._inputHolder()
    },
    _inputHolder: function () {
      return this.options.multiple && "SELECT" !== this.element.nodeName ? this.$element.parent() : this.$element
    },
    _insertErrorWrapper: function () {
      var t = this.options.errorsContainer;
      if (0 !== this._ui.$errorsWrapper.parent().length) return this._ui.$errorsWrapper.parent();
      if ("string" == typeof t) {
        if (e(t).length) return e(t).append(this._ui.$errorsWrapper);
        "function" == typeof window[t] ? t = window[t] : a.warn("The errors container `" + t + "` does not exist in DOM nor as a global JS function")
      }
      return "function" == typeof t && (t = t.call(this, this)), "object" == typeof t && t.length ? t.append(this._ui.$errorsWrapper) : this._inputHolder().after(this._ui.$errorsWrapper)
    },
    _actualizeTriggers: function () {
      var e, t = this,
        i = this._findRelated();
      i.off(".Parsley"), this._failedOnce ? i.on(a.namespaceEvents(this.options.triggerAfterFailure, "Parsley"), function () {
        t._validateIfNeeded()
      }) : (e = a.namespaceEvents(this.options.trigger, "Parsley")) && i.on(e, function (e) {
        t._validateIfNeeded(e)
      })
    },
    _validateIfNeeded: function (e) {
      var t = this;
      e && /key|input/.test(e.type) && (!this._ui || !this._ui.validationInformationVisible) && this.getValue().length <= this.options.validationThreshold || (this.options.debounce ? (window.clearTimeout(this._debounced), this._debounced = window.setTimeout(function () {
        return t.validate()
      }, this.options.debounce)) : this.validate())
    },
    _resetUI: function () {
      this._failedOnce = !1, this._actualizeTriggers(), "undefined" != typeof this._ui && (this._ui.$errorsWrapper.removeClass("filled").children().remove(), this._resetClass(), this._ui.lastValidationResult = [], this._ui.validationInformationVisible = !1)
    },
    _destroyUI: function () {
      this._resetUI(), "undefined" != typeof this._ui && this._ui.$errorsWrapper.remove(), delete this._ui
    },
    _successClass: function () {
      this._ui.validationInformationVisible = !0, this._ui.$errorClassHandler.removeClass(this.options.errorClass).addClass(this.options.successClass)
    },
    _errorClass: function () {
      this._ui.validationInformationVisible = !0, this._ui.$errorClassHandler.removeClass(this.options.successClass).addClass(this.options.errorClass)
    },
    _resetClass: function () {
      this._ui.$errorClassHandler.removeClass(this.options.successClass).removeClass(this.options.errorClass)
    }
  };
  var w = function (t, i, n) {
      this.__class__ = "Form", this.element = t, this.$element = e(t), this.domOptions = i, this.options = n, this.parent = window.Parsley, this.fields = [], this.validationResult = null
    },
    b = {
      pending: null,
      resolved: !0,
      rejected: !1
    };
  w.prototype = {
    onSubmitValidate: function (e) {
      var t = this;
      if (!0 !== e.parsley) {
        var i = this._submitSource || this.$element.find(a._SubmitSelector)[0];
        if (this._submitSource = null, this.$element.find(".parsley-synthetic-submit-button").prop("disabled", !0), !i || null === i.getAttribute("formnovalidate")) {
          window.Parsley._remoteCache = {};
          var n = this.whenValidate({
            event: e
          });
          "resolved" === n.state() && !1 !== this._trigger("submit") || (e.stopImmediatePropagation(), e.preventDefault(), "pending" === n.state() && n.done(function () {
            t._submit(i)
          }))
        }
      }
    },
    onSubmitButton: function (e) {
      this._submitSource = e.currentTarget
    },
    _submit: function (t) {
      if (!1 !== this._trigger("submit")) {
        if (t) {
          var i = this.$element.find(".parsley-synthetic-submit-button").prop("disabled", !1);
          0 === i.length && (i = e('<input class="parsley-synthetic-submit-button" type="hidden">').appendTo(this.$element)), i.attr({
            name: t.getAttribute("name"),
            value: t.getAttribute("value")
          })
        }
        this.$element.trigger(_extends(e.Event("submit"), {
          parsley: !0
        }))
      }
    },
    validate: function (t) {
      if (arguments.length >= 1 && !e.isPlainObject(t)) {
        a.warnOnce("Calling validate on a parsley form without passing arguments as an object is deprecated.");
        var i = _slice.call(arguments),
          n = i[0],
          r = i[1],
          s = i[2];
        t = {
          group: n,
          force: r,
          event: s
        }
      }
      return b[this.whenValidate(t).state()]
    },
    whenValidate: function () {
      var t, i = this,
        n = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0],
        r = n.group,
        s = n.force,
        o = n.event;
      this.submitEvent = o, o && (this.submitEvent = _extends({}, o, {
        preventDefault: function () {
          a.warnOnce("Using `this.submitEvent.preventDefault()` is deprecated; instead, call `this.validationResult = false`"), i.validationResult = !1
        }
      })), this.validationResult = !0, this._trigger("validate"), this._refreshFields();
      var l = this._withoutReactualizingFormOptions(function () {
        return e.map(i.fields, function (e) {
          return e.whenValidate({
            force: s,
            group: r
          })
        })
      });
      return (t = a.all(l).done(function () {
        i._trigger("success")
      }).fail(function () {
        i.validationResult = !1, i.focus(), i._trigger("error")
      }).always(function () {
        i._trigger("validated")
      })).pipe.apply(t, _toConsumableArray(this._pipeAccordingToValidationResult()))
    },
    isValid: function (t) {
      if (arguments.length >= 1 && !e.isPlainObject(t)) {
        a.warnOnce("Calling isValid on a parsley form without passing arguments as an object is deprecated.");
        var i = _slice.call(arguments),
          n = i[0],
          r = i[1];
        t = {
          group: n,
          force: r
        }
      }
      return b[this.whenValid(t).state()]
    },
    whenValid: function () {
      var t = this,
        i = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0],
        n = i.group,
        r = i.force;
      this._refreshFields();
      var s = this._withoutReactualizingFormOptions(function () {
        return e.map(t.fields, function (e) {
          return e.whenValid({
            group: n,
            force: r
          })
        })
      });
      return a.all(s)
    },
    refresh: function () {
      return this._refreshFields(), this
    },
    reset: function () {
      for (var e = 0; e < this.fields.length; e++) this.fields[e].reset();
      this._trigger("reset")
    },
    destroy: function () {
      this._destroyUI();
      for (var e = 0; e < this.fields.length; e++) this.fields[e].destroy();
      this.$element.removeData("Parsley"), this._trigger("destroy")
    },
    _refreshFields: function () {
      return this.actualizeOptions()._bindFields()
    },
    _bindFields: function () {
      var t = this,
        i = this.fields;
      return this.fields = [], this.fieldsMappedById = {}, this._withoutReactualizingFormOptions(function () {
        t.$element.find(t.options.inputs).not(t.options.excluded).each(function (e, i) {
          var n = new window.Parsley.Factory(i, {}, t);
          if (("Field" === n.__class__ || "FieldMultiple" === n.__class__) && !0 !== n.options.excluded) {
            var r = n.__class__ + "-" + n.__id__;
            "undefined" == typeof t.fieldsMappedById[r] && (t.fieldsMappedById[r] = n, t.fields.push(n))
          }
        }), e.each(a.difference(i, t.fields), function (e, t) {
          t.reset()
        })
      }), this
    },
    _withoutReactualizingFormOptions: function (e) {
      var t = this.actualizeOptions;
      this.actualizeOptions = function () {
        return this
      };
      var i = e();
      return this.actualizeOptions = t, i
    },
    _trigger: function (e) {
      return this.trigger("form:" + e)
    }
  };
  var F = function (e, t, i, n, r) {
      var s = window.Parsley._validatorRegistry.validators[t],
        a = new h(s);
      n = n || e.options[t + "Priority"] || a.priority, r = !0 === r, _extends(this, {
        validator: a,
        name: t,
        requirements: i,
        priority: n,
        isDomConstraint: r
      }), this._parseRequirements(e.options)
    },
    C = function (e) {
      var t = e[0].toUpperCase();
      return t + e.slice(1)
    };
  F.prototype = {
    validate: function (e, t) {
      var i;
      return (i = this.validator).validate.apply(i, [e].concat(_toConsumableArray(this.requirementList), [t]))
    },
    _parseRequirements: function (e) {
      var t = this;
      this.requirementList = this.validator.parseRequirements(this.requirements, function (i) {
        return e[t.name + C(i)]
      })
    }
  };
  var A = function (t, i, n, r) {
      this.__class__ = "Field", this.element = t, this.$element = e(t), "undefined" != typeof r && (this.parent = r), this.options = n, this.domOptions = i, this.constraints = [], this.constraintsByName = {}, this.validationResult = !0, this._bindConstraints()
    },
    E = {
      pending: null,
      resolved: !0,
      rejected: !1
    };
  A.prototype = {
    validate: function (t) {
      arguments.length >= 1 && !e.isPlainObject(t) && (a.warnOnce("Calling validate on a parsley field without passing arguments as an object is deprecated."), t = {
        options: t
      });
      var i = this.whenValidate(t);
      if (!i) return !0;
      switch (i.state()) {
        case "pending":
          return null;
        case "resolved":
          return !0;
        case "rejected":
          return this.validationResult
      }
    },
    whenValidate: function () {
      var e, t = this,
        i = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0],
        n = i.force,
        r = i.group;
      if (this.refresh(), !r || this._isInGroup(r)) return this.value = this.getValue(), this._trigger("validate"), (e = this.whenValid({
        force: n,
        value: this.value,
        _refreshed: !0
      }).always(function () {
        t._reflowUI()
      }).done(function () {
        t._trigger("success")
      }).fail(function () {
        t._trigger("error")
      }).always(function () {
        t._trigger("validated")
      })).pipe.apply(e, _toConsumableArray(this._pipeAccordingToValidationResult()))
    },
    hasConstraints: function () {
      return 0 !== this.constraints.length
    },
    needsValidation: function (e) {
      return "undefined" == typeof e && (e = this.getValue()), !(!e.length && !this._isRequired() && "undefined" == typeof this.options.validateIfEmpty)
    },
    _isInGroup: function (t) {
      return Array.isArray(this.options.group) ? -1 !== e.inArray(t, this.options.group) : this.options.group === t
    },
    isValid: function (t) {
      if (arguments.length >= 1 && !e.isPlainObject(t)) {
        a.warnOnce("Calling isValid on a parsley field without passing arguments as an object is deprecated.");
        var i = _slice.call(arguments),
          n = i[0],
          r = i[1];
        t = {
          force: n,
          value: r
        }
      }
      var s = this.whenValid(t);
      return !s || E[s.state()]
    },
    whenValid: function () {
      var t = this,
        i = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0],
        n = i.force,
        r = void 0 !== n && n,
        s = i.value,
        o = i.group,
        l = i._refreshed;
      if (l || this.refresh(), !o || this._isInGroup(o)) {
        if (this.validationResult = !0, !this.hasConstraints()) return e.when();
        if ("undefined" != typeof s && null !== s || (s = this.getValue()), !this.needsValidation(s) && !0 !== r) return e.when();
        var u = this._getGroupedConstraints(),
          d = [];
        return e.each(u, function (i, n) {
          var r = a.all(e.map(n, function (e) {
            return t._validateConstraint(s, e)
          }));
          if (d.push(r), "rejected" === r.state()) return !1
        }), a.all(d)
      }
    },
    _validateConstraint: function (t, i) {
      var n = this,
        r = i.validate(t, this);
      return !1 === r && (r = e.Deferred().reject()), a.all([r]).fail(function (e) {
        n.validationResult instanceof Array || (n.validationResult = []), n.validationResult.push({
          assert: i,
          errorMessage: "string" == typeof e && e
        })
      })
    },
    getValue: function () {
      var e;
      return e = "function" == typeof this.options.value ? this.options.value(this) : "undefined" != typeof this.options.value ? this.options.value : this.$element.val(), "undefined" == typeof e || null === e ? "" : this._handleWhitespace(e)
    },
    reset: function () {
      return this._resetUI(), this._trigger("reset")
    },
    destroy: function () {
      this._destroyUI(), this.$element.removeData("Parsley"), this.$element.removeData("FieldMultiple"), this._trigger("destroy")
    },
    refresh: function () {
      return this._refreshConstraints(), this
    },
    _refreshConstraints: function () {
      return this.actualizeOptions()._bindConstraints()
    },
    refreshConstraints: function () {
      return a.warnOnce("Parsley's refreshConstraints is deprecated. Please use refresh"), this.refresh()
    },
    addConstraint: function (e, t, i, n) {
      if (window.Parsley._validatorRegistry.validators[e]) {
        var r = new F(this, e, t, i, n);
        "undefined" !== this.constraintsByName[r.name] && this.removeConstraint(r.name), this.constraints.push(r), this.constraintsByName[r.name] = r
      }
      return this
    },
    removeConstraint: function (e) {
      for (var t = 0; t < this.constraints.length; t++)
        if (e === this.constraints[t].name) {
          this.constraints.splice(t, 1);
          break
        } return delete this.constraintsByName[e], this
    },
    updateConstraint: function (e, t, i) {
      return this.removeConstraint(e).addConstraint(e, t, i)
    },
    _bindConstraints: function () {
      for (var e = [], t = {}, i = 0; i < this.constraints.length; i++) !1 === this.constraints[i].isDomConstraint && (e.push(this.constraints[i]), t[this.constraints[i].name] = this.constraints[i]);
      this.constraints = e, this.constraintsByName = t;
      for (var n in this.options) this.addConstraint(n, this.options[n], void 0, !0);
      return this._bindHtml5Constraints()
    },
    _bindHtml5Constraints: function () {
      null !== this.element.getAttribute("required") && this.addConstraint("required", !0, void 0, !0), null !== this.element.getAttribute("pattern") && this.addConstraint("pattern", this.element.getAttribute("pattern"), void 0, !0);
      var e = this.element.getAttribute("min"),
        t = this.element.getAttribute("max");
      null !== e && null !== t ? this.addConstraint("range", [e, t], void 0, !0) : null !== e ? this.addConstraint("min", e, void 0, !0) : null !== t && this.addConstraint("max", t, void 0, !0), null !== this.element.getAttribute("minlength") && null !== this.element.getAttribute("maxlength") ? this.addConstraint("length", [this.element.getAttribute("minlength"), this.element.getAttribute("maxlength")], void 0, !0) : null !== this.element.getAttribute("minlength") ? this.addConstraint("minlength", this.element.getAttribute("minlength"), void 0, !0) : null !== this.element.getAttribute("maxlength") && this.addConstraint("maxlength", this.element.getAttribute("maxlength"), void 0, !0);
      var i = a.getType(this.element);
      return "number" === i ? this.addConstraint("type", ["number", {
        step: this.element.getAttribute("step") || "1",
        base: e || this.element.getAttribute("value")
      }], void 0, !0) : /^(email|url|range|date)$/i.test(i) ? this.addConstraint("type", i, void 0, !0) : this
    },
    _isRequired: function () {
      return "undefined" != typeof this.constraintsByName.required && !1 !== this.constraintsByName.required.requirements
    },
    _trigger: function (e) {
      return this.trigger("field:" + e)
    },
    _handleWhitespace: function (e) {
      return !0 === this.options.trimValue && a.warnOnce('data-parsley-trim-value="true" is deprecated, please use data-parsley-whitespace="trim"'), "squish" === this.options.whitespace && (e = e.replace(/\s{2,}/g, " ")), "trim" !== this.options.whitespace && "squish" !== this.options.whitespace && !0 !== this.options.trimValue || (e = a.trimString(e)), e
    },
    _isDateInput: function () {
      var e = this.constraintsByName.type;
      return e && "date" === e.requirements
    },
    _getGroupedConstraints: function () {
      if (!1 === this.options.priorityEnabled) return [this.constraints];
      for (var e = [], t = {}, i = 0; i < this.constraints.length; i++) {
        var n = this.constraints[i].priority;
        t[n] || e.push(t[n] = []), t[n].push(this.constraints[i])
      }
      return e.sort(function (e, t) {
        return t[0].priority - e[0].priority
      }), e
    }
  };
  var x = A,
    $ = function () {
      this.__class__ = "FieldMultiple"
    };
  $.prototype = {
    addElement: function (e) {
      return this.$elements.push(e), this
    },
    _refreshConstraints: function () {
      var t;
      if (this.constraints = [], "SELECT" === this.element.nodeName) return this.actualizeOptions()._bindConstraints(), this;
      for (var i = 0; i < this.$elements.length; i++)
        if (e("html").has(this.$elements[i]).length) {
          t = this.$elements[i].data("FieldMultiple")._refreshConstraints().constraints;
          for (var n = 0; n < t.length; n++) this.addConstraint(t[n].name, t[n].requirements, t[n].priority, t[n].isDomConstraint)
        } else this.$elements.splice(i, 1);
      return this
    },
    getValue: function () {
      if ("function" == typeof this.options.value) return this.options.value(this);
      if ("undefined" != typeof this.options.value) return this.options.value;
      if ("INPUT" === this.element.nodeName) {
        var t = a.getType(this.element);
        if ("radio" === t) return this._findRelated().filter(":checked").val() || "";
        if ("checkbox" === t) {
          var i = [];
          return this._findRelated().filter(":checked").each(function () {
            i.push(e(this).val())
          }), i
        }
      }
      return "SELECT" === this.element.nodeName && null === this.$element.val() ? [] : this.$element.val()
    },
    _init: function () {
      return this.$elements = [this.$element], this
    }
  };
  var P = function (t, i, n) {
    this.element = t, this.$element = e(t);
    var r = this.$element.data("Parsley");
    if (r) return "undefined" != typeof n && r.parent === window.Parsley && (r.parent = n, r._resetOptions(r.options)), "object" == typeof i && _extends(r.options, i), r;
    if (!this.$element.length) throw new Error("You must bind Parsley on an existing element.");
    if ("undefined" != typeof n && "Form" !== n.__class__) throw new Error("Parent instance must be a Form instance");
    return this.parent = n || window.Parsley, this.init(i)
  };
  P.prototype = {
    init: function (e) {
      return this.__class__ = "Parsley", this.__version__ = "2.8.1", this.__id__ = a.generateID(), this._resetOptions(e), "FORM" === this.element.nodeName || a.checkAttr(this.element, this.options.namespace, "validate") && !this.$element.is(this.options.inputs) ? this.bind("parsleyForm") : this.isMultiple() ? this.handleMultiple() : this.bind("parsleyField")
    },
    isMultiple: function () {
      var e = a.getType(this.element);
      return "radio" === e || "checkbox" === e || "SELECT" === this.element.nodeName && null !== this.element.getAttribute("multiple")
    },
    handleMultiple: function () {
      var t, i, n = this;
      if (this.options.multiple = this.options.multiple || (t = this.element.getAttribute("name")) || this.element.getAttribute("id"), "SELECT" === this.element.nodeName && null !== this.element.getAttribute("multiple")) return this.options.multiple = this.options.multiple || this.__id__, this.bind("parsleyFieldMultiple");
      if (!this.options.multiple) return a.warn("To be bound by Parsley, a radio, a checkbox and a multiple select input must have either a name or a multiple option.", this.$element), this;
      this.options.multiple = this.options.multiple.replace(/(:|\.|\[|\]|\{|\}|\$)/g, ""), t && e('input[name="' + t + '"]').each(function (e, t) {
        var i = a.getType(t);
        "radio" !== i && "checkbox" !== i || t.setAttribute(n.options.namespace + "multiple", n.options.multiple)
      });
      for (var r = this._findRelated(), s = 0; s < r.length; s++)
        if (i = e(r.get(s)).data("Parsley"), "undefined" != typeof i) {
          this.$element.data("FieldMultiple") || i.addElement(this.$element);
          break
        } return this.bind("parsleyField", !0), i || this.bind("parsleyFieldMultiple")
    },
    bind: function (t, i) {
      var n;
      switch (t) {
        case "parsleyForm":
          n = e.extend(new w(this.element, this.domOptions, this.options), new l, window.ParsleyExtend)._bindFields();
          break;
        case "parsleyField":
          n = e.extend(new x(this.element, this.domOptions, this.options, this.parent), new l, window.ParsleyExtend);
          break;
        case "parsleyFieldMultiple":
          n = e.extend(new x(this.element, this.domOptions, this.options, this.parent), new $, new l, window.ParsleyExtend)._init();
          break;
        default:
          throw new Error(t + "is not a supported Parsley type")
      }
      return this.options.multiple && a.setAttr(this.element, this.options.namespace, "multiple", this.options.multiple), "undefined" != typeof i ? (this.$element.data("FieldMultiple", n), n) : (this.$element.data("Parsley", n), n._actualizeTriggers(), n._trigger("init"), n)
    }
  };
  var V = e.fn.jquery.split(".");
  if (parseInt(V[0]) <= 1 && parseInt(V[1]) < 8) throw "The loaded version of jQuery is too old. Please upgrade to 1.8.x or better.";
  V.forEach || a.warn("Parsley requires ES5 to run properly. Please include https://github.com/es-shims/es5-shim");
  var T = _extends(new l, {
    element: document,
    $element: e(document),
    actualizeOptions: null,
    _resetOptions: null,
    Factory: P,
    version: "2.8.1"
  });
  _extends(x.prototype, y.Field, l.prototype), _extends(w.prototype, y.Form, l.prototype), _extends(P.prototype, l.prototype), e.fn.parsley = e.fn.psly = function (t) {
    if (this.length > 1) {
      var i = [];
      return this.each(function () {
        i.push(e(this).parsley(t))
      }), i
    }
    if (0 != this.length) return new P(this[0], t)
  }, "undefined" == typeof window.ParsleyExtend && (window.ParsleyExtend = {}), T.options = _extends(a.objectCreate(o), window.ParsleyConfig), window.ParsleyConfig = T.options, window.Parsley = window.psly = T, T.Utils = a, window.ParsleyUtils = {}, e.each(a, function (e, t) {
    "function" == typeof t && (window.ParsleyUtils[e] = function () {
      return a.warnOnce("Accessing `window.ParsleyUtils` is deprecated. Use `window.Parsley.Utils` instead."), a[e].apply(a, arguments)
    })
  });
  var O = window.Parsley._validatorRegistry = new p(window.ParsleyConfig.validators, window.ParsleyConfig.i18n);
  window.ParsleyValidator = {}, e.each("setLocale addCatalog addMessage addMessages getErrorMessage formatMessage addValidator updateValidator removeValidator hasValidator".split(" "), function (e, t) {
    window.Parsley[t] = function () {
      return O[t].apply(O, arguments)
    }, window.ParsleyValidator[t] = function () {
      var e;
      return a.warnOnce("Accessing the method '" + t + "' through Validator is deprecated. Simply call 'window.Parsley." + t + "(...)'"), (e = window.Parsley)[t].apply(e, arguments)
    }
  }), window.Parsley.UI = y, window.ParsleyUI = {
    removeError: function (e, t, i) {
      var n = !0 !== i;
      return a.warnOnce("Accessing UI is deprecated. Call 'removeError' on the instance directly. Please comment in issue 1073 as to your need to call this method."), e.removeError(t, {
        updateClass: n
      })
    },
    getErrorsMessages: function (e) {
      return a.warnOnce("Accessing UI is deprecated. Call 'getErrorsMessages' on the instance directly."), e.getErrorsMessages()
    }
  }, e.each("addError updateError".split(" "), function (e, t) {
    window.ParsleyUI[t] = function (e, i, n, r, s) {
      var o = !0 !== s;
      return a.warnOnce("Accessing UI is deprecated. Call '" + t + "' on the instance directly. Please comment in issue 1073 as to your need to call this method."), e[t](i, {
        message: n,
        assert: r,
        updateClass: o
      })
    }
  }), !1 !== window.ParsleyConfig.autoBind && e(function () {
    e("[data-parsley-validate]").length && e("[data-parsley-validate]").parsley()
  });
  var M = e({}),
    R = function () {
      a.warnOnce("Parsley's pubsub module is deprecated; use the 'on' and 'off' methods on parsley instances or window.Parsley")
    },
    D = "parsley:";
  e.listen = function (e, n) {
    var r;
    if (R(), "object" == typeof arguments[1] && "function" == typeof arguments[2] && (r = arguments[1], n = arguments[2]), "function" != typeof n) throw new Error("Wrong parameters");
    window.Parsley.on(i(e), t(n, r))
  }, e.listenTo = function (e, n, r) {
    if (R(), !(e instanceof x || e instanceof w)) throw new Error("Must give Parsley instance");
    if ("string" != typeof n || "function" != typeof r) throw new Error("Wrong parameters");
    e.on(i(n), t(r))
  }, e.unsubscribe = function (e, t) {
    if (R(), "string" != typeof e || "function" != typeof t) throw new Error("Wrong arguments");
    window.Parsley.off(i(e), t.parsleyAdaptedCallback)
  }, e.unsubscribeTo = function (e, t) {
    if (R(), !(e instanceof x || e instanceof w)) throw new Error("Must give Parsley instance");
    e.off(i(t))
  }, e.unsubscribeAll = function (t) {
    R(), window.Parsley.off(i(t)), e("form,input,textarea,select").each(function () {
      var n = e(this).data("Parsley");
      n && n.off(i(t))
    })
  }, e.emit = function (e, t) {
    var n;
    R();
    var r = t instanceof x || t instanceof w,
      s = Array.prototype.slice.call(arguments, r ? 2 : 1);
    s.unshift(i(e)), r || (t = window.Parsley), (n = t).trigger.apply(n, _toConsumableArray(s))
  };
  e.extend(!0, T, {
    asyncValidators: {
      "default": {
        fn: function (e) {
          return e.status >= 200 && e.status < 300
        },
        url: !1
      },
      reverse: {
        fn: function (e) {
          return e.status < 200 || e.status >= 300
        },
        url: !1
      }
    },
    addAsyncValidator: function (e, t, i, n) {
      return T.asyncValidators[e] = {
        fn: t,
        url: i || !1,
        options: n || {}
      }, this
    }
  }), T.addValidator("remote", {
    requirementType: {
      "": "string",
      validator: "string",
      reverse: "boolean",
      options: "object"
    },
    validateString: function (t, i, n, r) {
      var s, a, o = {},
        l = n.validator || (!0 === n.reverse ? "reverse" : "default");
      if ("undefined" == typeof T.asyncValidators[l]) throw new Error("Calling an undefined async validator: `" + l + "`");
      i = T.asyncValidators[l].url || i, i.indexOf("{value}") > -1 ? i = i.replace("{value}", encodeURIComponent(t)) : o[r.element.getAttribute("name") || r.element.getAttribute("id")] = t;
      var u = e.extend(!0, n.options || {}, T.asyncValidators[l].options);
      s = e.extend(!0, {}, {
        url: i,
        data: o,
        type: "GET"
      }, u), r.trigger("field:ajaxoptions", r, s), a = e.param(s), "undefined" == typeof T._remoteCache && (T._remoteCache = {});
      var d = T._remoteCache[a] = T._remoteCache[a] || e.ajax(s),
        h = function () {
          var t = T.asyncValidators[l].fn.call(r, d, i, n);
          return t || (t = e.Deferred().reject()), e.when(t)
        };
      return d.then(h, h)
    },
    priority: -1
  }), T.on("form:submit", function () {
    T._remoteCache = {}
  }), l.prototype.addAsyncValidator = function () {
    return a.warnOnce("Accessing the method `addAsyncValidator` through an instance is deprecated. Simply call `Parsley.addAsyncValidator(...)`"), T.addAsyncValidator.apply(T, arguments)
  }, T.addMessages("en", {
    defaultMessage: "This value seems to be invalid.",
    type: {
      email: "This value should be a valid email.",
      url: "This value should be a valid url.",
      number: "This value should be a valid number.",
      integer: "This value should be a valid integer.",
      digits: "This value should be digits.",
      alphanum: "This value should be alphanumeric."
    },
    notblank: "This value should not be blank.",
    required: "This value is required.",
    pattern: "This value seems to be invalid.",
    min: "This value should be greater than or equal to %s.",
    max: "This value should be lower than or equal to %s.",
    range: "This value should be between %s and %s.",
    minlength: "This value is too short. It should have %s characters or more.",
    maxlength: "This value is too long. It should have %s characters or fewer.",
    length: "This value length is invalid. It should be between %s and %s characters long.",
    mincheck: "You must select at least %s choices.",
    maxcheck: "You must select %s choices or fewer.",
    check: "You must select between %s and %s choices.",
    equalto: "This value should be the same."
  }), T.setLocale("en");
  var I = new n;
  I.install();
  var q = T;
  return q
});
//# sourceMappingURL=parsley.min.js.map

// IE Custom Event fix
(function () {
  if (typeof window.CustomEvent === "function") return false; //If not IE

  function CustomEvent(event, params) {
    params = params || {
      bubbles: false,
      cancelable: false,
      detail: undefined
    };
    let evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
    return evt;
  }

  CustomEvent.prototype = window.Event.prototype;

  window.CustomEvent = CustomEvent;
})();

$(window).bind("pageshow", function (event) {
  if (event.originalEvent.persisted) {
    window.location.reload()
  }
});

window.Parsley.addValidator('tel', {
  requirementType: 'string',
  validateString: function (value) {
    return (libphonenumber.isValidNumber(value) ||
        libphonenumber.isValidNumber('+1' + value)) ||
      libphonenumber.isValidNumber('+' + value);
  },
  messages: {
    en: 'This value should be a valid phone number.'
  }
});

jQuery(window).on('scroll', function () {
  if ($(".rfi-container").length > 0) {
    var top = jQuery(window).scrollTop(),
      divBottom = jQuery('.rfi-container').offset().top + jQuery('.rfi-container').outerHeight();
    if (divBottom > top) {
      jQuery('#rfi-mobile').removeClass('affix-top');
    } else {
      jQuery('#rfi-mobile').addClass('affix-top');
    }
  }
});

$('.btn_getstrated button').click(function (e) {
  $(this).fadeOut('slow', function () {
    $('#btn_selection').fadeIn('slow');
  });
});

function toTitleCase(str) {
  return str.replace(/(?:^|\s)\w/g, function (match) {
    return match.toUpperCase();
  });
}

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
  var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/)
  if (match) {
    var intlCode = (match[1] ? '+1 ' : '')
    return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('')
  }
  return null
}

$(document).ready(function () {
  $(document).on("focus", ".disabledField", function () {
    $(this).blur();
  });
  // setTimeout(() => {
  //   $('.owl-carousel').owlCarousel({
  //   loop:true,
  //   margin:10,
  //   nav:true,
  //   responsive:{
  //       0:{
  //           items:1
  //       },
  //       600:{
  //           items:3
  //       },
  //       1000:{
  //           items:3
  //       }
  //   }
  // });
//  },1000);
});

$(window).scroll(function (i) {
  var scrollDistance = $(window).scrollTop();
  if ($("#ecee_sec").length) {
    // var ele2 = $("#ecee_sec").offset().top - 100
    // var ele1 = $("#edu_sec").offset().top
    // // var lowerLimit = $("#opportunities").offset().top;
    // // var upperLimit = $("#are_you_eligible").offset().top
    // if (scrollDistance > lowerLimit && scrollDistance < upperLimit) {
    //   if (scrollDistance > ele1 && scrollDistance < ele2) {
    //     if ($('#ecee_nav').hasClass('nav-active-opp')) {
    //       $('#ecee_nav').removeClass('nav-active-opp');
    //     } else {
    //       $('#edu_sec_nav').addClass('nav-active-opp');
    //     }
    //   } else if (scrollDistance > ele1) {
    //     if ($('#edu_sec_nav').hasClass('nav-active-opp')) {
    //       $('#edu_sec_nav').removeClass('nav-active-opp');
    //     } else {
    //       $('#ecee_nav').addClass('nav-active-opp');
    //     }
    //   }
    // }
  }
  AOS.init({
    disable: function() {
      var maxWidth = 767;
      return $(window).width() < maxWidth;
    },
    duration: 1200,
  })
}).scroll();
