import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueAxios from "vue-axios";
import VeeValidate from "vee-validate";
import faqjson from "./assets/faqs.json";
import Scrollspy from "vue2-scrollspy/dist/index";
import InstantSearch from "vue-instantsearch";
import coursesjson from "./assets/courses.json";
import i18n from "./services/i18n/i18n";
import "./components/global/_globals";
const _ = require("lodash");
import "@/assets/scss/main.scss";
import VModal from "vue-js-modal";
import VueScrollactive from "vue-scrollactive";
import VueTypedJs from "vue-typed-js";
import {dataLayerAnchorTag,dataLayerButtonTag,dataLayerInputTag,dataLayerSelectTag} from "data-layer-test-oo-gtm"
import { BootstrapVue } from "bootstrap-vue";
Vue.use(VModal);
Vue.use(VueScrollactive);
Vue.use(InstantSearch);
Vue.use(VueAxios, axios);
Vue.use(VeeValidate);
Vue.use(Scrollspy);
Vue.use(VueTypedJs);
Vue.use(BootstrapVue);
Vue.config.productionTip = false;
Vue.directive(dataLayerAnchorTag,dataLayerButtonTag,dataLayerInputTag,dataLayerSelectTag)
let app = new Vue({
  router,
  store,
  i18n,
  data() {
    return {};
  },
  methods: {
    getWindowWidth(event) {
      this.windowWidth = document.documentElement.clientWidth;
      if (this.windowWidth > 992) {
        this.$store.state.desktop = true;
        this.$store.state.mobile = false;
        this.$store.state.tab = false;
      } else if (this.windowWidth > 767 && this.windowWidth < 991) {
        this.$store.state.desktop = false;
        this.$store.state.mobile = false;
        this.$store.state.tab = true;
      } else {
        this.$store.state.tab = false;
        this.$store.state.desktop = false;
        this.$store.state.mobile = true;
        if ($("#explore-programs").length && $("#discover").length) {
          let upperlimit =
            document.getElementById("explore-programs").offsetTop + 40;
          let lowerLimit =
            document.getElementById("discover").offsetTop -
            $("#discover").height() +
            40;
          if (
            window.pageYOffset > upperlimit &&
            window.pageYOffset < lowerLimit
          ) {
            this.$store.state.sticky = true;
          } else {
            this.$store.state.sticky = false;
          }
        }
      }
    },
  },
  created() {
  //  window.location.host.indexOf("eats") >= 0
  //   ? (this.$store.state.isUberEats = true)
  //   : (this.$store.state.isUberEats = false);
     this.$store.state.courses = coursesjson;
    coursesjson.CPE_Courses.forEach((element) => {
      this.$store.state.courseTitles = this.$store.state.courseTitles.concat(
        element.courses.map((a) => a.title)
      );
    });
    this.$store.state.faqs = faqjson;
    window.scroll({
      top: 100,
      left: 100,
      behavior: "smooth",
    });
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
    if (document.documentElement.clientWidth > 991) {
      this.$store.state.desktop = true;
      this.$store.state.mobile = false;
      this.$store.state.tab = false;
    } else if (
      document.documentElement.clientWidth < 991 &&
      document.documentElement.clientWidth > 767
    ) {
      this.$store.state.desktop = false;
      this.$store.state.mobile = false;
      this.$store.state.tab = true;
    } else {
      this.$store.state.tab = false;
      this.$store.state.desktop = false;
      this.$store.state.mobile = true;
    }
  },
  mounted() {
    window.addEventListener("resize", this.getWindowWidth);
    window.addEventListener("scroll", this.getWindowWidth);
  },
  beforeDestroy() {
    window.removeEventListener("resize", this.getWindowWidth);
  },
  render: (h) => h(App),
}).$mount("#app");
