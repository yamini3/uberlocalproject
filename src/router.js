import Vue from 'vue'
import Router from 'vue-router'
import Driver from './views/Driver.vue'
import Family from './views/Family.vue'
import Public from './views/Public.vue'
import ASUOLp from './views/ASUOLp.vue'
import Home from './views/home.vue'
import CPE from './views/CPE.vue'
import VueRouter from 'vue-router';
import Beneficiaryterms from './views/Beneficiaryterms.vue'
import Privacypage from './views/Privacypage.vue'
import Thankyou from './views/Thankyou.vue'
import Verify from './views/Verify.vue'
import Eligibility from './views/Eligibility.vue'
import CPESignup from './views/CPESignup.vue'

Vue.use(Router);


let router = new VueRouter({
  routes: [{
    path: '/',
    name: 'public',
    component: Public
  }, {
    path: '/driver',
    name: 'driver',
    component: Driver,
    meta: {
      requiresAuth: true
    }
  }, {
    path: '/family',
    name: 'family',
    component: Family
  }, {
    path: '/driver/asuo',
    name: 'asuolp',
    component: ASUOLp,
    meta: {
      requiresAuth: true
    }
  }, 
  {
    path: '/family/asuo',
    name: 'asuolp',
    component: ASUOLp,
    meta: {
      requiresAuth: true
    }
  },{
    path: '/home',
    name: 'home',
    component: Home
  },  {
    path: '/driver/cpe',
    name: 'cpe',
    component: CPE,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/family/cpe',
    name: 'cpe',
    component: CPE,
    meta: {
      requiresAuth: true
    }
  }, {
    path: '/beneficiaryterms',
    name: 'Beneficiaryterms',
    component: Beneficiaryterms
  }, {
    path: '/privacy',
    name: 'Privacypage',
    component: Privacypage
  }, {
    path: '/thankyou',
    name: 'Thankyou',
    component: Thankyou
  }, {
    path: '/cpe/signup',
    name: 'CPESignup',
    component: CPESignup
  }, {
    path: '/verify',
    name: 'verify',
    component: Verify
  },
  {
    path: '/eligibility',
    name: 'eligibility',
    component: Eligibility,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '*',
    redirect: '/'
  }],
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (to.fullPath.split('#')[0] !== from.fullPath.split('#')[0]) return {
      x: 0,
      y: 0
    }
  }
});



router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    window.location.search.slice(1).split("&").forEach(element => {
      if (element.split("=")[0] === 'error') next({ path: '/' });
    });
  }
  return !hasQueryParams(to) && hasQueryParams(from) ? next({ name: to.name, query: from.query }) : next();

  function hasQueryParams(route) {
    return !!Object.keys(route.query).length
  }
});

router.afterEach((to, from) => {
  Array.from(document.getElementsByTagName('meta')).forEach(element => {
    if (element.getAttribute('name') === 'robots') {
      element.removeAttribute('content');

      return to.name === 'public' ? void element.setAttribute('content', 'nofollow') : element.setAttribute('content', 'noindex, nofollow');
    }
  });
})

export default router;