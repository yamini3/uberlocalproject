import Vue from 'vue';
import VueI18n from 'vue-i18n';
import driver from '../../assets/pages/driver/index.js'
import asuo from '../../assets/pages/asuo/index.js'
import cpe from '../../assets/pages/cpe/index.js'
import publicPage from '../../assets/pages/public/index.js'
import family from '../../assets/pages/family/index.js'
import terms from '../../assets/pages/terms/index.js'
import faqs from '../../assets/faqs.json'

Vue.use(VueI18n);
const translations = {
    'uberPro': {
        driverPage:{
            content : driver.uberPro
        },
        asuo:{
            content : asuo.uberPro
        },
        cpe:{
            content : cpe.uberPro
        },
        publicPage:{
            content : publicPage.uberPro
        },
        family:{
            content : family.uberPro
        },
        faqs:{
            content : faqs.uberPro
        },
        terms:{
            content : terms.uberPro
        }
    },
    'uberEats': {
        driverPage:{
            content : driver.uberEats
        },
        asuo:{
            content : asuo.uberEats
        },
        cpe:{
            content : cpe.uberEats
        },
        publicPage:{
            content : publicPage.uberEats
        },
        family:{
            content : family.uberEats
        },
        faqs:{
            content : faqs.uberEats
        },
        terms:{
            content : terms.uberEats
        }
    }
}
const i18n = new VueI18n({
    locale: 'uberPro', // set locale
    fallbackLocale: 'uberEats', // set fallback locale
    messages:translations, // set locale messages
});


export default i18n;