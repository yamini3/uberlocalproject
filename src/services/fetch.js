import axios from "axios";
export default {
  getPrograms() {
    return new Promise((resolve, reject) => {
      axios
        .get("https://api.edpl.us/v1/asuo/programs?category=undergraduate", {
          Accept: "text/json"
        })
        .then(function(response) {
          resolve(response.data);
        })
        .catch(function(error) {
          reject({
            details: "failed to get the content",
            errCode: "STARB001"
          });
        });
    });
  }
};
