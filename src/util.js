'use strict';

/**
 * Util functions to go here updated the env files
 */
let util = {
    getEnv() {
        return (window.location.host.split('.')[0] === 'staging') ? 'stage' : 
        (window.location.host.split('.')[0].indexOf('localhost') >=0 || window.location.host.split('.')[0].indexOf('dev') >= 0 )? 'dev' : 
        (window.location.host.split('.')[0].indexOf('p2') >= 0) ? 'stage2' : 'prod'
    },
    getEnvP3() {
        return (window.location.host.split('.')[0] === 'p2') ? 'stage' : 
        (window.location.host.split('.')[0].indexOf('localhost')) >= 0 ? 'dev': 'prod'
    },
    getAuthorizeUrl() {
        let callback = encodeURIComponent(window.location.protocol + "//" + window.location.host + "/driver" + window.location.search);

        return "https://login.uber.com/oauth/v2/authorize?response_type=code&redirect_uri=" + callback + "&scope=partner.rewards%20partner.accounts&client_id=Pa2vrsR0wBpQxpinedGQ1fRlNuIyrIZb"
    },
    getVerifyUrl() {
        let callback = encodeURIComponent(window.location.protocol + "//" + window.location.host + "/eligibility" + window.location.search);

        return "https://login.uber.com/oauth/v2/authorize?response_type=code&redirect_uri=" + callback + "&scope=partner.rewards%20partner.accounts&client_id=Pa2vrsR0wBpQxpinedGQ1fRlNuIyrIZb"
    },
    getVerifyOAuthAPIUrl() {
        let env = this.getEnv()
        switch(env) {
            case 'dev':
                return `https://a8jgxgx67a.execute-api.us-west-2.amazonaws.com/dev`;
            case 'stage':
                return `https://a8jgxgx67a.execute-api.us-west-2.amazonaws.com/dev`;
            case 'prod':
                return `https://ibdlicarhc.execute-api.us-west-2.amazonaws.com/prod`;
            default:
                return `https://a8jgxgx67a.execute-api.us-west-2.amazonaws.com/dev`;    
        }
    },
    checkRFIUrl() {
        let env = this.getEnv()
        switch(env) {
            case 'dev':
                return `https://dev-api.edpl.us/uber-verify-rfi/dev`;
            case 'stage':
                return `https://dev-api.edpl.us/uber-verify-rfi/dev`;
            case 'prod':
                return `https://api.edpl.us/uber-verify-rfi/prod`;
            default:
                return `https://dev-api.edpl.us/uber-verify-rfi/dev`;   
        }
    },
    getLeadAPIUrl() {
        return `https://${this.getEnv() === 'prod' ? '' : this.getEnv() === 'dev' ? 'dev-' : 'qa-'}api.edpl.us/v1/rfiJourney/rfi-leads?get_dx_url=true`;
    },
    getAPIUrl() {
        return `https://1rb0y109ia.execute-api.us-west-2.amazonaws.com/${this.getEnv()}`;
    },
    getEPP(){
        return this.getEnv() === 'prod' ? `https://uber-catalog.cpe.asu.edu` : `https://staging-uber-catalog.cpe.asu.edu` ; 
    },
    getCPEPortalLink() {
        return  this.getEnv() === 'prod' ? `https://uber-catalog.cpe.asu.edu?rfi_id=%rfi_id%` : `https://staging-uber-catalog.cpe.asu.edu?rfi_id=%rfi_id%`
    },
    getTransferLogicApi(){
        return `https://${this.getEnv() === 'prod' ? '' : this.getEnv() === 'dev' ? 'dev-' : 'qa-'}api.edpl.us/v1/cp/uber/partner/benefit/transfers/`
    },
    getDXLink(id) {
        return this.getEnv() === 'prod' ? `https://next.online.asu.edu?rfi_id=${id}` : `https://staging-next.online.asu.edu?rfi_id=${id}`;
    },
    getTestEnv() {
        return this.getEnv() === 'prod' ? `FALSE` : 'TRUE';
    },
    getUberCategory(){
        if (window.location.host.indexOf("eats") >= 0) {
            return {
                sub_class:{
                    driver : "UED",
                    beneficiaries: "UEF"
                },
                dx:{
                    driver : "ued",
                    beneficiaries: "uef"
                },
                source_id:"uber_eats_authenticated_lp-NK+iNwZ2F/uu+GXl"
            }
          } else {
            return {
                sub_class:{
                    driver : "uber_driver",
                    beneficiaries: "uber_beneficiary"
                },
                dx:{
                    driver : "uber-driver",
                    beneficiaries: "uber-beneficiary"
                },
                source_id:"uber_authenticated_lp-Bsaef0JxosOKGFzW"
            }
          }
    }
}

export default util;