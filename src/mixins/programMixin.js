import {
  timeout
} from "q";
import {
  PreviousMap
} from "postcss";

export const programMixin = ({
  data() {
    return {
      section: 0,
      dispCourses: false,
      value: 0,
      carouseldata: [],
      data: [],
      selected: undefined
    };
  },
  methods: {
    showdesc: function (code) {
      this.selected = code;
      let offset = $("#" + code).prev().offset();
      let ele1 = $('.algolia-sticky-header')[0];
      let ele2 = $("#" + code)[0];
      if (this.isColliding(ele1, ele2)) {
        $('html, body').animate({
          scrollTop: offset.top - 200
        });
      }
      $('#' + code).toggleClass('d-block');
    },
    isColliding: function (el1, el2) {
      var rect1 = el1.getBoundingClientRect();
      var rect2 = el2.getBoundingClientRect();

      let bool = !(
        rect1.top > rect2.bottom ||
        rect1.right < rect2.left ||
        rect1.bottom < rect2.top ||
        rect1.left > rect2.right
      );
      return bool;
    },
    installOwlCarousel: function () {
      $(".owl-carousel").owlCarousel({
        loop: true,
        items: 2,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
            nav: true,
            loop: false,
            touchDrag: true,
            mouseDrag: true
          },
          600: {
            items: 2,
            nav: false,
            nav: true,
            loop: false,
            touchDrag: false,
            mouseDrag: false
          },
          1000: {
            items: 3,
            nav: true,
            loop: true,
            touchDrag: false,
            mouseDrag: false,
            margin: 20
          }
        }
      })
    },
    showDescMobile(desc) {
      this.dispCourses = !this.dispCourses;
    },
    getImgUrl(image) {
      try{
        return require("../assets/images/degreeIcons/" + _.camelCase(image) + ".svg");
      }
      catch{
        return require("../assets/images/degreeIcons/education.svg");
      }
    },
  },
  //   created() {
  //     this.$http({
  //       method: "get",
  //       url: "https://asuonline.asu.edu/lead-submissions-v3.3/programs?category=undergraduate",
  //       headers: {
  //         Accept: "application/javascript"
  //       }
  //     }).then(response => {
  //       console.log(response)
  //       this.carouseldata = response.data;
  //       this.$nextTick(function(){
  //       this.installOwlCarousel();
  //       $(this).attr("id", 'degree-owl-dot')
  //     }.bind(this))
  // });
  //   },
})