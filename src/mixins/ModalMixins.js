import util from "../util.js";
export const ModalMixins = {
  data() {
    return {
      tcChecked: false,
      totalLimit: null,
      transferAPILoading: true,
      transferAPIError: false,
      profileData: {
        first_name: "",
      },
      DateOfBirth: {},
      date_current: [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
      ],
      date_28: [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
      ],
      date_31: [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
      ],
      date_30: [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
      ],
      date_29: [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
      ],
      month: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      year: [],
      select_month: "",
      select_date: "",
      select_year: "",
      select_month_b: "",
      select_date_b: "",
      select_year_b: "",
      date_current_b: [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
      ],
      baseUrl: "",
      rewardsUrl: "/v1/partners/me/rewards/eligibility",
      flags: {
        toggleBeneficiary: false, //  false means the driver hasn't clicked on the benny checkbox
        rfiForm: true,
        rfiFormError: false,
        rfiFormSuccess: false,
        rfiOpportunity: "",
        rfiDriver: false,
        toggleterms: false,
        selectedCourses: false, //  false means no courses are selected
        selectedDependentRelation: false, //  false means beneficiary relation type is not selected
      },
      submitted: false,
      data: {
        rfi: "",
      },
      signupTitle: "",
      userEmail: "",
      rfiSubmissionMsg: {
        driver_degree: {
          rfiSubmitTitle: "Thank you for your interest in ASU Online",
          rfiSubmitMessage:
            "<p style='font-size: 13px; font-weight: 1000; margin-bottom: 30px;'>You are now being redirected to an interactive guide that will help you explore your tuition coverage and apply to ASU.</p>",
          rfiFooterMessage:
            "<span style='font-size: 11px;'>If you haven't been redirected in 30 seconds, please click this <a data-region='uber-landing-page' style='font-weight: bold; border-bottom: 1px dotted #a30046;' href='%url%'>link</a> to continue</span>",
          isredirectDX: true,
        },
        driver_course: {
          rfiSubmitTitle: "Thank you",
          rfiSubmitMessage:
            "You are now being redirected to the Course Enrollment Portal where you will choose your courses and complete your enrollment.",
          rfiFooterMessage:
            "<span style='font-size: 11px;'>If you haven't been redirected in 30 seconds, please click this <a data-region='uber-landing-page' style='font-weight: bold; border-bottom: 1px dotted #a30046;' href='%url%'>link</a> to continue</span>",
          isredirectDX: true,
        },
        family_degree: {
          rfiSubmitTitle: "Thank you",
          rfiSubmitMessage:
            "You just unlocked the opportunity for a family member to earn an undergraduate degree online at Arizona State University. This is a special moment!",
          rfiFooterMessage:
            "Let your family member know they will receive an email from ASU containing the next steps to apply. Thank you for sharing the gift of education.",
        },
        family_course: {
          rfiSubmitTitle: "Thank you",
          rfiSubmitMessage:
            "You just unlocked the opportunity for a family member to build upon their English language skills or learn how to build a business with entrepreneurship courses. This is a special moment!",
          rfiFooterMessage:
            "Let your family member know they will receive an email from ASU containing an invitation to access their upskilling courses and complete their sign-up. Thank you for sharing the gift of education.",
        },
        rfiSubmitTitle: "",
        rfiSubmitMessage: "",
        rfiFooterMessage: "",
      },
    };
  },
  computed: {
    computeUniqueClass() {
      let uniqueClass = "";
      if (this.flags.rfiOpportunity === "CPE") {
        uniqueClass = this.flags.toggleBeneficiary
          ? "RFI Submit CPE Dependent’"
          : "RFI Submit CPE Driver";
      } else if (this.flags.rfiOpportunity === "ASUO") {
        uniqueClass = this.flags.toggleBeneficiary
          ? "RFI Submit ASUO Dependent"
          : "RFI Submit ASUO Driver";
      }
      return uniqueClass;
    },
  },
  methods: {
    retryTransferLogicApi() {
      this.transferLogicApi(JSON.parse(sessionStorage.getItem("profileData")));
    },
    transferLogicApi(resp) {
      console.log(resp);
      this.transferAPILoading = true;
      this.transferAPIError = false;
      this.$http({
        method: "get",
        url: util.getTransferLogicApi() + resp.driver_id,
        headers: {
          Accept: "application/javascript",
        },
      })
        .then((response) => {
          let currentLimit = response.data.limit - response.data.current;
          this.totalLimit = currentLimit <= 0 ? 0 : currentLimit;
          // this.totalLimit = 0;
          this.transferAPILoading = false;
          this.transferAPIError = false;
        })
        .catch((err) => {
          this.transferAPILoading = false;
          this.transferAPIError = true;
        });
    },
    handleSubmit(e) {
      this.submitted = true;
      this.$validator.validate().then((valid) => {
        if (
          valid ||
          (this.$validator.errors.items.length === 1 &&
            this.$validator.errors.items[0].field === "course_list_cpe" &&
            this.flags.rfiOpportunity === "ASUO")
        )
          this.rfi_form_submit();
      });
    },
    checkEligibility(rfi) {
      let self = this;
      this.flags.selectedCourses = false; //  Since no courses are selected before opening the rfi modal we are setting this value to false
      this.flags.toggleBeneficiary = false;
      this.flags.toggleterms = false;
      $("#is_family_selection").prop("checked", false);
      $("#benificiary-btn-1").attr("disabled", "disabled");
      $("#benificiary-btn-2").attr("disabled", "disabled");
      this.flags.rfiOpportunity = rfi;
      this.flags.rfiOpportunity === "driver"
        ? (this.flags.rfiDriver = true)
        : (this.flags.rfiOpportunity = rfi);
      if (rfi === "driver") {
        this.flags.rfiOpportunity = "";
      }

      this.signupTitle =
        rfi === "ASUO"
          ? "Sign up for a degree"
          : rfi === "driver"
          ? "Sign up"
          : "Sign up for courses";
      this.$http({
        method: "get",
        url: `${this.baseUrl}${this.rewardsUrl}?driver=${this.profileData.driver_id}`,
        headers: {
          Authorization: JSON.parse(sessionStorage.getItem("token"))
            .access_token,
        },
      })
        .then((response) => {
          if (
            (response.data.reward_type && response.data.eligible) ||
            self.profileData.email === "fuller.christian@gmail.com"
          ) {
            if (!(this.flags.rfiFormError || this.flags.rfiFormSuccess)) {
              this.flags.rfiForm = true;
              document.getElementById("rfi-main-form").style.display = "block";
            }
          } else {
            this.flags.rfiFormError = true;
            this.flags.rfiForm = false;
          }
        })
        .catch((err) => (this.flags.rfiFormError = true));
    },
    closeAllAreas() {
      $(
        "#view_degree_modal .modal-body ul li div.collapse.show, #view_courses_modal .modal-body ul li div.collapse.show"
      ).each((i, e) => {
        $(e).removeClass("show");
        $(e)
          .parent()
          .find("a")
          .attr("aria-expanded", false);
      });
      $(".hidden-overlay, #show-all-course").show("fast");
      $("ul.list-group-programs li:nth-child(n+6), #hide-all-course").hide();
    },
    hrefValue(index) {
      return "#collapse-code-" + index;
    },
    idValue(index) {
      return "collapse-code-" + index;
    },
    getCourseIds(array) {
      if (!array) return "";

      return array.sort((a, b) => parseInt(a) - parseInt(b)).join("|");
    },
    rfiFormSubmissionMessage: function(DXlink) {
      var msgLink;
      if (
        !this.flags.toggleBeneficiary &&
        this.flags.rfiOpportunity == "ASUO"
      ) {
        msgLink = this.rfiSubmissionMsg.driver_degree;
      } else if (
        !this.flags.toggleBeneficiary &&
        this.flags.rfiOpportunity == "CPE"
      ) {
        msgLink = this.rfiSubmissionMsg.driver_course;
      } else if (
        this.flags.toggleBeneficiary &&
        this.flags.rfiOpportunity == "CPE"
      ) {
        msgLink = this.rfiSubmissionMsg.family_course;
      } else if (
        this.flags.toggleBeneficiary &&
        this.flags.rfiOpportunity == "ASUO"
      ) {
        msgLink = this.rfiSubmissionMsg.family_degree;
      }

      if (msgLink) {
        this.rfiSubmissionMsg.rfiSubmitTitle = msgLink.rfiSubmitTitle;
        this.rfiSubmissionMsg.rfiSubmitMessage = msgLink.rfiSubmitMessage;
        this.rfiSubmissionMsg.rfiFooterMessage = msgLink.rfiFooterMessage.replace(
          "%url%",
          DXlink
        );
        this.rfiSubmissionMsg.isredirectDX = msgLink.isredirectDX;
      }
    },
    rfi_form_submit() {
      var driver_asuo = false,
        driver_cpe = false,
        requestBody = {},
        self = this;

      if (this.flags.toggleBeneficiary) {
        requestBody = {
          first_name: this.profileData.beneficiary_fname,
          last_name: this.profileData.beneficiary_lname,
          email_address: this.profileData.beneficiary_mail,
          dob:
            document.getElementById("year_b").value +
            "-" +
            document.getElementById("month_b").value +
            "-" +
            document.getElementById("day_b").value,
          sub_class: util.getUberCategory().sub_class.beneficiaries,
          cp_employee_id: this.profileData.driver_id,
          cp_benefits_eligible: "Y",
          cp_employee_relationship: $("#program_type").select2("val"),
          cp_benefits_eligible_end_date: "",
          cp_employee_first_name: this.profileData.first_name,
          cp_employee_last_name: this.profileData.last_name,
          cp_employee_dob:
            document.getElementById("year").value +
            "-" +
            document.getElementById("month").value +
            "-" +
            document.getElementById("day").value,
          cp_employee_email_address: this.profileData.email,
          cp_employee_phone: this.profileData.phone_number,
          lead_class: "CORP",
          sourceid: util.getUberCategory().source_id,
          origin_uri: window.location.protocol + "//" + window.location.host,
          cp_employee_terms_and_conditions: true,
          sms_permission: true,
        };
      } else {
        driver_asuo = this.flags.rfiOpportunity == "ASUO" ? true : false;
        driver_cpe = this.flags.rfiOpportunity == "CPE" ? true : false;
        requestBody = {
          first_name: this.profileData.first_name,
          last_name: this.profileData.last_name,
          email_address: this.profileData.email,
          // dob: this.profileData.dob,
          dob:
            document.getElementById("year").value +
            "-" +
            document.getElementById("month").value +
            "-" +
            document.getElementById("day").value,
          phone: this.profileData.phone_number,
          sub_class: util.getUberCategory().sub_class.driver,
          lead_class: "CORP",
          sourceid: util.getUberCategory().source_id,
          origin_uri: window.location.protocol + "//" + window.location.host,
          cp_employee_id: this.profileData.driver_id,
          cp_benefits_eligible: "Y",
          cp_employee_terms_and_conditions: true,
          sms_permission: true,
        };
      }

      if (this.flags.rfiOpportunity == "ASUO") {
        requestBody.program_key = "UGUD-UNDECIDED";
      } else {
        requestBody.course_id = "00";
      }

      requestBody.enterpriseclientid = cidA;
      requestBody.clientid = cidE;

      window.location.search
        .slice(1)
        .split("&")
        .forEach((e) => {
          if (e.split("=")[0] !== "params" && e.split("=")[0] !== "state")
            requestBody[e.split("=")[0]] = e.split("=")[1];
        });

      let leadUrl = util
        .getLeadAPIUrl()
        .replace("rfiJourney", this.flags.rfiOpportunity.toLowerCase());

      // if (window.location.search.indexOf('test=1') >= 0) leadUrl = leadUrl + '?test=1';
      this.$http({
        method: "post",
        url: leadUrl,
        data: require("qs").stringify(requestBody),
        headers: {
          Accept: "application/javascript",
          "x-partner-token": JSON.parse(sessionStorage.getItem("token"))
            .refresh_token,
          "Content-Type": "application/x-www-form-urlencoded",
        },
      })
        .then(function(response) {
          dataLayer.push({
            event: self.computeUniqueClass,
            asuoid: response.data.id,
          });

          if (driver_asuo) {
            let phoneNumber = document
              .getElementById("phone")
              .value.replace(/\+/g, ""); // remove + if exists
            let phone = {};
            if (phoneNumber.length > 10) {
              phone = {
                last10Char: phoneNumber.substring(phoneNumber.length - 10),
                remainingChar: phoneNumber.substring(
                  0,
                  phoneNumber.length - 10
                ),
              };
            } else {
              phone = {
                last10Char: phoneNumber.substring(phoneNumber.length - 10),
                remainingChar: "1",
              };
            }
            let DXlink;
            if (!response.data.dx_url) {
              DXlink = util.getDXLink(response.data.id);
              console.log("rfiID" + response.data.id);
            } else {
              DXlink = response.data.dx_url;
            }
            return (window.location = DXlink);
          } else if (driver_cpe) {
            self.flags.rfiFormSuccess = false;
            self.flags.rfiForm = false;
            let CPEPortalLink = util
              .getCPEPortalLink()
              .replace("%rfi_id%", response.data.id);
            self.rfiFormSubmissionMessage(CPEPortalLink);
            self.sendMailer(response.data.id);
            setTimeout(() => {
              return (window.location = CPEPortalLink);
            }, 10000);
          } else {
            self.rfiFormSubmissionMessage();
            self.flags.rfiFormSuccess = true;
            self.flags.rfiForm = false;
            self.sendMailer(response.data.id);
          }
        })
        .catch(function(response) {
          console.log("REESPONSE====++++ " + response);
        });
    },
    sendMailer(responseId) {
      let requestBody = {
        RFIClient:
          window.location.host.indexOf("eats") >= 0 ? "UberEats" : "UberPro",
        bennyFirstName: (document.getElementById("beneficiary_fname") || {})
          .value,
        emailAddress: this.flags.toggleBeneficiary
          ? this.profileData.beneficiary_mail
          : this.profileData.email,
        driverFname: this.profileData.first_name,
        name: this.profileData.first_name + " " + this.profileData.last_name,
        rfiJourney:
          this.flags.rfiOpportunity.toLowerCase() +
          "_" +
          this.flags.toggleBeneficiary,
        url:
          window.location.protocol +
          "//" +
          window.location.host +
          "/beneficiaryterms?benefit=" +
          this.flags.rfiOpportunity.toLowerCase() +
          "&id=" +
          responseId,
        baseURL: util.getEPP(),
        redirect_url: window.location.protocol + "//" + window.location.host,
        benefit: this.flags.rfiOpportunity.toLowerCase(),
        RFIid: responseId,
        test: util.getTestEnv ? 1 : 0,
      };
      this.$http({
        method: "post",
        url: this.baseUrl + "/mail",
        data: require("qs").stringify(requestBody),
        headers: {
          Authorization: JSON.parse(sessionStorage.getItem("token"))
            .access_token,
          "Content-Type": "application/x-www-form-urlencoded",
        },
      })
        .then()
        .catch((response) => {
          console.log("REESPONSE====++++ " + response);
        });
    },
  },
  beforeMount() {
    const dict = {
      custom: {
        Email: {
          required: "Please fill in your email address",
        },
        Email: {
          email: "Please fill in a valid email address",
        },
        beneficiary_first_name: {
          required: () => "Please enter the beneficiary's first name",
        },
        beneficiary_last_name: {
          required: () => "Please enter the beneficiary's last name",
        },
        beneficiary_date_of_birth_m: {
          required: () => "Please select the beneficiary's date of birth",
        },
        beneficiary_date_of_birth_d: {
          required: () => "Please select the beneficiary's date of birth",
        },
        beneficiary_date_of_birth_y: {
          required: () => "Please select the beneficiary's date of birth",
        },
        beneficiary_email: {
          email: () => "Please enter a valid email address",
        },
        course_list_cpe: {
          required: () => "Please select a course to continue",
        },
        date_of_birth_m: {
          required: () => "Please select your date of birth",
        },
        date_of_birth_d: {
          required: () => "Please select your date of birth",
        },
        date_of_birth_y: {
          required: () => "Please select your date of birth",
        },
        course_list_cpe: {
          required: () => "Please select a course to continue",
        },
        beneficiary_email: {
          required: () => "Please enter the beneficiary's email address",
        },
        program_type: {
          required: () =>
            "Please tell us how the beneficiary is related to you",
        },
      },
    };
    this.$validator.localize("en", dict);
  },
  created() {
    this.baseUrl = util.getAPIUrl();
    var i,
      n = new Date().getFullYear();
    for (i = 0; i < 100; i++) {
      this.year.push(n - i);
    }
  },
  beforeUpdate() {
    $("#course_list_cpe.form-control").select2({
      placeholder: "Select your courses",
      allowClear: true,
      minimumResultsForSearch: -1,
    });
    $(".dependent-select select.form-control").select2({
      minimumResultsForSearch: -1,
    });
    var self = this,
      array;
    $(document).ready(function() {
      $("#course_list_cpe").change(function() {
        //  V-validate doesn't work well with select2. Even after a value is selected the error message doesn't disappear
        array = $("#course_list_cpe").select2("val"); //  This is a JQuery function to explicitly
        self.flags.selectedCourses = !!array; //  remove the error message upon selection of an option
      });
      $("#program_type").change(function() {
        //  V-validate doesn't work well with select2. Even after a value is selected the error message doesn't disappear
        array = $("#program_type").select2("val"); //  This is a JQuery function to explicitly
        self.flags.selectedDependentRelation = !!array; //  remove the error message upon selection of an option
      });
      $("#month").change(function() {
        self.select_month = $("#month").val();
        if (self.select_month == 2) {
          if (
            (self.select_year != "" &&
              self.select_year % 4 == 0 &&
              self.select_year % 100 != 0) ||
            (self.select_year != "" && self.select_year % 400 == 0)
          ) {
            self.date_current = self.date_29;
          } else {
            self.date_current = self.date_28;
          }
        } else if (
          ["1", "3", "5", "7", "8", "10", "12"].indexOf(self.select_month) >= 0
        ) {
          self.date_current = self.date_31;
        } else {
          self.date_current = self.date_30;
        }
      });
      $("#year").change(function() {
        self.select_year = $("#year").val();
        if (self.select_month == 2) {
          if (
            (self.select_year % 4 == 0 && self.select_year % 100 != 0) ||
            self.select_year % 400 == 0
          ) {
            self.date_current = self.date_29;
          } else {
            self.date_current = self.date_28;
          }
        }
      });
      $("#day").change(function() {
        self.select_date = $("#day").val();
      });
      $("#month_b").change(function() {
        self.select_month_b = $("#month_b").val();
        if (self.select_month_b == 2) {
          if (
            (self.select_year_b != "" &&
              self.select_year_b % 4 == 0 &&
              self.select_year_b % 100 != 0) ||
            (self.select_year_b != "" && self.select_year_b % 400 == 0)
          ) {
            self.date_current_b = self.date_29;
          } else {
            self.date_current_b = self.date_28;
          }
        } else if (
          ["1", "3", "5", "7", "8", "10", "12"].indexOf(self.select_month_b) >=
          0
        ) {
          self.date_current_b = self.date_31;
        } else {
          self.date_current_b = self.date_30;
        }
      });
      $("#year_b").change(function() {
        self.select_year_b = $("#year_b").val();
        if (self.select_month_b == 2) {
          if (
            (self.select_year_b % 4 == 0 && self.select_year_b % 100 != 0) ||
            self.select_year_b % 400 == 0
          ) {
            self.date_current_b = self.date_29;
          } else {
            self.date_current_b = self.date_28;
          }
        }
      });
      $("#day_b").change(function() {
        self.select_date_b = $("#day_b").val();
      });
    });
  },
  mounted() {
    $(document).ready(function() {
      $("#check_box_1").click(function() {
        if (document.getElementById("check_box_1").checked) {
          $("#benificiary-btn-1").removeAttr("disabled");
          $("#benificiary-btn-2").removeAttr("disabled");
        } else {
          $("#benificiary-btn-1").attr("disabled", "disabled");
          $("#benificiary-btn-2").attr("disabled", "disabled");
        }
      });
      // $("#course_list_cpe.form-control").select2({
      //   placeholder: "Select your courses",
      //   allowClear: true,
      //   minimumResultsForSearch: -1
      // });

      $("ul.list-group-programs li:not(:nth-child(n+6))")
        .show()
        .css("display", "flex");
      $("ul.list-group-programs li:nth-child(n+6)")
        .show()
        .css("display", "none");
      $("ul.list-group-programs li:nth-child(5)").after(
        '<div class="hidden-overlay" style="position:absolute;top:38%;bottom:0;left:0;width:100%;height:62%;background: linear-gradient(to bottom, rgba(238,238,238,0), rgba(238,238,238,238) 100%);"></div>'
      );

      $("#show-all-course").on("click", function() {
        $(".hidden-overlay").hide("fast");
        $("ul.list-group-programs li")
          .show()
          .css("display", "flex");
        $("#hide-all-course").show();
        $("#show-all-course").hide();
      });

      $("#hide-all-course").on("click", function() {
        $(".hidden-overlay").show("fast");
        $("ul.list-group-programs li:nth-child(n+6)").hide();
        $("#hide-all-course").hide();
        $("#show-all-course").show();
      });

      $(".program-toggle, .card-header .row").click(function() {
        //$(this).find("div").toggleClass("toggle-open");
        if ($(this).hasClass("collapsed")) {
          $(this)
            .find("div")
            .removeClass("toggle-open");
          $(this)
            .find("div")
            .addClass("toggle-open");
          $(this)
            .find("span")
            .removeClass("toggle-open");
          $(this)
            .find("span")
            .addClass("toggle-open");
          $(this).removeClass("toggle-open");
          $(this).addClass("toggle-open");
        } else {
          $(this)
            .find("div")
            .removeClass("toggle-open");
          $(this)
            .find("span")
            .removeClass("toggle-open");
          $(this)
            .find("div")
            .addClass("toggle-open");
          $(this)
            .find("span")
            .addClass("toggle-open");
          $(this).removeClass("toggle-open");
          $(this).addClass("toggle-open");
        }
      });
    });
  },
};
