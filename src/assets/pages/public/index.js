import _uberPro from './_uberPro.json'
import _uberEats from './_uberEats.json'

const publicPage = {
    uberPro : _uberPro.data,
    uberEats : _uberEats.data
}

export default publicPage;