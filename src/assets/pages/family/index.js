import _uberPro from './_uberPro.json'
import _uberEats from './_uberEats.json'

const family = {
    uberPro : _uberPro.data,
    uberEats : _uberEats.data
}


export default family;