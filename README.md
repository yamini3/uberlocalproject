## Spaceballs: Client framework

### Steps to set up the project and production build
* `npm install`
* `npm run serve`

If you need to develop Vue project install `npm install -g @vue/cli`

For production build follow
* `npm run build`

And the './dist' folder needs to be deployed to s3 bucket, this is taken care by pipelines.